var GJM = {

    prefix : 'gjm',

    // the submitted form
    form : false,

    // google maps api features holder
    api_features : false,

    // target element
    target : false,

    // address field
    address_field : false,

    // previous address field
    previous_address : false,

    //coordinates
    lat_field : false,
    lng_field : false,

    auto_located : false,

    enter_clicked : false,

    prevent_submission : false,

    locator_button : false,

    /**
     * Run on page load
     * 
     * @return {[type]} [description]
     */
    init : function() {

        GJM.history_state_fix();
        GJM.custom_map_location();
        GJM.enable_geolocation_features();
        GJM.reset_form_fields();
        GJM.form_filters_trigger();
        GJM.update_form();
        //GJM.hide_map();
    },

    /**
     * This is a fix for a conflict with the GJM element ID when 
     * loading results from history.state
     *
     * The gjm_element_id is also passes via the state. But because the element ID
     * is generated randomly on page load ( unless specified by the user via shortcode attribute )
     * The element ID saved in the state will be different than the new ID generated on page load. 
     * This cause a conflict with the map element.
     *
     * This fix remove the gjm_element_id parameter from the state when the page first loads.
     * 
     * @return {[type]} [description]
     */
    history_state_fix : function() {

        if ( window.history.state && window.location.hash ) {

            jQuery( '.job_filters' ).each( function() {

                var state = window.history.state;
                
                if ( state.id && 'job_manager_state' ) {

                    state.data = state.data.replace( 'gjm_element_id', 'gjm_eid' ).replace( 'gjm_use', 'gjm_u' );
                    
                    //console.log(state)
                    //window.history.replaceState( { id: 'job_manager_state', page: page, data: data, index: index }, '', location + '#s=1' );

                    //form.find( ':input[name^="search_categories"]' ).not(':input[type="hidden"]').trigger( 'chosen:updated' );
                }
            });
        };
    },

    /**
     * Auto locator feature
     * 
     * @param  {[type]} locator_element [description]
     * @return {[type]}                 [description]
     */
    auto_locator : function( locator_element ) {

        GJM.locator_button = locator_element;
        GJM.prefix         = GJM.form.find( '.gjm-enabled' ).data( 'prefix' );
        GJM.form           = locator_element.closest( 'form' );
        
        // aniumate loader
        GJM.locator_button.addClass( 'animate-spin' );

        // if GPS exists locate the user //
        if ( navigator.geolocation ) {

            navigator.geolocation.getCurrentPosition( GJM.navigator_sucess, GJM.navigator_failed, { timeout: 10000 } );

        } else {

            GJM.locator_button.removeClass( 'animate-spin' );

            // if nothing found we cant do much. we cant locate the user :( //
            alert( 'Sorry! Geolocation is not supported by this browser and we cannot locate you.' );
        }
    },

    /**
     * Navigator sucess callback function
     * 
     * @param  {[type]} position [description]
     * @return {[type]}          [description]
     */
    navigator_sucess : function( position ) {

        var geocoder = new google.maps.Geocoder();

        geocoder.geocode( { 'latLng': new google.maps.LatLng(position.coords.latitude, position.coords.longitude ) }, function( results, status ) {

            if ( status == google.maps.GeocoderStatus.OK ) {

                GJM.form.find( '#search_location' ).val( results[0].formatted_address );
                GJM.form.find( '.gjm-prev-address' ).val( results[0].formatted_address );
                GJM.form.find( '.gjm-lat' ).val( results[0].geometry.location.lat() );
                GJM.form.find( '.gjm-lng' ).val( results[0].geometry.location.lng() );
                
                //run the ajax search
                //give it a bit of extra time to set the coordinates in the hidden fields
                setTimeout( function() {

                    //thisTarget.triggerHandler( 'update_results', [ 1, false ] );
                    GJM.form.find( '#search_location' ).change();
                    
                    GJM.locator_button.removeClass( 'animate-spin' );

                }, 500 );

            } else {

                GJM.locator_button.removeClass( 'animate-spin' );

                alert( 'Geocoder failed due to: ' + status );
            }
        });        
    },

    /**
     * Navigator failed callback function
     * 
     * @param  {[type]} error [description]
     * @return {[type]}       [description]
     */
    navigator_failed : function( error ) {

        if ( ! GJM.auto_located ) {
            
            switch ( error.code ) {

                case error.PERMISSION_DENIED :

                    alert( 'User denied the request for Geolocation.' );
                    
                break;

                case error.POSITION_UNAVAILABLE :

                    alert( 'Location information is unavailable.' );

                break;

                case 3 :

                    alert( 'The request to get user location timed out.' );

                break;

                case error.UNKNOWN_ERROR :

                    alert( 'An unknown error occurred' );

                break;
            }
        }

        //hide locator button and show loader 
        GJM.locator_button.removeClass( 'animate-spin' );
    },

    /**
     * Display map in custom location using shortcode
     *
     * if using results map shortcode elsewhere on the page remove the map from its original place
     * which is above the list of results into the new map holder.
     * 
     * @return {[type]} [description]
     */
    custom_map_location : function() {

        jQuery( '.gjm-map-wrapper, .grm-map-wrapper' ).each( function() {

            GJM.form   = jQuery( this ).closest( 'form' );
            GJM.prefix = GJM.form.find( 'gjm-enabled' ).data( 'prefix' );
            
            var mapId = jQuery( this ).attr( 'data-map-id' );

            if ( jQuery( '#' + GJM.prefix + '-results-map-holder-' + mapId ).length ) {

                jQuery( this ).detach().appendTo( '#' + GJM.prefix + '-results-map-holder-' + mapId );

            } else if ( jQuery( '.results-map-holder' ).length ) {

                jQuery( this ).detach().appendTo( '.results-map-holder' );
            }
        });
    },

    /**
     * Enable various geolocation features
     * 
     * @return {[type]} [description]
     */
    enable_geolocation_features : function() {

        //move the locator button inside the address field and trigger the cloick event
       /* jQuery( '.gjm-locator-btn, .grm-locator-btn' ).each( function() {
            
            GJM.address_field = jQuery( this ).closest( 'form' ).find( '.search_location' );

            jQuery( this ).detach().appendTo( GJM.address_field ).show().click( function() {

                GJM.auto_located = false;

                GJM.auto_locator( jQuery( this ) );
            });
        }); */

        // enable some features
        jQuery( '.gjm-enabled' ).each( function() {

            GJM.form         = jQuery( this ).closest( 'form' );
            GJM.api_features = jQuery( this ).data();

            // enable chosen on dropdown filters only in default themes.
            // Premium theme most likely already enables it
            if ( jQuery.fn.chosen ) {
                jQuery( 'select.' + GJM.api_features.prefix + '-filter' ).chosen( { search_contains: true } );
            }

            // add class if no filters exists in the form
            // Also remove a class added by jobify theme
            if ( ! GJM.form.find( '.gjm-filters-wrapper' ).length ) {
                GJM.form.find( '.search_jobs' ).addClass( 'gjm_no_filters' ).removeClass( 'gjm_use' );
            }
            
            // if action attribute exists means this is a custom form and will show results in a different page.
            // In this case we need to disable some value to prevent theme from passing into the URL
            if ( GJM.form.attr( 'action' ) ) {
                GJM.form.find( '.url-disabled' ).attr( 'disabled', 'disabled' );
            }  

            // Enable auto-locator
            if ( GJM.api_features.autolocator == 'enabled' ) {

                var locator_element = jQuery( this );

                jQuery( window ).on( 'load' ).on( 'load', ( function () {

                    GJM.auto_located = true;

                    GJM.auto_locator( locator_element );
                })); 
            }

            // Enable auto-locator
            if ( GJM.api_features.locator_button == 'enabled' ) {

                var locatorBtn = jQuery( '<i class="' + GJM.api_features.prefix + '-filter ' + GJM.api_features.prefix +'-locator-btn gjm-icon-target-1" title="Get your current position"></i>' ).click( function() {

                    GJM.auto_located = false;

                    GJM.auto_locator( jQuery( this ) );

                }).insertAfter( GJM.form.find( 'input#search_location' ) );

                //GJM.form.find( 'input#search_location' ).after( locatorBtn );
            }

            // enable address autocomplete
            if ( GJM.api_features.autocomplete == 'enabled' ) {
                
                acField = GJM.form.find( '#search_location' )[0];
                
                // options               
                var acOptions = {
                    types : [GJM.api_features.results_type]
                };

                if ( GJM.api_features.country != '' ) {
                    acOptions.componentRestrictions = { country : GJM.api_features.country }
                } 

                var acObject = new google.maps.places.Autocomplete( acField , acOptions );
                
                google.maps.event.addListener( acObject, 'place_changed', function(e) {

                    GJM.form.find( '.gjm-country, .gjm-state' ).val( '' );
                       
                    var place = acObject.getPlace();

                    if ( ! place.geometry ) {
                        return;
                    }

                    // if only country entered set its value in hidden fields
                    if ( place.address_components.length == 1 && place.address_components[0].types[0] == 'country' ) {
                                        
                        GJM.form.find( '.gjm-country' ).val( place.address_components[0].short_name );

                    // otherwise if only state entered.
                    } else if ( place.address_components.length == 2 && place.address_components[0].types[0] == 'administrative_area_level_1' ) {
                        
                        GJM.form.find( '.gjm-state' ).val( place.address_components[0].short_name );
                        GJM.form.find( '.gjm-country' ).val( place.address_components[1].short_name );
                    }

                    //update coords fields
                    GJM.form.find( '.gjm-lat' ).val( place.geometry.location.lat().toFixed( 6 ) );
                    GJM.form.find( '.gjm-lng' ).val( place.geometry.location.lng().toFixed( 6 ) );

                    //update prev_field to be the same as address field. This way
                    //we don't need to geocode the address again since we have coordinates already
                    GJM.form.find( '.gjm-prev-address' ).val( GJM.form.find( '#search_location' ).val() );       
                });
            }
        });
    },
    
    /**
     * Hide map when no results found
     * 
     * @return {[type]} [description]
     */
    /*
    hide_map : function() {
          
        jQuery( '.job_listings' ).on( 'updated_results', function( results ) {

            //temporary solution to check if "No results" after search submitted.
            //Until we find a "cleaner" way.
            if ( results.target.lastChild.innerHTML.indexOf('no_job_listings_found') > -1 )  {

                //GJM.prefix = jQuery(this).find('#gjm_prefix').val();
                GJM.prefix = tjQuery( this ).find( '#gjm-features-holder' ).data( 'prefix' );
                mapId      = jQuery( this ).find( '#gjm_element_id' ).val();

                alert(mapId)
                jQuery( '#' + GJM.prefix + '-map-wrapper-' + mapId ).slideUp();
            }
        });
    },
    */
   
    /**
     * Reset GJM form Fields when "Reset" link clicked
     * 
     * @return {[type]} [description]
     */
    reset_form_fields : function() {
        
        jQuery( '.job_filters, .resume_filters' ).on( 'click', '.reset', function( e ) {

            GJM.form = jQuery( this ).closest( 'form' );   

            GJM.form.find( '.gjm-filter option:first-child, .grm-filter option:first-child' ).attr( 'selected', 'selected' ).trigger( 'chosen:updated' );;
            GJM.form.find( 'select.gjm-filter, select.grm-filter' ).chosen( 'updated' );

            GJM.form.find( '.gjm-lat, .gjm-lng, .gjm-prev-address' ).val( '' )
        } );
    },

    /**
     *
     * Form Filters Trigger
     * 
     * Trigger form submission when GJM filters update
     * 
     * @return {[type]} [description]
     */
    form_filters_trigger : function() {
     
        jQuery( '.gjm-filter, .grm-filter' ).change( function() {

            if ( ( jQuery( this ).hasClass( 'radius-filter' ) || jQuery( this ).hasClass( 'units-filter' ) ) && jQuery( this ).closest( 'form' ).find( '#search_location' ).val() == '' ) {
                return;
            }

            jQuery( this ).closest( 'div.job_listings, div.resumes' ).trigger('update_results', [ 1, false ] );
        });
    },

    /**
     * update form and geocode address
     * 
     * @return {[type]} [description]
     */
    update_form : function() {

        jQuery( '.gjm-enabled' ).closest( 'form' ).find( '#search_location' ).keydown( function( event ) {

            if ( event.keyCode == 13 ) {
                
                GJM.enter_clicked = true;
               
                event.preventDefault();

                return false;
            }
        });

        jQuery( '.gjm-enabled' ).closest( 'form' ).find( '#search_keywords, #search_location, .job_types :input, #search_categories, .job-manager-filter' ).off( 'change' ).on( 'change', ( function ( event ) {
            
            // work around to prevent multiple submissions when enter click
            // For some reason, when enter key presses in the address field the form is being submitted.
            // However, after leaving the address field ( focusout ) the form is being submitted again.
            if ( GJM.prevent_submission && ! GJM.enter_clicked ) {

                GJM.enter_clicked      = false;
                GJM.prevent_submission = false;

                return false;
            
            } else {

                if ( GJM.enter_clicked ) {

                    GJM.enter_clicked      = false;
                    GJM.prevent_submission = true;     
                }
            }

            //get some fields
            GJM.form = jQuery( this ).closest( 'form' );
            GJM.target = jQuery( this ).closest( 'div.job_listings, div.resumes' );
            GJM.lat_field = GJM.form.find( '.gjm-lat' );
            GJM.lng_field = GJM.form.find( '.gjm-lng' );
            GJM.previous_address = GJM.form.find( '.gjm-prev-address' );

            //set timeout to let enough time for the address autocomplete value to set in the location field.
            //This is kind of a "hack" since the change event happesn faster then the address value can be set for the field
            //which results in a "currapted" address value.
            setTimeout( function() {
                
                newAddress     = GJM.form.find( '#search_location' ).val();
                prevAddress    = GJM.form.find( '.gjm-prev-address' ).val();
                address_exists = jQuery.trim( newAddress ).length ? true : false;
                
                // if no address entered
                if ( ! address_exists ) {
                    
                    //console.log('no address');

                    // clear fields from values
                    GJM.lat_field.val( '' );
                    GJM.lng_field.val( '' );
                    GJM.previous_address.val( '' );
                    
                    GJM.target.triggerHandler( 'update_results', [ 1, false ] );

                // if address entered
                } else if ( address_exists ) {
                    
                    //dynamically set the radius dropdown to the last option
                    //if address entered and the radius dropdown value selected is default
                    if ( jQuery( '.gjm-radius option:first-child' ).is( ':selected' ) ) {
                        jQuery( '.gjm-radius option:last-child' ).attr( 'selected', 'selected' );
                    }
                    
                    //if address changed or no coordinates exist we will geocode the address
                    //we check if the address changed using the prev_address hidden field
                    if ( newAddress != prevAddress || GJM.lat_field.val() == '' || GJM.lng_field.val() == '' ) {

                       // console.log('new address ' + newAddress );

                        GJM.form.find( '.gjm-country, .gjm-state' ).val( '' );
                   
                        //set the new address in the prev_address field
                        GJM.previous_address.val( newAddress );

                        //geocode the address
                        var geocoder = new google.maps.Geocoder();

                        apiRegion = gjmOptions.region !== undefined ? gjmOptions.region : 'us';

                        geocoder.geocode( { 'address': newAddress, 'region' : apiRegion }, function( results, status) {
                            
                            //if geocoding successful
                            if ( status == google.maps.GeocoderStatus.OK ) {

                                // if only country entered set its value in hidden fields
                                if ( results[0].address_components.length == 1 && results[0].address_components[0].types[0] == 'country' ) {
                                    
                                    GJM.form.find( '.gjm-country' ).val( results[0].address_components[0].short_name );

                                // otherwise, if only state entered.
                                } else if ( results[0].address_components.length == 2 && results[0].address_components[0].types[0] == 'administrative_area_level_1' ) {
         
                                    GJM.form.find( '.gjm-state' ).val( results[0].address_components[0].short_name );
                                    GJM.form.find( '.gjm-country' ).val( results[0].address_components[1].short_name );
                                }

                                //set the coordinates
                                GJM.lat_field.val( results[0].geometry.location.lat().toFixed( 6 ) );
                                GJM.lng_field.val( results[0].geometry.location.lng().toFixed( 6 ) );

                                //run the ajax search
                                //give it a bit of extra time to set the coordinates in the hidden fields
                                setTimeout( function() {
                                    GJM.target.triggerHandler( 'update_results', [ 1, false ] );
                                }, 500 );
                            
                            //if geocoding failed
                            } else {

                                GJM.lat_field.val( '' );
                                GJM.lng_field.val( '' );
                                GJM.previous_address.val( '' );

                                //if address could not geocoded abort the function and display an error message
                                alert( 'We could not find the address you entered. Please try a different address. Error message: ' + status );     
                            }
                        });

                    //if the same address entered we should be able to use the previous coords again.
                    } else {

                        //console.log('same address');
                        GJM.target.triggerHandler( 'update_results', [ 1, false ] );
                    }
                } 
            }, 500 );

            return false;    
        }));
    }
}
jQuery( document ).ready( function($) {
    
    GJM.init();
});