// global maps object 
var GJM_Maps = {};

/**
 * Map generator function
 * 
 * @param {[type]} map_id [description]
 * @param {[type]} vars  [description]
 */
var GJM_Map = function( vars ) {

	/**
	 * [self description]
	 * @type {[type]}
	 */
	var self = this;

	/**
	 * Map ID
	 * @type {integer}
	 */
	//var map_id = vars.args.map_id;

	/**
	 * Map args
	 * @type {array}
	 */
	//var map_args = vars.args;

	/**
	 * Map options
	 * @type {array}
	 */
	//var self.map_options = vars.self.map_options;

	/**
	 * Locations
	 * @type {array}
	 */
	//var self.locations = vars.self.locations;

	/**
	 * User position
	 * @type {array}
	 */
	//var user_position = vars.user_position;

	/**
	 * GEO my WP Form 
	 * @type {array}
	 */
	//var form = vars.form;


	/**
	 * Init function
	 * 
	 * @return void
	 */
	this.init = function() {
 
		// abort if map element not exist
		if ( ! jQuery( '#' + vars.args['map_element'] ).length ) {
			return;
		}

		self.map_id        = vars.args.map_id;

		this.collect_data();

		// Show map element if hidden.
		jQuery( self.map_args['map_wrapper'] ).slideDown( 'slow', function() {
			
			console.log(GJM_Maps)
			// check if map already exist. Otherwise, genertate it.
			if ( ! GJM_Maps[self.map_id].map_data['map'] ) {
		
				this.collect_data();

				self.render_map();

			// otherwise update existing map
			} else {

				this.collect_data();
				
				self.update_map();
			}
		});
	};

	this.collect_data = function() {

		var map_settings = jQuery( '#' + vars.args.map_element ).data();


		console.log( map_settings )

		console.log( vars )
		
		self.map_id        = vars.args.map_id;

		self.map_args 	   			= vars.args;
		self.map_args.group_markers = map_settings.group_markers;

		self.map_options   			 = vars.map_options;
		self.map_options.mapTypeId   = map_settings.map_type;
		self.map_options.scrollwheel = map_settings.scrollwhee;
		self.map_options.maxZoom     = map_settings.max_zoom_level;
		self.map_options.zoomLevel   = map_settings.zoom_level;

		self.locations 	   = vars.locations;
		
		self.user_position = vars.user_position;
		self.user_position.map_icon = map_settings.user_marker;

		self.form 		   = vars.form;
		self.map_data 	   = {
			id 		  	  	: self.map_id,
			map   		  	: false,
			markers 	  	: [],
			user_position 	: false,
			userMarker	  	: false,
			clusters	  	: false,
			spiderfier    	: false,
			location_iw	  	: false,
			userInfoWindow	: false,
			bounds 		  	: new google.maps.LatLngBounds(),
			infobox  	    : null,
			currentMarker   : null,
			autoZoomLevel   : false
		};
	}

	/**
	 * Render the map
	 * 
	 * @return {[type]} [description]
	 */
	this.render_map = function() {
		
		// set auto zoom level
		if ( self.map_options['zoomLevel'] == 'auto' ) {

			self.map_data['autoZoomLevel'] = true;
			self.map_options['zoomLevel'] = 13;
		
		// otherwise specifiy the zoom level
		} else {

			self.map_data['autoZoomLevel'] = false;
			self.map_options['zoomLevel'] = parseInt( self.map_options['zoomLevel'] );
		}
 
		// map center
		//self.map_options['center'] = new google.maps.LatLng( user_position['lat'], user_position['lng'] );
		self.map_options['center'] = new google.maps.LatLng( '40.758895', '-73.985131' );

		// map type
		self.map_options['mapTypeId'] = google.maps.MapTypeId[self.map_options['mapTypeId']];
									
		// generate the map element
		self.map_data['map'] = new google.maps.Map( 
			document.getElementById( self.map_args['map_element'] ),
			self.map_options 
		);
	
		// after map was generated
		google.maps.event.addListenerOnce( self.map_data['map'], 'idle', function(){	
			
			// fadeout the map loader
			jQuery( '#gmw-map-loader-' + self.map_id ).fadeOut( 1000 );
			
			// create map expand toggle if needed
			// temporary disabled. It seems that Google added this feature to his API
			if ( self.map_options['resizeMapControl'] && jQuery( '#gmw-resize-map-toggle-' + self.map_id ).length != 0 ) {

				// generate resize toggle
				self.map_data['resize_map_control'] = document.getElementById( 'gmw-resize-map-toggle-' + self.map_id );
				self.map_data['resize_map_control'].style.position = 'absolute';	
				self.map_data['map'].controls[google.maps.ControlPosition.TOP_RIGHT].push( self.map_data['resize_map_control'] );			
				self.map_data['resize_map_control'].style.display = 'block';
			
				// resize map on click event	
		    	jQuery( '#gmw-resize-map-toggle-' + self.map_id ).on( 'click', function() {
		 	
		 			// get the current map center
		    		var mapCenter = self.map_data['map'].getCenter();

		    		// replace map wrapper class to expended
		    		jQuery(this).closest( '.gmw-map-wrapper' ).toggleClass( 'gmw-expanded-map' ); 

		    		// replace the toggle icon         		
		    		jQuery(this).toggleClass( 'gmw-icon-resize-full' ).toggleClass( 'gmw-icon-resize-small' );
		    		
		    		// we wait a very short moment to allow the wrapper element resize
		    		setTimeout( function() { 	

		    			// resize map		    		
		    			google.maps.event.trigger( self.map_data['map'], 'resize' );

		    			// recenter map
		    			self.map_data['map'].setCenter(mapCenter);		

					}, 100 );            		
		    	});
			}

			// clear existing markers
			self.clear_markers();

			// generate new markers
			self.render_markers();
		});
	};

	/**
	 * Update existing map
	 * 
	 * @param  {[type]} mapVars [description]
	 * @return {[type]}         [description]
	 */
	this.update_map = function() {

		// clear existing markers
		self.clear_markers();

		// generate new markers
		self.render_markers();
	}

	/**
	 * Remove markers if exists on the map
	 * 
	 * @return {[type]} [description]
	 */
	this.clear_markers = function() {

		// loop through existing markers
		for ( var i = 0; i < self.map_data['markers'].length + 1 ; i++ ) {

			if ( i < self.map_data['markers'].length ) {

				//if ( typeof self.map_data['markers'][i] !== 'undefined' ) {
				
				// verify marker
				if ( self.map_data['markers'][i] ) {

					// clear marker
					self.map_data['markers'][i].setMap( null );	
				}
				//}
				
			// proceed when doen
			} else {

				// generate new markers array.
				self.map_data['markers'] = [];
				
				// clear group markers
				this.group_markers();
			}
		}
	};

	/**
	 * Initilize markers grouping. 
	 * 
	 * markers clusters, spiderfier and can be extended
	 * 
	 * @return {[type]} [description]
	 */
	this.group_markers = function() {
		
		// hook custom functions if needed
		GMW.do_action( 'gmw_map_group_markers', self.map_args['group_markers'], self );

		// generate the grouping function
		var groupMarkerFunction = 'group_' + self.map_args['group_markers'];

		// verify grouping function
		if ( typeof self[groupMarkerFunction] === 'function' ) {

			// run marker grouping
			self[groupMarkerFunction]( self );
		
		// otherwise show error message
		} else {

			console.log( 'The function ' + groupMarkerFunction + ' not exists.' );
		}
	};

	/**
	 * normal grouping function holder
	 *
	 * since there is no normal grouping we do nothing here. 
	 * 
	 * this is just a function holder. 
	 * 
	 * @return void
	 * 
	 */
	this.group_normal = function() {}

	/**
	 * Markers Clusterer grouping
	 *  
	 * @param  {[type]} group_markers [description]
	 * @param  {[type]} mapObject     [description]
	 * @return {[type]}               [description]
	 */
	this.group_markers_clusterer = function( mapObject ) {
		
		// initialize markers clusterer if needed and if exists
	    if ( typeof MarkerClusterer === 'function' ) {

	    	// remove existing clusters
	    	if ( self.map_data['clusters'] != false ) {		

	    		self.map_data['clusters'].clearMarkers();
	    		//data['clusters'].setMap( null );
	    	}
	    	
	    	//create new clusters object
			self.map_data['clusters'] = new MarkerClusterer( 
				self.map_data['map'], 
				self.map_data['markers'] 
			);
		} 
	}

	/**
	 * Generate single marker
	 * 
	 * @return {[type]} [description]
	 */
	this.generate_marker = function( marker_id ) {

		var marker_options = {
			position 	: self.locations[marker_id]['marker_position'],
			icon     	: self.locations[marker_id]['map_icon'],
			map      	: self.map_data['map'],
			id       	: marker_id,
			gmwContent  : self.locations[marker_id]['info_window_content']
		}

		marker_options = GMW.apply_filters( 'gmw_generate_marker_options', marker_options, marker_id, self );

		// generate marker
		return new google.maps.Marker( marker_options );
	}

	/**
	 * Create markers loop
	 * 
	 * @return {[type]} [description]
	 */
	this.render_markers = function() {

		var locations_count = self.locations.length;

		// loop through self.locations
		for ( i = 0; i < locations_count + 1 ; i++ ) {  
			
			// generate markers
			if ( i < locations_count ) {

				// verify location coordinates
				if ( self.locations[i]['lat'] == undefined || self.locations[i]['lng'] == undefined || self.locations[i]['lat'] == '0.000000' || self.locations[i]['lng'] == '0.000000' ) {
					continue;
				}
				
				// generate the marker position
				self.locations[i]['marker_position'] = new google.maps.LatLng( 
					self.locations[i]['lat'], 
					self.locations[i]['lng'] 
				);
				
				// append location into bounds
				self.map_data['bounds'].extend( self.locations[i]['marker_position'] );
	
			    // generate marker
				self.map_data['markers'][i] = self.generate_marker( i );
				
				// if no grouping
				if ( self.map_args['group_markers'] == 'normal' ) {		

					self.map_data['markers'][i].setMap( self.map_data['map'] );

					// init marker click event
					google.maps.event.addListener( self.map_data['markers'][i], 'click', function() {
						self.marker_click( this )
					});

				// add marker to cluster if needed
				} else if ( self.map_args['group_markers'] == 'markers_clusterer' ) {	

					// add marker to cluster object
					self.map_data['clusters'].addMarker( self.map_data['markers'][i] );	

					// init marker click event
					google.maps.event.addListener( self.map_data['markers'][i], 'click', function() {
						self.marker_click( this )
					});	
				
				// add marker to spiderfier if needed
				} else if ( self.map_args['group_markers'] == 'markers_spiderfier' ) {	

					// add marker into spiderfier object
					self.map_data['spiderfier'].addMarker( self.map_data['markers'][i] );

					// place marker on the map
					self.map_data['markers'][i].setMap( self.map_data['map'] );	
				}

				// hook custom function for custom grouping and others
				GMW.do_action( 'gmw_map_single_marker', self.map_data['markers'][i], self );

			// Continue when done generating the markers.
			} else {

				// display the user position
				self.get_user_position();
			}
		}

	};

	/**
	 * User position
	 *
	 * Create the user's marker and info window
	 * 
	 * @return {[type]} [description]
	 */
	this.get_user_position = function() {

		// remove existing user marker
		if ( self.map_data['userMarker'] != false ) {
			self.map_data['userMarker'].setMap( null );
			self.map_data['userMarker']    = false;
			self.map_data['user_position'] = false;
		}

		// generate new user location marker
		if ( self.user_position['lat'] && self.user_position['lng'] && self.user_position['map_icon'] != '0' && self.user_position['map_icon'] != '' ) {

			// generate user's position
			self.map_data['user_position'] = new google.maps.LatLng( 
				self.user_position['lat'], 
				self.user_position['lng'] 
			);
			
			// append user position to bounds
			self.map_data['bounds'].extend( self.map_data['user_position'] );
			
			// generate user marker
			self.map_data['userMarker'] = new google.maps.Marker({
				position : self.map_data['user_position'],
				map      : self.map_data['map'],
				icon     : self.user_position['map_icon']
			});
			
			// generate info-window if content exists
			if ( self.user_position['iw_content'] != false && self.user_position['iw_content'] != null ) {

				// generate info-window
				self.map_data['userInfoWindow'] = new google.maps.InfoWindow({
					content : self.user_position['iw_content']
				});
			      					
			    // open info window on map load
				if ( self.user_position['iw_open'] == true ) {

					self.map_data['userInfoWindow'].open( 
						self.map_data['map'], 
						self.map_data['userMarker'] 
					);
				}
				
				// open info window on marker click
			    google.maps.event.addListener( self.map_data['userMarker'], 'click', function() {

			    	self.map_data['userInfoWindow'].open( 
			    		self.map_data['map'], 
			    		self.map_data['userMarker'] 
			    	);
			    });     
			}

			// center map when done creating the users position
			self.center_map();

		// otherwise, just center the map
		} else {

			// center map
			self.center_map();
		}		
	}

	/**
	 * center and zoom map
	 * 
	 * @return {[type]} [description]
	 */
	this.center_map = function() {

		// custom zoom point
		if ( self.map_args['zoom_position'] != false && ! self.map_data['autoZoomLevel'] ) {

			// get position
			var latLng = new google.maps.LatLng( 
				self.map_args['zoom_position']['lat'], 
				self.map_args['zoom_position']['lng'] 
			);

			self.map_data['map'].setZoom( parseInt( self.map_options['zoomLevel'] ) );
			self.map_data['map'].panTo( latLng );

		// zoom map when a single marker exists on the map
		} else if ( self.locations.length == 1 && self.map_data['user_position'] == false ) {

			if ( self.map_data['autoZoomLevel'] ) {
				self.map_data['map'].setZoom( 13 );
			} else {
				self.map_data['map'].setZoom( parseInt( self.map_options['zoomLevel'] ) );
			}

			self.map_data['map'].panTo( self.map_data['markers'][0].getPosition() );

		} else if ( ! self.map_data['autoZoomLevel'] && self.map_data['user_position'] != false ) {

			self.map_data['map'].setZoom( parseInt( self.map_options['zoomLevel'] ) );
			self.map_data['map'].panTo( self.map_data['user_position'] );

		} else if ( self.map_data['autoZoomLevel'] || self.map_data['user_position'] == false  ) { 
		
			self.map_data['map'].fitBounds( self.map_data['bounds'] );
		}
	}

	/**
	 * Marker click event
	 * 
	 * @param  {[type]} marker [description]
	 * 
	 * @return {[type]}        [description]
	 */
	this.marker_click = function( marker ) {

		// hook custom functions
		GMW.do_action( 'gmw_map_marker_click', marker, self );

		self.map_data['currentMarker'] = marker;

		// Clear directions if set on the map 
		if ( typeof directionsDisplay !== 'undefined' ) {
			directionsDisplay.setMap( null );
		}
		
		// generate the click function
		var markerClickFunction = 'marker_click_' + self.map_args['info_window_type'];

		// verify marker click function
		if ( typeof self[markerClickFunction] === 'function' ) {

			// execute marker click event
			self[markerClickFunction]( marker, self );
		
		// show an error if function is missing
		} else {
			console.log( 'The function ' + markerClickFunction + ' not exists.' );
		}

	};

	/**
	 * Normal info window 
	 * 
	 * @param  {[type]} marker    [description]
	 * @param  {[type]} iw_type   [description]
	 * @param  {[type]} mapObject [description]
	 * @return {[type]}           [description]
	 */
	this.marker_click_normal = function( marker ) {

		// verify iw content
		if ( self.locations[marker.id]['info_window_content'] ) {
			
			// close open window
			if ( self.map_data['location_iw'] ) {
				self.map_data['location_iw'].close();
				self.map_data['location_iw'] = null;
			}
			
			// generate new window
			self.map_data['location_iw'] = new google.maps.InfoWindow({
				content  : self.locations[marker.id]['info_window_content'],
				maxWidth : 300
			});
		
			// open window
			self.map_data['location_iw'].open( 
				self.map_data['map'], 
				marker 
			);
		}
	}
}

/**
 * On document ready generate all maps exists in the global maps holder
 * 
 * @param  {GMW_Map}
 * @return {[type]}       [description]
 */
jQuery( document ).ready( function($){ 	

	var GJM_Maps_Object = typeof gjmMapObjects != 'undefined' ? gjmMapObjects : {};

	// loop through and generate all maps
	jQuery.each( GJM_Maps_Object, function( map_id, vars ) {	

		// generate new map
		GJM_Maps[map_id] = new GJM_Map( vars );
		// initiate it
		GJM_Maps[map_id].init();
	});
});




/**
 * GMW main map function
 * @param gmwForm
 */
function gjmMapInit( mapObject, mapId ) {
	
	
	//make sure the map element exists to prevent JS error
	if ( ! jQuery( '#' + mapObject['mapElement'] ).length ) {
		return;
	}
	
	// get map settings from the map element
	mapObject['settings'] = jQuery( '#' + mapObject['mapElement'] ).data();

	if ( typeof gjmMapElements[mapId] === 'undefined' ) {

		gjmMapElements[mapId] = gjmMapElements[mapId];

		//add map obejct to global maps object
		gjmMapElements[mapId] = {
			mapId 		  : mapId,
			map 		  : false,
			markers 	  : [],
			userPosition  : false,
			userMarker 	  : false,
			clusters	  : false,
			spiderfier 	  : false,
			markerClicked : false,
			locationIW 	  : false,
			userIW 		  : false,
			bound 		  : [],
			prevLocations : [],
			polylines 	  : []
		};

		//mapObject['mapOptions']['mapTypeId']   = mapObject['settings']['map_type'];
		//mapObject['mapOptions']['zoomLevel']   = mapObject['settings']['zoom_level'];
		mapObject['mapOptions']['scrollwheel'] = mapObject['settings']['scrollwheel'];
	

		//initiate map options
		mapObject['mapOptions']['zoom'] 	 = 1;
		mapObject['mapOptions']['center'] 	 = new google.maps.LatLng( mapObject['userPosition']['lat'], mapObject['userPosition']['lng'] );
		mapObject['mapOptions']['mapTypeId'] = google.maps.MapTypeId[mapObject['settings']['map_type']];
					
	console.log( mapObject['mapOptions'])				
		gjmMapElements[mapId]['map'] = new google.maps.Map(document.getElementById( mapObject['mapElement'] ), mapObject['mapOptions'] );

		//after map was created
		google.maps.event.addListenerOnce( gjmMapElements[mapId]['map'], 'idle', function(){	
			 
			//fadeout the map loader if needed
			if ( mapObject['mapLoaderElement'] != false ) {
				jQuery(mapObject['mapLoaderElement']).fadeOut(1000);
			}
			
			//create map expand toggle if needed
			/*if ( mapObject['resizeMapElement'] != false ) {
			
				gjmMapElements[mapId]['resizeMapControl'] = document.getElementById(mapObject['resizeMapElement']);
				gjmMapElements[mapId]['resizeMapControl'].style.position = 'absolute';	
				gjmMapElements[mapId]['map'].controls[google.maps.ControlPosition.TOP_RIGHT].push(gjmMapElements[mapId]['resizeMapControl']);			
				gjmMapElements[mapId]['resizeMapControl'].style.display = 'block';
			
				//expand map function		    	
		    	jQuery('#'+mapObject['resizeMapElement']).click(function() {
		    		
		    		var mapCenter = gjmMapElements[mapId]['map'].getCenter();
		    		jQuery(this).closest('.'+mapObject.prefix+'-map-wrapper').toggleClass(mapObject.prefix+'-expanded-map');          		
		    		jQuery(this).toggleClass('gjm-icon-resize-full').toggleClass('gjm-icon-resize-small');
		    		
		    		setTimeout(function() { 			    		
		    			google.maps.event.trigger(gjmMapElements[mapId]['map'], 'resize');
		    			gjmMapElements[mapId]['map'].setCenter(mapCenter);							
					}, 100);            		
		    	});
			} */
		});
	} 
	
	mapObject['userPosition']['mapIcon'] = mapObject['settings']['user_marker'];

	// if indeed listings exist
	/* for th future
	if ( jQuery( ".indeed_job_listing" ).length ) {

		var IndeedMapIcon = jQuery( 'input[name="gjm_element_id"][value="'+mapId+'"]' ).closest( 'div.job_listings' ).find( 'input[name="gjm_location_marker"]' ).val();

		//search for indeed locations
		jQuery( 'input[name="gjm_element_id"][value="'+mapId+'"]' ).closest( 'div.job_listings' ).find( '.indeed_job_listing.job_listing' ).each( function( index ) {
				
			var lat = jQuery( this ).data( 'latitude' );
			var lng = jQuery( this ).data( 'longitude' );
			
			// create marker only if not a remote location
			if ( lat != 25 && lng != -40 ) {
				
				if ( jQuery( this ).attr( 'data-location' ) ) {
					var address = jQuery( this ).data( 'location' );
				} else {
					jQuery( this ).find( '.job_listing-about .job_listing-location' ).text();
				}

				mapObject['locations'].push( {
					lat 				: lat,
					long 				: lng,
					mapIcon 			: IndeedMapIcon,
					info_window_content : jQuery( this ).find( '.job_listing-about' )[0]
					//info_window_content : '<div class="gjm-info-window-wrapper "><h3 class="title"><a target="_blank" href="'+jQuery( this ).data( 'href' )+'">'+jQuery( this ).data( 'title' )+'</a></h3><span class="location">'+jQuery( this ).data( 'location' )+'</span></div>'
				});
			}
		});
	}
	*/

	function group_markers() {

		//initiate markers clusterer or spiderfier if needed
	    if ( mapObject['settings']['group_markers'] == 'markers_clusterer' && typeof MarkerClusterer === 'function' ) {
	    	
	    	//remove clusters if exist
	    	if ( gjmMapElements[mapId]['clusters'] != false ) {	
	    		gjmMapElements[mapId]['clusters'].clearMarkers();
	    		//gjmMapElements[mapId]['clusters'].setMap(null);
	    	}
	    	
	    	// make sure its not set to discontinued path
	    	//if ( clusterImage == 'https://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclustererplus/images/m' ) {
	    	//	clusterImage = 'https://raw.githubusercontent.com/googlemaps/js-marker-clusterer/gh-pages/images/m';
	    	//}

			gjmMapElements[mapId]['clusters'] = new MarkerClusterer( gjmMapElements[mapId]['map'], gjmMapElements[mapId]['markers'], { 
				imagePath : clusterImage,
				maxZoom   : 15 
			} );
			 
		} else if ( mapObject['settings']['group_markers'] == 'markers_spiderfier' && typeof OverlappingMarkerSpiderfier === 'function' ) {
			
			gjmMapElements[mapId]['spiderfier'] = new OverlappingMarkerSpiderfier( 
				gjmMapElements[mapId]['map'], { 
					legWeight : 2,
					markersWontMove: true, 
	  				markersWontHide: true,
	  				basicFormatEvents: true,
	  				keepSpiderfied : true
				} 
			);
		
			gjmMapElements[mapId]['spiderfier'].addListener('click', function( markerClicked , event) {
				iwOnClick( markerClicked );
			});	
		} 	
	}

	gjmMapElements[mapId]['bounds'] = new google.maps.LatLngBounds();

	//remove existing markers
	for ( var i = 0; i < gjmMapElements[mapId]['markers'].length + 1 ; i++ ) {

		//remove each marker from the map
		if ( i < gjmMapElements[mapId]['markers'].length ) {
			if ( typeof gjmMapElements[mapId]['markers'][i] !== 'undefined' ) {
				gjmMapElements[mapId]['markers'][i].setMap( null );	
			}
		//clear markers array
		} else {
			gjmMapElements[mapId]['markers'] = [];
			group_markers();
		}
	} 

	for ( var i = 0; i < gjmMapElements[mapId]['polylines'].length + 1 ; i++ ) {

		//remove each marker from the map
		if ( i < gjmMapElements[mapId]['polylines'].length ) {
			if ( typeof gjmMapElements[mapId]['polylines'][i] !== 'undefined' ) {
				gjmMapElements[mapId]['polylines'][i].setMap( null );	
			}
		//clear markers array
		} else {
			gjmMapElements[mapId]['polylines'] = [];
		}            
	}

	// only if not using pagination. We need to appened markers 
	if ( ! formPagin ) {
	    
	    if ( gjmMapElements[mapId]['prevLocations'] == 0 ) {

	    	gjmMapElements[mapId]['prevLocations'] = mapObject['locations'];

	    } else {

	    	if ( appendResults ) {
				
				temLoc = jQuery.merge( mapObject['locations'], gjmMapElements[mapId]['prevLocations'] );

	    		gjmMapElements[mapId]['prevLocations'] = mapObject['locations'];

	    		mapObject['locations'] = temLoc;
			} else {
				gjmMapElements[mapId]['prevLocations'] = mapObject['locations'];
			}
	    }
	}
console.log( mapObject['locations'] )
	// loop through locations
	for ( i = 0; i < mapObject['locations'].length + 1 ; i++ ) {  
				

		//create markers
		if ( i < mapObject['locations'].length ) {

			//make sure location has coordinates to prevent JS error
			if ( mapObject['locations'][i]['lat'] == undefined || mapObject['locations'][i]['long'] == undefined || mapObject['locations'][i]['lat'] == '0.000000' || mapObject['locations'][i]['long'] == '0.000000' )
				continue;
			
			var gjmLocation = new google.maps.LatLng( mapObject['locations'][i]['lat'], mapObject['locations'][i]['long'] );
			
			// only if not using markers spiderfeir
			if ( mapObject['settings']['group_markers'] != 'markers_spiderfier' ) {

				// check if marker with the same location already exists
				// if so, we will move it a bit
	            if ( gjmMapElements[mapId]['bounds'].contains( gjmLocation ) ) {
	                
	                // do the math     
	                var a = 360.0 / mapObject['locations'].length;
	                var newLat = gjmLocation.lat() + - .000025 * Math.cos( ( +a*i ) / 180 * Math.PI );  //x
	                var newLng = gjmLocation.lng() + - .000025 * Math.sin( ( +a*i )  / 180 * Math.PI );  //Y
	                var newPosition = new google.maps.LatLng( newLat,newLng );

	                // draw a line between the original location 
	                // to the new location of the marker after it moves
	                var polyline = new google.maps.Polyline({
					    path : [
					        gjmLocation, 
					        newPosition
					    ],
					    strokeColor   : "#FF0000",
					    strokeOpacity : 1.0,
					    strokeWeight  : 2,
					    map 		  : gjmMapElements[mapId]['map']
					});

	                gjmMapElements[mapId]['polylines'].push( polyline );

					var gjmLocation = newPosition;
	            }
	        }

			gjmMapElements[mapId]['bounds'].extend( gjmLocation );

		    //create marker
			gjmMapElements[mapId]['markers'][i] = new google.maps.Marker({
				position : gjmLocation,
				icon 	 : mapObject['locations'][i]['mapIcon'],
				map      : gjmMapElements[mapId]['map'],
				id 		 : i 
			});
			
			 //add marker to clusterer if needed
			if ( mapObject['settings']['group_markers'] == 'markers_clusterer' ) {	

				gjmMapElements[mapId]['clusters'].addMarker( gjmMapElements[mapId]['markers'][i] );		

				//initiae marker click
				google.maps.event.addListener( gjmMapElements[mapId]['markers'][i], 'click', function() {
					iwOnClick( this )
				});

			//add marker to spiderfier if needed
			} else if ( mapObject['settings']['group_markers'] == 'markers_spiderfier' ) {	

				gjmMapElements[mapId]['spiderfier'].addMarker( gjmMapElements[mapId]['markers'][i] );
				gjmMapElements[mapId]['markers'][i].setMap(gjmMapElements[mapId]['map']);	
				
			} else {		

				gjmMapElements[mapId]['markers'][i].setMap(gjmMapElements[mapId]['map'] );

				google.maps.event.addListener( gjmMapElements[mapId]['markers'][i], 'click', function() {
					iwOnClick( this )
				});			
			}

		//when done creating the markers continue
		} else {

			//remove existing user marker
			if ( gjmMapElements[mapId]['userMarker'] != false ) {
				gjmMapElements[mapId]['userMarker'].setMap(null);
				gjmMapElements[mapId]['userMarker']   = false;
				gjmMapElements[mapId]['userPosition'] = false;
			}

			//create user's location marker
			if ( mapObject['userPosition']['lat'] != false && mapObject['userPosition']['lng'] != false && mapObject['userPosition']['mapIcon'] != false ) {
			
				//user's location
				gjmMapElements[mapId]['userPosition'] = new google.maps.LatLng( mapObject['userPosition']['lat'], mapObject['userPosition']['lng'] );
				
				//append user's location to bounds
				gjmMapElements[mapId]['bounds'].extend( gjmMapElements[mapId]['userPosition'] );
				
				//create user's marker
				gjmMapElements[mapId]['userMarker'] = new google.maps.Marker({
					position : gjmMapElements[mapId]['userPosition'],
					map      : gjmMapElements[mapId]['map'],
					icon     : mapObject['userPosition']['mapIcon']
				});
				
				/* 
				var circle = new google.maps.Circle({
				  map: gjmMapElements[mapId]['map'],
				  radius: 200,    // 10 miles in metres
				  fillColor: '#AA0000'
				});

				circle.bindTo( 'center', gjmMapElements[mapId]['userMarker'], 'position' );
				*/

				//create user's marker info-window
				if ( mapObject['userPosition']['iwContent'] != null ) {

					gjmMapElements[mapId]['userIW'] = new google.maps.InfoWindow({
						content: mapObject['userPosition']['iwContent']
					});
				      					
					if ( mapObject['userPosition']['iwOpen'] == true ) {
						gjmMapElements[mapId]['userIW'].open( gjmMapElements[mapId]['map'], gjmMapElements[mapId]['userMarker'] );
					}
					
				    google.maps.event.addListener( gjmMapElements[mapId]['userMarker'], 'click', function() {
				    	gjmMapElements[mapId]['userIW'].open( gjmMapElements[mapId]['map'], gjmMapElements[mapId]['userMarker'] );
				    });     
				}
			} 

			//zoom map to fit all markers
			if ( gjmMapElements[mapId]['markers'].length == 0 ) {

				//gjmMapElements[mapId]['map'].setZoom(1);

			} else if ( mapObject['locations'].length == 1 && gjmMapElements[mapId]['userPosition'] == false ) {

				$zoom = ( mapObject['zoomLevel'] == 'auto' ) ? 12 : mapObject['zoomLevel'];

				gjmMapElements[mapId]['map'].setZoom( parseInt( $zoom ) );
				gjmMapElements[mapId]['map'].panTo( gjmMapElements[mapId]['markers'][0].getPosition() );

			} else if ( mapObject['zoomLevel'] != 'auto' && gjmMapElements[mapId]['userPosition'] != false ) {

				gjmMapElements[mapId]['map'].setZoom( parseInt( mapObject['zoomLevel'] ) );
				gjmMapElements[mapId]['map'].panTo( gjmMapElements[mapId]['userPosition'] );

			} else if ( mapObject['zoomLevel'] == 'auto' || gjmMapElements[mapId]['userPosition'] == false  ) { 
		
				gjmMapElements[mapId]['map'].fitBounds(gjmMapElements[mapId]['bounds']);
			}
		}
	}
	
	//on marker click function
	function iwOnClick( markerClicked ) {

		if ( mapObject['locations'][markerClicked.id]['info_window_content']  ) {
				
			if ( gjmMapElements[mapId]['locationIW'] ) {
				gjmMapElements[mapId]['locationIW'].close();
				gjmMapElements[mapId]['locationIW'] = null;
			}
			
			gjmMapElements[mapId]['locationIW'] = new google.maps.InfoWindow({
				content: mapObject['locations'][markerClicked.id]['info_window_content']
			});
	
			gjmMapElements[mapId]['locationIW'].open(gjmMapElements[mapId]['map'], markerClicked);
		}
	}
}
/*
jQuery( document ).ready( function( $ ){ 	
	
	formPagin = ( typeof formFilters === 'undefined' ) ? false : formFilters.show_pagination;

	if ( typeof formPagin === 'undefined' || ( formPagin != 'true' && formPagin != 'yes' && formPagin != 1 ) ) {
		
		formPagin = false;

		$( '.job_listings, .resumes' ).on( 'update_results', function( event, page, append ) {
			if ( append ) {
				appendResults = true;
			} else {
				appendResults = false;
			}
		});
	}	

	// hide map if no results found.
	// temporary solution untill we come up with something better.
	$( document ).ajaxStop( function( event ) {

	  if ( $( '.no_job_listings_found, .no_resumes_found' ).length  ) {

	  	$( '.no_job_listings_found, .no_resumes_found' ).each( function( index ) {

	  		// get the map ID
	  		mapId = $( this ).closest( 'div.job_listings, div.resumes' ).find( '.gjm_element_id').val();
	  		
	  		// hide the map
			$( '.map-wrapper[data-map_id="'+mapId+'"]' ).slideUp();
	  		//mapId = $( this ).closest( 'div.job_listings, div.resumes' ).find( '.map-wrapper').slideUp();
	  	});
	  }
	});

	if ( typeof gjmMapObjects == 'undefined' ) {
		return false;
	}

	gjmMapsObjectInit( gjmMapObjects );
});
*/
/*
function gjmMapsObjectInit( gjmMapObjects ) {
	
	//create global maps object only once on page load
	//the maps object will allow us to have multiple maps on the screen
	if ( typeof gjmMapElements === 'undefined' ) {
		gjmMapElements = {};
	}

	jQuery.each( gjmMapObjects, function( index, mapObject ) {

		if ( mapObject['triggerMap'] == true ) {
		
			//if map element is hidden show it first
			if ( mapObject['hiddenElement'] != false && jQuery(mapObject['hiddenElement']).is(':hidden') ) {
		
				jQuery(mapObject['hiddenElement']).slideToggle( 'fast', function() {
					gjmMapInit( mapObject, index );
				});

			} else {

				gjmMapInit( mapObject, index );
			} 	
		}
	});		
}
*/