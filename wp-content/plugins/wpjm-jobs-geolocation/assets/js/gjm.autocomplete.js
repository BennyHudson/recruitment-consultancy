jQuery( document ).ready( function($) {

    if ( typeof gjmAddressAutocompleteFields != 'undefined' ) {
        
        //do it for each autocomplete field in the form
        $.each( gjmAddressAutocompleteFields, function( index, field ) {

            inputField = document.getElementById( field );

            var options = {
                types : [gjmOptions.autocomplete_results_type]
            };

            if ( gjmOptions.autocomplete_country != '' ) {
                options.componentRestrictions = { 
                    country : gjmOptions.autocomplete_country 
                }
            } 

            var autocomplete = new google.maps.places.Autocomplete( inputField, options );
            
            google.maps.event.addListener( autocomplete, 'place_changed', function(e) {

                var place = autocomplete.getPlace();

                if ( ! place.geometry ) {
                    return;
                }

            });
        });   
    }   
});