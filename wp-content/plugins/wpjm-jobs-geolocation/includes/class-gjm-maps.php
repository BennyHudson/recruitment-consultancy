<?php 
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Deprecated function
 * @param  [type] $atts [description]
 * @return [type]       [description]
 */
function gjm_get_results_map( $atts ) {

	_deprecated_function( 'gjm_get_results_map', '1.9', 'gjm_get_map_element' );
	
	return GJM_Maps::get_map_element( $atts );
}

/**
 * Map functions class
 */
class GJM_Maps { 

	/**
	 * Generate map element
	 *  
	 * @param  array $args 
	 * @return map element      
	 */
	public static function get_map_element( $atts ) {

		//default map settings
		$args = shortcode_atts( array(
			'map_id'		   => rand( 5, 100 ),
			'prefix'		   => 'gjm',
			'map_width'        => '100%',
			'map_height'       => '300px',
			'init_map_show'    => true,
			'user_marker'      => 'https://maps.google.com/mapfiles/ms/icons/blue-dot.png',
			'zoom_level'	   => 'auto',
			'max_zoom_level'   => '',
			'map_type'		   => 'ROADMAP',
			'scrollwheel'	   => 1,
			'group_markers'    => 'normal',
			'clusters_path'    => 'https://raw.githubusercontent.com/googlemaps/js-marker-clusterer/gh-pages/images/m',
			'map_name'	       => 'results-map'
		), $atts );

		$display  = empty( $args['init_map_show'] ) ? 'display:none;' : '';
		$args     = apply_filters( $args['prefix'].'_map_output_args', $args );
		$prefix   = esc_attr( $args['prefix'] );
		$map_id   = esc_attr( $args['map_id'] );
		$map_name = ! empty( $args['map_name'] ) ? esc_attr( $args['map_name'] ) : 'results-map';

	   	$args = array_map( 'esc_attr', $args );

	    $output['open'] = '<div id="'.$prefix.'-map-wrapper-'.$map_id.'" class="'.$prefix.'-map-wrapper map-wrapper '.$map_name.'" data-map_id="'.$map_id.'" style="'.$display.' width:'.esc_attr( $args['map_width'] ).';height:'.esc_attr( $args['map_height'] ).'">';
	    $output['resize_toggle'] = '<span id="'.$prefix.'-resize-map-toggle-'.$map_id.'" class="'.$prefix.'-resize-map-toggle gjm-icon-resize-full" style="display:none;" title="'.__( 'Resize map','GMW' ) .'"></span>';
	    $output['map'] = '<div id="'.$prefix.'-map-'.$map_id.'" data-map-id="'.$map_id.'" class="'.$prefix.'-map" style="width:100%; height:100%" data-user_marker="'.esc_url( $args['user_marker'] ).'" data-zoom_level="'.esc_attr( $args['zoom_level'] ).'" data-max_zoom_level="'.esc_attr( $args['max_zoom_level'] ).'" data-map_type="'.esc_attr( $args['map_type'] ).'" data-scrollwheel="'.esc_attr( $args['scrollwheel'] ).'" data-group_markers="'.esc_attr( $args['group_markers'] ).'" data-clusters_path="'.esc_url( $args['clusters_path'] ).'"></div>';
	    $output['loader'] = '<i id="'.$prefix.'-map-loader-'.$map_id.'" class="'.$prefix.'-map-loader gjm-icon-spin-thin animate-spin"></i>';
	    $output['close'] = '</div>';
				
	    //modify the map element
	    $output = apply_filters( $args['prefix']."_map_output", $output, $args );
	    
	    
	    // enqueue grouping scripts
	    self::enqueue_scripts( $args );

	    return implode( ' ', $output );
	}

	/**
	 * Enqueue map scripts
	 * 
	 * @param  [type] $args [description]
	 * @return [type]       [description]
	 */
	public static function enqueue_scripts( $args ) {

		//enqueue the map script
		if ( ! wp_script_is( 'gjm-map', 'enqueued' )  ) {	
			wp_enqueue_script( 'gjm-map' );
		}

		if ( ! wp_script_is( 'gjm-info-bubble', 'enqueued' )  ) {	
			wp_enqueue_script( 'gjm-info-bubble', GJM_URL . '/assets/js/lib/info-bubble/info.bubble.min.js', array( 'jquery' ), GJM_VERSION, true );
		}

		//Load marker clusterer library when needed. It might already loaded by GEO my WP or Jobify theme.
	    //When that is the case we will use the Jobify clusters
		if ( $args['group_markers'] == 'markers_clusterer' && ! wp_script_is( 'gjm-marker-clusterer', 'enqueued' ) && ! wp_script_is( 'gmw-marker-clusterer', 'enqueued' ) && ! wp_script_is( 'jobify-app-map', 'enqueued' ) ) {	
			wp_enqueue_script( 'gjm-marker-clusterer', GJM_URL . '/assets/js/lib/marker-clusterer/marker.clusterer.min.js', array( 'jquery' ), GJM_VERSION, true );
		}

		//load marker clusterer library - to be used with premium features
		if ( $args['group_markers'] == 'markers_spiderfier' && ! wp_script_is( 'gjm-marker-spiderfier', 'enqueued' ) && ! wp_script_is( 'gmw-marker-spiderfier', 'enqueued' ) ) {	
			wp_enqueue_script( 'gjm-marker-spiderfier', GJM_URL . '/assets/js/lib/marker-spiderfier/marker.spiderfier.min.js', array( 'jquery' ), GJM_VERSION, true );
		}
	}

	/**
	 * Generate the map aruments
	 * 
	 * @param  array  $map_args      [description]
	 * @param  array  $map_options   [description]
	 * @param  array  $locations     [description]
	 * @param  array  $user_position [description]
	 * @return [type]                [description]
	 */
	public static function get_map_object( $map_args = array(), $map_options = array(), $locations = array(), $user_position = array() ) {
		
		global $gjm_maps_elements;
		
		//check if global already set
		if ( empty( $gjm_maps_elements ) ) {
			$gjm_maps_elements = array();
		}

		// randomize map ID if not exists
		$map_id = ! empty( $map_args['map_id'] ) ? $map_args['map_id'] : rand( 100, 1000 );

		// default map args
		$default_map_args = array(
			'map_id' 	 		=> $map_id,
			'prefix'			=> 'gjm',
			'zoom_position'		=> false
		);

		// merge default with incoming map args
		$map_args = array_merge( $default_map_args, $map_args );

		// default map options
		$default_map_options = array(
			'backgroundColor' 		 => '#f1f1f1',			
			'disableDefaultUI' 		 => false,
			'disableDoubleClickZoom' => false,
			'draggable'				 => true,
			'draggableCursor'		 => '',
			'draggingCursor'		 => '',
			'fullscreenControl'      => false,
			'keyboardShortcuts'		 => true,
			'mapMaker'				 => false,
			'mapTypeControl' 		 => true,
			'mapTypeControlOptions'	 => true,
			'mapTypeId'				 => 'ROADMAP',
			'maxZoom'		 		 => null,
			'minZoom'		 		 => null,
			'zoomLevel'				 => 13,
			'noClear'				 => false,
			'rotateControl'		     => true,
			'scaleControl'			 => true,
			'scrollwheel'	 		 => true,
			'streetViewControl' 	 => true,
			'styles'				 => null,
			'tilt'					 => null,
			'zoomControl'	 		 => true,
			'resizeMapControl'		 => true,
			'panControl'			 => true
		);

		$map_options = array_merge( $default_map_options, $map_options );

		// default user position
		$default_user_position = array(
			'lat'		 => false,
			'lng'		 => false,
			'address' 	 => false,
			'iw_content' => null,
			'iw_open'	 => false
		);

		// if user position exists, merge it with default
		if ( ! empty( $user_position['lat'] ) ) {

			$user_position = array_merge( $default_user_position, $user_position );
		
		} else {

			$user_position = $default_user_position;
		}
		
		// push the map args into the global array of maps
		$gjm_maps_elements[$map_id] = array(
			'args'		    => $map_args,
			'map_options'   => $map_options,
			'locations'		=> $locations,
			'user_position' => $user_position
		);

		// allow plugins modify the map args
		$gjm_maps_elements[$map_id] = apply_filters( 'gmw_map_element', $gjm_maps_elements[$map_id], $map_id );
		
		GJM_Init::enqueue_google_maps_api();

		//enqueue the map script
		//if ( ! wp_script_is( 'gjm-map', 'enqueued' )  ) {	
		//	wp_enqueue_script( 'gjm-map' );
		//}

		//pass the mapObjects to JS
		wp_localize_script( 'gjm-map', 'gjmMapObjects', $gjm_maps_elements );
		
		return $gjm_maps_elements[$map_id];
	}

	/**
	 * Info-window content
	 * 
	 * @param unknown_type $post
	 */
	public static function get_info_window_content( $post ) {

		//get the address
		if ( ! empty( $post->formatted_address ) ) {
			
			$address = $post->formatted_address;
		
		} elseif ( ! empty( $post->address ) ) {
		
			$address = $post->address;
		
		} else {
			
			$address = get_post_meta( $post->ID, 'geolocation_formatted_address', true );
		}

		if ( empty( $address ) ) {
			$address = __( 'N/A', 'GJM' );		
		}

		$output['title'] = '<a class="title" href="'.get_permalink( $post->ID ).'" title="'. esc_attr( $post->post_title ).'">'. esc_attr( $post->post_title ). '</a>';
		
		$output['address'] = '<span class="location gjm-icon-location">' . esc_attr( $address ) . '</span>';

		$additional_info = apply_filters( 'gjm_info_window_additional_info', false, $post );

		// additional data disabled by default to keep the info window minimal and clean.
		if ( $additional_info != false && is_array( $additional_info ) ) {
			
			$output = array_merge( $output, $additional_info );
		}

		if ( isset( $post->distance ) ) {
			$output['distance']= '<span class="distance">'.esc_attr( $post->distance .' '.$post->units ).'</span>';
		}
				
		$output = apply_filters( 'gjm_info_window_content', $output, $post );

		return implode( '', $output );
	}
}