<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Global map class
 * 
 * @since 1.7
 */
class GJM_Global_Map {

	/**
	 * @since 1.7
	 * Public $args
	 * Array of Incoming arguments
	 */
	protected $args = array(
		'element_id'	  => 0,
		'addon'			  => 'gjm',
		'post_types'	  => 'job_listing',
		'prefix'		  => 'gjm',
		'map_height'      => '400px',
		'map_width'       => '100%',
		'map_type'        => 'ROADMAP',
		'results_count'	  => 200,
		'scroll_wheel'	  => 1,
		'group_markers'	  => 'markers_clusterer',
		'user_marker' 	  => 'https://maps.google.com/mapfiles/ms/icons/blue-dot.png',
		'location_marker' => 'https://maps.google.com/mapfiles/ms/icons/red-dot.png',
		'clusters_path'   => 'https://raw.githubusercontent.com/googlemaps/js-marker-clusterer/gh-pages/images/m'
	);

	/**
	 * @since 1.7
	 * Public $args
	 * Array for child class to extends the main array above
	 */
	protected $ext_args = array();

	/**
	 * [__construct description]
	 * @param array $atts [description]
	 */
	function __construct( $atts=array() ) {
		
		//extend the default args
		$this->args = array_merge( $this->args, $this->ext_args );
		
		//get the shortcode atts
		$this->args = shortcode_atts( $this->args, $atts );
		
		//set random element id if not exists
		$this->args['element_id'] = ! empty( $this->args['element_id'] ) ? $this->args['element_id'] : rand( 10, 1000 );
	}
	
	/**
	 * Output global map
	 * @return void
	 */
	public function display() {
		
		$query_args = array(
			'post_type'           => array( $this->args['post_types'] ),
			'post_status'         => 'publish',
			'ignore_sticky_posts' => 1,
			'posts_per_page'      => $this->args['results_count'],
			'tax_query'           => array(),
			'meta_query'          => array()
		);

		add_filter( 'posts_clauses', array( $this, 'query_clauses' ) );
		
		$results = new WP_Query( apply_filters( $this->args['prefix'].'_global_map_args', $query_args, $this->args ) );

		remove_filter( 'posts_clauses', array( $this, 'query_clauses' ) );
				
		//if no results, abort!
		if ( empty( $results->posts ) ) {
			return;
		}

		// loop through results and add info window content to each $post object
		foreach ( $results->posts as $key => $post ) {	

			// info window content	
			$results->posts[$key]->info_window_content = $this->args['prefix'] == 'gjm' ? GJM_Maps::get_info_window_content( $post, true ) : grm_info_window_content( $post, true );
			
			// map icon
			$map_icon = ! empty( $this->args['location_marker'] ) ? $this->args['location_marker'] : 'https://maps.google.com/mapfiles/ms/icons/red-dot.png';
			$results->posts[$key]->map_icon = apply_filters( 'gjm_global_map_icon', $map_icon, $post );
		}

		//map arguments
    	$map_args = array(
    		'map_id'	 	 => $this->args['element_id'],
			'prefix'	 	 => $this->args['prefix'],
			'map_width'  	 => $this->args['map_width'],
			'map_height' 	 => $this->args['map_height'],
			'init_map_show'	 => true,
			'user_marker'    => $this->args['user_marker'],
			'zoom_level'	 => 'auto',
			'map_type'		 => $this->args['map_type'],
			'scrollwheel'	 => $this->args['scroll_wheel'],
			'group_markers'  => $this->args['group_markers'],
			'clusters_path'	 => $this->args['clusters_path'],
			'map_name'	     => 'global-map'
    	);

    	//Creae the map element
		$map = GJM_Maps::get_map_element( $map_args );
				
		$map_args = array(
    		'map_id' => $this->args['element_id'],
            'prefix' => $this->args['prefix']
        );
        
        $map_args = GJM_Maps::get_map_object( $map_args, array(), $results->posts, array() );
		
        return $map;
	}
		
	/**
	 * Global Map clauses
	 * 
	 * Modify the wp_query to display jobs with locations
	 * @param  object $clauses WP_Query clauses
	 * @return void          
	 */
	public function query_clauses( $clauses ) {
		
		global $wpdb;
		
		// join the location table into the query
		$clauses['join']   .= " INNER JOIN " . $wpdb->prefix . "places_locator gjmLocations ON $wpdb->posts.ID = gjmLocations.post_id ";
		$clauses['fields']  = "{$wpdb->posts}.*, {$wpdb->posts}.post_title, gjmLocations.lat, gjmLocations.long as lng, gjmLocations.address, gjmLocations.formatted_address ";
		$clauses['where']  .= " AND ( gjmLocations.lat != '0.000000' AND gjmLocations.long != '0.000000' ) ";
		
		return apply_filters( $this->args['prefix'].'_global_map_query_clauses', $clauses );	
	}
}