jQuery(document).ready(function($) { 
					
	if ( $( '#alert_location' ).length ) {
		
		var locationFieldset = $( '#alert_location' ).closest( 'fieldset' );
		var distanceElement  = '';
		
		distanceElement += '<fieldset id="gjm-alerts-location">';
		distanceElement += '<label for="gjm_alert_distance">' + gjmFormParams.labels.field_label + '</label>';
		distanceElement += '<div class="field">';

		if ( gjmFormParams.global_radius == 'user' ) { 
			
			distanceElement += '<input type="text" name="gjm_alert_distance" value="' + gjmFormParams.distance + '" id="gjm_alert_distance" class="input-text ' + gjmFormParams.div_size + '" placeholder="' + gjmFormParams.labels.distance_placeholder + '" />';

		}

		if ( gjmFormParams.global_units == 'user' ) { 

			distanceElement += '<select name="gjm_alert_units" id="gjm_alert_units" class="' + gjmFormParams.div_size + '">';
			distanceElement += '<option value="imperial">' + gjmFormParams.labels.miles + '</option>';
			distanceElement += '<option ' + gjmFormParams.selected + ' value="metric">' + gjmFormParams.labels.kilometers + '</option>';
			distanceElement += '</select>';
			
		}

		distanceElement += '</div>';
		distanceElement += '</fieldset>';

		$( distanceElement ).insertAfter( locationFieldset );
	}
});