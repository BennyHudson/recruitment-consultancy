<?php
/**
 * GJM Alerts Class
 *
 * Add geolocation features to WP Job Manager Alerts
 */
class GJM_Alert_Query_Class {

	/**
	 * [__construct description]
	 */
	public function __construct() {

		// admin settings
		add_filter( 'wp_job_manager_alerts_settings', array( $this, 'admin_settings' ), 50 );

		// check if geolocation feature enabled
		$this->geo_enabled = get_option( 'gjm_alerts_geolocation_enabled' );
	
		// abort if geolocation for alerts is disabled
		if ( empty( $this->geo_enabled ) ) {
			
			return;
		}

		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
		add_action( 'wp_footer', array( $this, 'alert_form_fields' ) );
		add_filter( 'job_manager_alerts_template', array( $this, 'replace_email_tags' ), 50, 4 );
		add_action( 'save_post_job_alert', array( $this, 'gjm_save_job_alert' ) );
		add_filter( 'job_manager_alerts_get_job_listings_args', array( $this, 'gjm_job_alert_query_args' ), 50, 2 );
	}
	
	public function enqueue_scripts() {
		wp_register_script( 'gjm-alerts', GJM_URL .'/includes/3rd-party/wpjm-alerts/assets/js/gjm.alerts.min.js', array( 'jquery' ), GJM_VERSION, true );
	}

	/**
	 * Add email tags to setting description
	 * 
	 * @param  [type] $args [description]
	 * @return [type]       [description]
	 */
	public function admin_settings( $args ) {

		// add email tags to description only if geolocation enabled
		if ( ! empty( $this->geo_enabled ) ) {
		
			$args[0]['desc'] .= '<code>{alert_location}</code>' . ' - ' . __( 'The location of the alert', 'gjm' ) . '<br/>' .
				'<code>{alert_distance}</code>' . ' - ' . __( 'The distance of the alert', 'wp-job-manager-alerts' ) . '<br/>';
		}

		$args[] = array(
			'name' 		=> 'gjm_alerts_geolocation_enabled',
			'std' 		=> '0',
			'label' 	=> __( 'Alerts Geolocation', 'gjm' ),
			'cb_label' 	=> __( 'Enable', 'gjm' ),
			'desc'		=> __( 'Enable geolocation for alerts. This feature allows to filter jobs alerts based on location and distance.', 'gjm' ),
			'type'      => 'checkbox'
		);

		$args[] = array(
			'name' 		=> 'gjm_alerts_address_autocomplete',
			'std' 		=> '0',
			'label' 	=> __( 'Address autocomplete', 'gjm' ),
			'cb_label' 	=> __( 'Enable', 'gjm' ),
			'desc'		=> __( 'Enable Google Places address autocomplete in the Location fields of the Alerts form.', 'gjm' ),
			'type'      => 'checkbox'
		);

		$args[] = array(
			'name' 		   => 'gjm_alerts_radius',
			'std' 		   => '',
			'placeholder'  => 'radius',
			'label' 	   => __( 'Radius', 'gjm' ),
			'desc'		   => __( 'Set the radius for the alerts. There are 3 options:
				<ol style="font-style: italic;font-size: 13px;margin: 3px 20px;color: #666">
				    <li>Enter a numeric value that will be used as the global radius for all the alerts.</li>
				    <li>Leave the input box blank to search without a radius.</li>
				    <li>Type in <code>user</code> to let the users set the value when editing or creating a new alert.</li>
				</ol>', 'gjm' ),
			'type'         => 'text'
		);
	
		$args[] = array(
			'name' 		=> 'gjm_alerts_units',
			'std' 		=> 'imperial',
			'label' 	=> __( 'Units', 'gjm' ),
			'desc'		=> __( 'Choose between miles or kilometers to be used as a global value for all the alerts, or select "User defined" to let the users set the value when editing or creating a new alert.', 'gjm' ),
			'type'      => 'select',
			'options' => array(
				'imperial' => 'Miles',
				'metric'   => 'Kilometers',
				'user'	   => __( 'User defined', 'gjm' )
			)
		);
	
		return $args;
	}

	/**
	 * Replace email tags in email notifications
	 * 
	 * @param  [type] $template [description]
	 * @param  [type] $alert    [description]
	 * @param  [type] $user     [description]
	 * @param  [type] $jobs     [description]
	 * @return [type]           [description]
	 */
	public function replace_email_tags( $template, $alert, $user, $jobs ) {

		$global_radius = get_option( 'gjm_alerts_radius' );
		$global_units  = get_option( 'gjm_alerts_units' );

		// get location from custom fields
		$alert_location = get_post_meta( $alert->ID, 'alert_location', 1 );

		// get radius
	    if ( $global_radius != 'user' ) {

	    	$alert_radius = $global_radius;

	    } else {

			$alert_radius = get_post_meta( $alert->ID, 'gjm_alert_distance', 1 );
		}

		// get radius
	    if ( $global_units != 'user' ) {

	    	$alert_units = $global_units;

	    } else {

			$alert_units = get_post_meta( $alert->ID, 'gjm_alert_units', 1 );
		}

		$alert_units = ( ! empty( $alert_units ) && $alert_units == 'metric' ) ? 'km' : 'mi';

		// email tags
		$replacements = array(
			'{alert_location}' => ! empty( $alert_location ) ? esc_html( $alert_location ) : '',
			'{alert_distance}' => ! empty( $alert_radius ) ? esc_html( $alert_radius . $alert_units ) : '',
		);

		// replace tags
		$template = str_replace( array_keys( $replacements ), array_values( $replacements ), $template );

		return $template;
	}

	/**
	 * Add geo fields to alert form
	 * 
	 * @return [type] [description]
	 */
	public function alert_form_fields() {

		// check if in alerts form page ( edit or new )
		if ( ! empty( $_REQUEST['action'] ) && ( ( $_REQUEST['action'] == 'add_alert' ) || ( $_REQUEST['action'] == 'edit' && ! empty( $_REQUEST['alert_id'] ) ) ) ) {

			// get global values
			$global_radius = get_option( 'gjm_alerts_radius' );
			$global_units  = get_option( 'gjm_alerts_units' );

			// abort if user not allow to override values
			if ( $global_radius != 'user' && $global_units != 'user' ) {
				return;
			}

			$distance = '';
			$units    = 'imperial';

			// get submitted values if submission failed 
			if ( ! empty( $_POST['submit-job-alert'] ) && $_POST['submit-job-alert'] == 'Save alert' ) {

				$distance = ! empty( $_POST['gjm_alert_distance'] ) ? $_POST['gjm_alert_distance'] : '';
				$units    = ! empty( $_POST['gjm_alert_units'] ) ? $_POST['gjm_alert_units'] : 'imperial';

			} else if ( ! empty( $_GET['action'] ) && $_GET['action'] == 'add_alert' ) {

				$distance = ! empty( $_GET['alert_gjm_radius'] ) ? $_GET['alert_gjm_radius'] : '';

			// get values saved in custom fields if editing alert
			} elseif ( $_REQUEST['action'] == 'edit' ) {

				$alert_id = $_REQUEST['alert_id'];
				$distance = get_post_meta( $alert_id, 'gjm_alert_distance', true );	
				$units    = get_post_meta( $alert_id, 'gjm_alert_units', true );	
			}

			$selected = ( $units == 'metric' ) ? 'selected="selected"' : '';

			if ( $global_units == 'user' && $global_radius == 'user' ) {
				$div_size = 'half';
			} else {
				$div_size = 'full';
			}

			// labels
			$form_labels = apply_filters( 'gjm_alerts_form_labels', array(
				'field_label' 		   => __( 'Distance', 'gjm' ),
				'distance_placeholder' => __( 'Optionally define a radius to search within', 'gjm' ),
				'miles' 			   => __( 'Miles', 'gjm' ),
				'kilometers' 		   => __( 'Kilometers', 'gjm' )
			) );

			$form_labels = array_map( 'esc_attr', $form_labels );
			
			// aruments to pass to the JS file.
			$form_params = array(
				'labels'        => $form_labels,
				'global_radius' => $global_radius,
				'global_units'  => $global_units,
				'distance'		=> sanitize_text_field( $distance ),
				'div_size'		=> $div_size,
				'selected'		=> $selected
			);

			wp_localize_script( 'gjm-alerts', 'gjmFormParams', $form_params );	
			wp_enqueue_script( 'gjm-alerts' );

			$this->google_address_autocomplete();
		}
	}

	/**
	 * Enable address autocomplete in Alerts form
	 * 
	 * @return [type] [description]
	 */
	public function google_address_autocomplete() {

		$settings = get_option( 'gjm_options' );

		if ( get_option( 'gjm_alerts_address_autocomplete' ) == '' ) { 
			return;
		}
		
		GJM_Init::enqueue_google_maps_api();

		wp_enqueue_script( 'gjm-autocomplete');
	}

	/**
	 * Save geo values on alert form subission
	 * 
	 * @param  [type] $post_id [description]
	 * @return [type]          [description]
	 */
	public function gjm_save_job_alert( $post_id ) {

		// verify nonce
		if ( empty( $_REQUEST['action'] ) || empty( $_REQUEST['_wpnonce'] ) || ! wp_verify_nonce( $_REQUEST['_wpnonce'], 'job_manager_alert_actions' ) ) {

			return;
		}

	    $alert_id 	   = $post_id;
	    $global_radius = get_option( 'gjm_alerts_radius' );
		$global_units  = get_option( 'gjm_alerts_units' );
 
	    /**
	     * 
	     * get distance and units values and save them in custom fields
	   	 * if user can enter values we get them from the form submission
	     * otherwise, we use the global values in the settings page
	     * get radius
	     * 
	    **/
	    if ( $global_radius != 'user' ) {

	    	$radius = $global_radius;    
	    
	    } else {
	    
	    	$radius = ( ! empty( $_POST['gjm_alert_distance'] ) && is_numeric( $_POST['gjm_alert_distance'] ) ) ? $_POST['gjm_alert_distance'] : '';
	    }

	    // get units
	    if ( $global_units != 'user' ) {

	    	$units = $global_units;

	    } else {

			$units = ( ! empty( $_POST['gjm_alert_units'] ) && $_POST['gjm_alert_units'] == 'metric' ) ? 'metric' : 'imperial';
		}

		update_post_meta( $alert_id, 'gjm_alert_distance', sanitize_text_field( $radius ) );
		update_post_meta( $alert_id, 'gjm_alert_units', $units );

		// Abort if no location entered
	    if ( empty( $_POST['alert_location'] ) ) {

			delete_post_meta( $alert_id, 'gjm_alert_lat' );
			delete_post_meta( $alert_id, 'gjm_alert_lng' );

			return;
	    }

	    $location = $_POST['alert_location'];

	    // geocoder address
	    $geocoded_location = WP_Job_Manager_Geocode::get_location_data( $location );

	    // if no location returned abort
	    if ( is_wp_error( $geocoded_location ) || empty( $geocoded_location['lat'] ) || empty( $geocoded_location['long'] )) {

	    	delete_post_meta( $alert_id, 'gjm_alert_lat' );
			delete_post_meta( $alert_id, 'gjm_alert_lng' );
			
			return;
	    }
		
		// save coords in custom fields
		update_post_meta( $alert_id, 'gjm_alert_lat', sanitize_text_field( $geocoded_location['lat'] ) );
		update_post_meta( $alert_id, 'gjm_alert_lng', sanitize_text_field( $geocoded_location['long'] ) );
	}

	/**
	 * gjm_job_alert_query_args description
	 * 
	 * @param  [type] $args  [description]
	 * @param  [type] $alert [description]
	 * @return [type]        [description]
	 */
	public function gjm_job_alert_query_args( $args, $alert ) {

		add_filter( 'get_job_listings_query_args', array( $this, 'search_query' ), 99, 2 );

		$global_radius = get_option( 'gjm_alerts_radius' );
		$global_units  = get_option( 'gjm_alerts_units' );

		$this->alert_id  = $alert->ID;
		$this->alert_lat = get_post_meta( $alert->ID, 'gjm_alert_lat', 1 );
		$this->alert_lng = get_post_meta( $alert->ID, 'gjm_alert_lng', 1 );

		// get radius
	    if ( $global_radius != 'user' ) {

	    	$this->alert_radius = $global_radius;

	    } else {

			$this->alert_radius = get_post_meta( $alert->ID, 'gjm_alert_distance', 1 );
		}

		// get radius
	    if ( $global_units != 'user' ) {

	    	$this->alert_units = $global_units;

	    } else {

			$this->alert_units = get_post_meta( $alert->ID, 'gjm_alert_units', 1 );
		}

		$this->alert_units = ( ! empty( $this->alert_units ) && $this->alert_units == 'metric' ) ? 'metric' : 'imperial';
				
		add_filter( 'the_job_location', array( $this, 'job_distance' ), 99, 2 );

		return $args;
	}

	/**
	 * Do some distance quering
	 * @since  1.0
	 * 
	 * @author Eyal Fitoussi
	 */
	public function search_query( $query_args, $args ) {
						
		if ( empty( $args['search_location'] ) ) {
			return $query_args;
		}
		
		// if no coords exists in custom fields try to geocode the address
		if ( empty( $this->alert_lat ) || empty( $this->alert_lng ) ) {

			if ( ! class_exists( 'WP_Job_Manager_Geocode' ) ) {
				return $query_args;
			}

			$this->geocoded = WP_Job_Manager_Geocode::get_location_data( $args['search_location'] );

			if ( ! is_wp_error( $this->geocoded ) && ! empty( $this->geocoded['lat'] ) && ! empty( $this->geocoded['long'] ) ) {
				
				$this->alert_lat = $this->geocoded['lat'];
				$this->alert_lng = $this->geocoded['long'];

			} else {
				
				return $query_args;
			}
		}

		// add location args to query to be used in cache
		$query_args['gjm']['location'] = $args['search_location'];
		$query_args['gjm']['units']    = $this->alert_units;
		$query_args['gjm']['radius']   = $this->alert_radius;

		// disable the location query made by WP Job Manager
		unset( $query_args['meta_query'][0] );

		// default values
		$location_query_args = array(
			'post_type' => 'job_listing',
			'lat'		=> $this->alert_lat,
			'lng'		=> $this->alert_lng,
			'radius'	=> $this->alert_radius,
			'units'		=> $this->alert_units,
		);

		// get jobs based on location and address
		$locations_data = GJM_Form_Query_Class::get_locations_data( $location_query_args );

		// get locations data
		$this->locations_data = $locations_data['data'];

    	// When searching based on location, we need to include the locatios post ID in the WP_Query.
    	// This is to restricts the query to only show posts with location and exclude the "Anywhere" jobs.
    	// Otherwise, we skip this and the query will include all jobs, with or without location. 
		if ( ! empty( $this->alert_lat ) && ! empty( $this->alert_lng ) ) {

			// pass the locations post ID into the WP_Query
			$query_args['post__in'] = ! empty( $locations_data['posts_id'] ) ? $locations_data['posts_id'] : array( '0' );

			// when sorting by distance we pass "post__in" to the orderby arg. 
			// This way the query will sort the results based on the locations post ID, which are ordered
			// by the distance.
			//if ( $this->filters[$this->prefix.'_orderby'] == 'distance' ) {

				$query_args['orderby'] = 'post__in';
			//}
		}		
		
		remove_filter( 'get_job_listings_query_args', array( $this, 'search_query' ), 99, 2 );

		return $query_args;
	}

	/**
	 * GJM Function - add distance value to results
	 * @since  1.0
	 * @author Eyal Fitoussi
	 */
	public function job_distance( $location, $post ) {

		if ( empty( $this->locations_data[$post->ID]->distance ) ) {
			return $location;
		}

		$location_data = $this->locations_data[$post->ID];
		
		return  $location . ' ( ' . esc_attr( $location_data->distance . $location_data->units ) . ' ) ';
	}
}
new GJM_Alert_Query_Class();