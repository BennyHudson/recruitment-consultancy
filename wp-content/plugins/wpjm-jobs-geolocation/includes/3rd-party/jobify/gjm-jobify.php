<?php
/**
 * GJM Alerts Class
 *
 * Add geolocation features to WP Job Manager Alerts
 */
class GJM_Jobify_Integration_Class {

	/**
	 * [__construct description]
	 */
	public function __construct() {

		// admin settings
		add_filter( 'job_manager_settings', array( $this, 'admin_settings' ), 50 );
		add_action( 'job_manager_job_filters_search_jobs_end', array( $this, 'hero_geolocation' ), 9 );
	}
	
	/**
	 * Add email tags to setting description
	 * 
	 * @param  [type] $args [description]
	 * @return [type]       [description]
	 */
	public function admin_settings( $args ) {

		//search form options
		$args['gjm_jobify_hero'] = array(
			__( 'Geo Jobify Hero', 'GJM' ),
			array(
				array(
					'name'        => 'gjm_jobify_hero_enabled',
					'std'         => '1',
					'label'       => __( 'Hero Search Geolocation', 'GJM' ),
					'cb_label'    => __( 'Enable', 'GJM' ),
					'desc'        => __( 'Enable geolocation for Hero search on the home page of the Jobify theme.' , 'GJM' ),
					'type'        => 'checkbox',
					'attributes'  => array()
				),
				array(
					'name'        => 'gjm_jobify_auto_locator',
					'std'         => '1',
					'label'       => __( 'Page Load Locator', 'GJM' ),
					'cb_label'    => __( 'Enable', 'GJM' ),
					'desc'        => __( 'When enabled, the browser will try to detect the user\'s current position on page load, and dynamically submit the search form using this location.' , 'GJM' ),
					'type'        => 'checkbox',
					'attributes'  => array()
				),
				array(
					'name'        => 'gjm_jobify_autocomplete',
					'std'         => '1',
					'label'       => __( 'Address Autocomplete', 'GJM' ),
					'cb_label'    => __( 'Enable', 'GJM' ),
					'desc'        => __( 'Enable suggested results by Google Places while typing an address in the location field.', 'GJM' ),
					'type'        => 'checkbox',
					'attributes'  => array()
				),
				array(
					'name'        => 'gjm_jobify_locator_button',
					'std'         => '0',
					'label'       => __( 'Locator Button', 'GJM' ),
					'cb_label'    => __( 'Enable', 'GJM' ),
					'desc'        => __( 'Disply a locator button, inside the location field, that will dynamically retrive the visitor\'s current position.', 'GJM' ),
					'type'        => 'checkbox',
					'attributes'  => array()
				),
				array(
					'name'        => 'gjm_jobify_radius',
					'std'         => '5,10,15,25,50,100',
					'placeholder' => '',
					'label'       => __( 'Radius', 'GJM' ),
					'desc'        => __( 'Enter a single value to be used as the default value, or multiple values comma separated, to displaed as a select box in the search form.', 'GJM' ),
					'type'		  => 'text',
					'attributes'  => array( 'size' => '25' )
				),
				array(
					'name'        => 'gjm_jobify_units',
					'std'         => 'both',
					'placeholder' => '',
					'label'       => __( 'Units', 'GJM' ),
					'desc'        => __( 'Select a single unit value to be used as the default, or select "Both" to display both units in a select box in the search form.', 'GJM' ),
					'type'        => 'select',
					'options'	  => array( 
						'both' 		=> 'Both', 
						'imperial' 	=> 'Miles', 
						'metric' 	=> 'Kilometers' ),
					'attributes'  => array()
				),
				array(
					'name'        => 'gjm_jobify_orderby',
					'type'		  => 'text',
					'std'         => 'distance,title,featured,date',
					'placeholder' => '',
					'label'       => __( 'Orderby', 'GJM' ),
					'desc'        => __( 'Enter a single value that will be used as the default, or multiple values, comma separated and in the order which you would like to display them, to display an orderby select box in the jobs search form.', 'GJM' ),
					'attributes'  => array( 'size' => '25' )
				),					
			),
		);

		return $args;
	}

	/**
	 * Enable Geolocation for Hero Search
	 * 
	 * @param  [type] $atts [description]
	 * @return [type]       [description]
	 */
	public function hero_geolocation( $atts ) {

		if ( get_option( 'gjm_jobify_hero_enabled' ) && is_front_page() ) {
			
			$auto_locator = get_option( 'gjm_jobify_auto_locator' );
			$autocomplete = get_option( 'gjm_jobify_autocomplete' );
			$locator_btn  = get_option( 'gjm_jobify_locator_button' );
			$radius       = get_option( 'gjm_jobify_radius' );
			$units        = get_option( 'gjm_jobify_units' );
			$orderby      = get_option( 'gjm_jobify_orderby' );

			echo do_shortcode( '[gjm_form_geolocation_features gjm_use="1" gjm_auto_locator="'.$auto_locator.'" gjm_autocomplete="'.$autocomplete.'" gjm_locator_button="'.$locator_btn.'" gjm_radius="'.$radius.'" gjm_units="'.$units.'" gjm_orderby="'.$orderby.'"]' );

			remove_action( 'job_manager_job_filters_search_jobs_end', array( $this, 'hero_geolocation' ), 9 );
		}
	}
}
new GJM_Jobify_Integration_Class();