<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * GJM Labels
 * 
 * @return labels to be used in the plugin. Labels can be modified using the filter 'gjm_form_labels'
 */
function gjm_labels() {
	
	$labels = apply_filters( 'gjm_form_labels', array(
		'miles' 	 => __( 'Miles', 'GJM' ),
		'kilometers' => __( 'Kilometers', 'GJM' ),
		'within'	 => __( 'Within', 'GJM' ),

		'orderby_filters'	=> array(
			'label'	 	 => __( 'Order by', 'GJM' ),
			'distance' 	 => __( 'Distance', 'GJM' ),
			'title' 	 => __( 'Title', 'GJM' ),
			'featured' 	 => __( 'Featured', 'GJM' ),
			'date' 		 => __( 'Date', 'GJM' ),
			'modified'	 => __( 'Last Updated', 'GJM' ),
			'ID'		 => __( 'ID', 'GJM' ),
			'parent'	 => __( 'Parent', 'GJM' ),
			'rand'		 => __( 'Random', 'GJM' ),
			'name'		 => __( 'Name', 'GJM' ),
		),
		'search_result'	=> array(
			'company'	 => __( 'Company:', 'GJM' ),
			'address'    => __( 'Address:', 'GJM' ),
			'job_types'	 => __( 'Job Types:', 'GJM' ),
			'posted'	 => __( 'Posted %s ago', 'GJM' ),
			'mi'		 => __( 'mi', 'GJM' ),
			'km'		 => __( 'km', 'GJM ' )
		),
		'resize_map' => __( 'Resize map', 'GJM' ),
	) ); 

	return $labels;
}

/**
 * Single Job Page map
 * 
 * @return void 
 */
function gjm_single_page_map( $prefix = 'gjm', $post_type = 'job_listing' ) {

	global $post;

	//verfy that we are in a single job post type
	if ( $post->post_type != $post_type ) {
		return;
	}

	$settings = get_option( $prefix.'_options' );
	$settings = $settings['single_page'];
		
	global $wpdb;
	
	$location = $wpdb->get_row( 
		$wpdb->prepare( 
			"SELECT *, `long` as lng FROM {$wpdb->prefix}places_locator WHERE post_id = %d", $post->ID 
		) 
	);

	if ( empty( $location ) || empty( $location->lat ) || empty( $location->long ) || $location->lat == '0.000000' ) {
		return;
	}

	$location->map_icon = $settings[$prefix.'_single_map_location_marker'];

	$element_id = rand( 10, 100 );

	$map_args = array(
		'map_id'		 => $element_id,
		'prefix'		 => $prefix,
		'map_width'  	 => $settings[$prefix.'_single_map_width'],
		'map_height' 	 => $settings[$prefix.'_single_map_height'],
		'init_map_show'  => true,
		'zoom_level'	 => ! empty( $gjm_settings[$prefix.'_single_map_zoom_level'] ) ? $settings[$prefix.'_single_map_zoom_level'] : 12,
		'max_zoom_level' => ! empty( $settings[$prefix.'_single_map_max_zoom_level'] ) ? $settings[$prefix.'_single_map_max_zoom_level'] : '',
		'map_type'		 => $settings[$prefix.'_single_map_type'],
		'scrollwheel'	 => $settings[$prefix.'_single_map_scroll_wheel'],
		'map_name'	     => 'single-page'
	);

	// Generate the map element
	$map = GJM_Maps::get_map_element( $map_args );
			
	// create the map object
    $map_args = array(
		'map_id' => $element_id,
        'prefix' => $prefix
    );

    $map_args = GJM_Maps::get_map_object( $map_args, array(), array( $location ), array() );
	
	// make sure only one single map page will show on the page
	//remove_action( 'job_application_end',      'gjm_single_page_map' );
	//remove_action( 'single_job_listing_start', 'gjm_single_page_map' );
	//remove_action( 'single_job_listing_end',   'gjm_single_page_map' );
	//
    //remove_all_actions( 'gjm_single_page_map' );
    //remove_all_filters( 'gjm_single_page_map' );

	echo $map;
}

/**
 * Single job map shortcode
 * 
 * @return [type] [description]
 */
function gjm_single_page_map_shortcode() {

	ob_start();

	gjm_single_page_map();

	$output = ob_get_contents();

	ob_end_clean();

	return $output;
}
add_shortcode( 'gjm_single_job_map', 'gjm_single_page_map_shortcode' );

/**
 * Load map on single page
 * 
 * @return [type] [description]
 */
function gjm_enable_single_page_map() {

	$gjm_options    = get_option( 'gjm_options' );
	$current_filter = current_filter();

	// in preview submission page
	if ( ! empty( $_POST['submit_job'] ) && $_POST['submit_job'] == 'Preview' ) {

		if ( apply_filters( 'gjm_form_preview_map', true ) && $current_filter == 'single_job_listing_start' ) {

			gjm_single_page_map();
		}

	// in single job page
	} else {

		$single_page_option = $gjm_options['single_page']['gjm_single_map_enabled'];

		if ( ( $single_page_option == 'top' && $current_filter == 'single_job_listing_start' ) || ( $single_page_option == 'bottom' && $current_filter == 'single_job_listing_end' ) ) {
			
			gjm_single_page_map();
		}
	}

	remove_action( 'single_job_listing_start', 'gjm_enable_single_page_map', 50 );
	remove_action( 'single_job_listing_end', 'gjm_enable_single_page_map', 50 );
}
add_action( 'single_job_listing_start', 'gjm_enable_single_page_map', 50 );
add_action( 'single_job_listing_end', 'gjm_enable_single_page_map', 50 );

/**
 * Enable address autocomplete for the Front-end job form
 * 
 * @return [type] [description]
 */
function gjm_address_autocomplete_front_end() {

	$settings = get_option( 'gjm_options' );

	if ( empty( $settings['general_settings']['gjm_address_autocomplete_form_frontend'] ) ) {
		return;
	}

	GJM_Init::enqueue_google_maps_api();

	wp_enqueue_script( 'gjm-autocomplete');
}
add_action( 'submit_job_form_job_fields_start', 'gjm_address_autocomplete_front_end' );

/**
 * Additional details for info window
 *
 * This data was available in previous versions of the plugin, but was removed since 
 * version 2.0 for the purpose of a cleaner info-window and improve performance.
 *
 * To enable this info back use the filter 'gjm_info_window_additional_details' to return true.
 * 
 * @param  [type] $post [description]
 * @return [type]       [description]
 */
function gjm_info_window_additional_info( $post ) {

	$labels = gjm_labels();

	$company = the_company_name( '', '', false, $post );

	if ( empty( $company ) ) {
		$company = __( 'N/A', 'GJM' );
	}

	$job_type_slug = $job_type_name = '';

	// for version 1.27 or higer
	if ( function_exists( 'wpjm_get_the_job_types' ) ) {
	
		$job_types = wpjm_get_the_job_types( $post->ID );
		
	// before 1.27
	} else {

		$job_types = get_the_job_type( $post );
		$job_types = ! empty( $job_types ) ? array( $job_types ) : array(); 
	}

	if ( ! empty( $job_types ) ) {
			
		$job_type_names = $job_type_slugs = array();

		foreach ( $job_types as $type ) {
			$job_type_names[] = $type->name;
			$job_type_slugs[] = sanitize_title( $type->slug );
		}

		$job_type_names = implode( ', ', $job_type_names );
		$job_type_slugs = implode( ' ', $job_type_slugs );

	} else {

		$job_type_names = __( 'N/A', 'GJM' );
		$job_type_slugs = 'na';
	}
	
	$output = array();

	$output['items_start'] = '<ul class="job-items">';
	
	$output['company'] = '<li class="compny-name"><span class="label">'.esc_attr( $labels['search_result']['company'] ).' </span><span class="item">'.esc_attr( $company ).'</span></li>';
	
	$output['job_types'] = '<li class="job-types '.esc_attr( $job_type_slugs ).'"><span class="label">'.esc_attr( $labels['search_result']['job_types'] ).' </span><span class="item">'.esc_attr( $job_type_names ).'</span></li>';
	
	$output['posted'] = '<li class="date"><span class="item"><date>'.sprintf( $labels['search_result']['posted'], human_time_diff( get_post_time( 'U', false, $post ), current_time( 'timestamp' ) ) ).'</date></li>';
	
	$output['items_end']   = '</ul>';

	return $output;
}
//add_filter( 'gjm_info_window_additional_info', 'gjm_info_window_additional_info' );

/**
 * add new job to GJM table in database
 * 
 * @since  1.0
 * @author Eyal Fitoussi
 */
function gjm_update_location_front_end( $post_id, $values ) {

	$geolocated = get_post_meta( $post_id, 'geolocated', true );
	
	//delete location if address field empty
	if ( empty( $values['job']['job_location'] ) || empty( $geolocated ) ) {

		global $wpdb;
		
		$wpdb->query( $wpdb->prepare( "DELETE FROM " . $wpdb->prefix . "places_locator WHERE post_id=%d", $post_id ) );
		
		return;
	
	} else {
		
		$street_number = get_post_meta( $post_id, 'geolocation_street_number', true );
		$street_name   = get_post_meta( $post_id, 'geolocation_street', true );
		$street_check  = trim( $street_number.' '.$street_name );
		$street		   = ! empty( $street_check ) ? $street_number.' '.$street_name : '';
		
		//Save location information to database
		global $wpdb;
		$wpdb->replace( $wpdb->prefix . 'places_locator', array(
			'post_id'           => $post_id,
			'feature'           => 0,
			'post_type'         => 'job_listing',
			'post_title'        => sanitize_text_field( $values['job']['job_title'] ),
			'post_status'       => 'publish',
			'street_number' 	=> sanitize_text_field( $street_number ),
			'street_name' 		=> sanitize_text_field( $street_name ),
			'street' 			=> sanitize_text_field( $street ),
			'apt' 				=> '',
			'city' 				=> sanitize_text_field( get_post_meta( $post_id, 'geolocation_city', true ) ),
			'state' 			=> sanitize_text_field( get_post_meta( $post_id, 'geolocation_state_short', true ) ),
			'state_long' 		=> sanitize_text_field( get_post_meta( $post_id, 'geolocation_state_long', true ) ),
			'zipcode' 			=> sanitize_text_field( get_post_meta( $post_id, 'geolocation_postcode', true ) ),
			'country' 			=> sanitize_text_field( get_post_meta( $post_id, 'geolocation_country_short', true ) ),
			'country_long' 		=> sanitize_text_field( get_post_meta( $post_id, 'geolocation_country_long', true ) ),
			'address' 			=> ! empty( $values['job']['job_location'] ) ? sanitize_text_field( $values['job']['job_location'] ) : '',
			'formatted_address' => sanitize_text_field( get_post_meta( $post_id, 'geolocation_formatted_address', true ) ),
			'phone'             => '',
			'fax'               => '',
			'email'             => ! empty( $values['job']['application'] ) ? sanitize_text_field( $values['job']['application'] ) : '',
			'website'           => ! empty( $values['job']['company_website'] ) ? sanitize_text_field( $values['job']['company_website'] ) : '',
			'lat'               => sanitize_text_field( get_post_meta( $post_id, 'geolocation_lat',  true ) ),
			'long'              => sanitize_text_field( get_post_meta( $post_id, 'geolocation_long', true ) ),
			'map_icon'          => '_default.png',
		));
	}
}
add_action( 'job_manager_update_job_data', 'gjm_update_location_front_end', 20, 2 );