<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 *  GJM Form Query Class
 */
class GJM_Form_Query_Class {

	// GJM settings
	public $settings;

	// prefix
	public $prefix = 'gjm';

	// shortcode filters
	public $filters;

	// locations data holder
	public $locations_data = array();

	// map locations holder
	public $map_locations = array();

	// post type to be used when doing locations query
	public $post_type_query = 'job_listing';

	/**
	 * [__construct description]
	 */
	function __construct() {

		$this->settings = get_option( 'gjm_options' );
		
		// gjm shortcode attributes
		add_filter( 'job_manager_output_jobs_defaults', array( $this, 'default_atts' ) );
		// modify the search form
		add_action( 'job_manager_job_filters_search_jobs_end', array( $this, 'extend_search_form' ), 8 );
		// modify the search query based on location
		add_filter( 'get_job_listings_query_args', array( $this, 'search_query' ), 99 );
	
		// add distance to results for listable theme
		//if ( function_exists( 'listable_setup' ) ) {
			
			add_action( 'listable_job_listing_card_image_top', array( $this, 'listable_job_distance' ), 99 );
		
		// add distance to results for other themes
		//} else {

			add_action( 'job_listing_meta_end',	array( $this, 'the_distance' ), 99 );
		//}

		// append map to results
		add_action( 'job_manager_job_filters_after', array( $this, 'results_map' ) );

		// create shortcode for map holder
		add_shortcode( 'gjm_results_map', array( $this, 'results_map_shortcode' ) );
		add_shortcode( 'gjm_form_geolocation_features', array( $this, 'generate_geolocation_features' ) );

		// enable indeed features is extension activated
		if ( class_exists( 'WP_Job_Manager_Indeed_Integration' ) ) {
			add_action( 'gjm_modify_search_form_element', array( $this, 'enable_indeed_features'), 15, 2 );
			add_filter( 'job_manager_indeed_get_jobs_args', array( $this, 'modify_indeed_radius' ), 40 );
		}
	}
	
	/**
	 * GJM default shortcode attributes
	 * 
	 * @return void
	 */
	public function shortcode_atts() {

		return array(
			$this->prefix.'_element_id' 	  => rand( 10, 1000 ),
			$this->prefix.'_use'         	  => 0,
			$this->prefix.'_orderby'       	  => 'distance,featured,title,date',
			$this->prefix.'_autocomplete'  	  => 1,
			$this->prefix.'_radius'        	  => '5,10,15,25,50,100',
			$this->prefix.'_units'         	  => 'both',
			$this->prefix.'_distance'      	  => 1,
			$this->prefix.'_auto_locator' 	  => 1,
			$this->prefix.'_locator_button'   => 1,
			$this->prefix.'_map'           	  => 1,
			$this->prefix.'_map_width'     	  => '100%',
			$this->prefix.'_map_height'    	  => '250px',
			$this->prefix.'_map_type'      	  => 'ROADMAP',
			$this->prefix.'_scroll_wheel'  	  => 1,
			$this->prefix.'_group_markers' 	  => 'markers_clusterer',
			$this->prefix.'_user_marker' 	  => 'https://maps.google.com/mapfiles/ms/icons/blue-dot.png',
			$this->prefix.'_location_marker'  => 'https://maps.google.com/mapfiles/ms/icons/red-dot.png',
			$this->prefix.'_clusters_path'    => 'https://raw.githubusercontent.com/googlemaps/js-marker-clusterer/gh-pages/images/m',
			$this->prefix.'_zoom_level' 	  => 'auto',
			$this->prefix.'_max_zoom_level'   => '',
		);
	}

	/**
	 * Check if current page is preset to enable geolocation features
	 * 
	 * @return [type] [description]
	 */
	public function check_preset_page() {

		if ( empty( $this->settings['search_form'][$this->prefix.'_enabled_pages'] ) || ! is_array( $this->settings['search_form'][$this->prefix.'_enabled_pages'] ) ) {
			return false;
		}

		global $wp_query;

		$preset_pages = $this->settings['search_form'][$this->prefix.'_enabled_pages'];

		// get current page ID
    	$page_id = $wp_query->get_queried_object_id();

    	// Look for and enable the geolocation features on preset pages 
		if ( in_array( $page_id, $preset_pages ) || ( is_front_page() && in_array( 'front_page', $preset_pages ) ) ) {

			return true;
		}

		return false;
	}

	/**
	 * Merge GJM and WP JOb Manager shortcode attributes
	 * 
	 * @since 1.0
	 * @author Eyal Fitoussi
	 */
	public function default_atts( $args ) {
		return array_merge( $args, $this->shortcode_atts() );
	}

	/**
	 * generate the geolocation field
	 *
	 * This can be used to enable the geolocation features
	 * in a custom search form that does not use the [jobs] shortcode.
	 * 
	 * @param  array  $atts [description]
	 * @return [type]       [description]
	 */
	public function generate_geolocation_features( $atts = array() ) { 

		$atts = shortcode_atts( $this->shortcode_atts(), $atts );

		return $this->get_search_form_filters( $atts );
	}

	/**
	 * Results map
	 * 
	 * @param  [type] $atts [description]
	 * @return [type]       [description]
	 */
	public function results_map( $atts ) {

		if ( empty( $this->filters[$this->prefix.'_map'] ) || $this->filters[$this->prefix.'_map'] != 1 ) {
			return;
		}

    	// Pass the map arguments to generate the map element
    	$args = array(
    		'map_id'		  => $this->filters[$this->prefix.'_element_id'],
			'prefix'	 	  => $this->prefix,
			'init_map_show'	  => false,
			'map_width'  	  => $this->filters[$this->prefix.'_map_width'],
			'map_height' 	  => $this->filters[$this->prefix.'_map_height'],
			'user_marker'     => $this->filters[$this->prefix.'_user_marker'],
			'zoom_level'	  => $this->filters[$this->prefix.'_zoom_level'],
			'max_zoom_level'  => $this->filters[$this->prefix.'_max_zoom_level'],
			'map_type'		  => $this->filters[$this->prefix.'_map_type'],
			'scrollwheel'	  => $this->filters[$this->prefix.'_scroll_wheel'],
			'group_markers'   => $this->filters[$this->prefix.'_group_markers'],
			'clusters_path'	  => $this->filters[$this->prefix.'_clusters_path']
    	);
 
    	//get the map element
    	echo GJM_Maps::get_map_element( $args );
	}

	/**
	 * Locator button element
	 * 
	 * @since 1.2
	 * 
	 * @author Eyal Fitoussi
	 */
	protected function locator_button() {

		if ( empty( $this->filters[$this->prefix.'_locator_button'] ) ) {
			return;
		}

		$locator =  '<i class="'.$this->prefix.'-filter '.$this->prefix.'-locator-btn gjm-icon-target-1" title="Get your current position" style="display:none;"></i>';
			
		return apply_filters( $this->prefix.'_locator_button', $locator, $this->filters );
	}

	/**
	 * Radius filter to display in search form
	 * 
	 * When multiple values pass, a dropdown select box will be displayed. Otherwise, when single value passes, 
	 * it will be a default value in hidden field.
	 * 
	 * @since 1.0
	 * 
	 * @author Eyal Fitoussi
	 */
	protected function filters_radius() {
				
		$radius = explode( ",", $this->filters[$this->prefix.'_radius'] );
		$output = '';
			
		//display dropdown
		if ( count( $radius ) > 1 ) {
			
			//Displace dropdown
			$output .= '<div class="'.$this->prefix.'-filter-wrapper radius dropdown '.$this->prefix.'-filters-count-'.esc_attr( $this->filters['filters_count'] ).'">';
			$output .= '<select name="radius" class="'.$this->prefix.'-filter" id="'.$this->prefix.'-radius">';
			$output .= '<option value="' . esc_attr( end( $radius ) ) . '">';
						
			//set the first option of the dropdown
			if ( $this->filters[$this->prefix.'_units'] == 'imperial' ) {

				$output .= $this->labels['miles'];

			} elseif ( $this->filters[$this->prefix.'_units'] == 'metric' ) { 

				$output .= $this->labels['kilometers'];
			} else {

				$output .= $this->labels['within'];
			}

			$output .= '</option>';

			$default_value = ! empty( $_GET['radius'] ) ? $_GET['radius'] : '';

			foreach ( $radius as $value ) {
				
				$value    = esc_attr( $value );
				$selected = ( ! empty( $default_value ) && $default_value == $value ) ? 'selected="selected"' : '';

				$output .=  '<option value="' . $value . '" '.$selected.'>' . $value . '</option>';
			}
			
			$output .= '</select>';
			$output .= '</div>';
		
		} else {
			//display hidden default value
			$output .=  '<input type="hidden" id="'.$this->prefix.'-radius" name="radius" value="' . esc_attr( end( $radius ) ) . '" />';
		}
		
		return apply_filters( $this->prefix.'_form_radius_filter', $output, $radius, $this );
	}

	/**
	 * Units filter for search form
	 * 
	 * Will display a dropdown menu when displaying both units. Otherwise, the default units value will be a hidden field.
	 * 
	 * @since  1.0
	 * 
	 * @author Eyal Fitoussi
	 */
	protected function filters_units() {

		$output = '';

		//display dropdown
		if ( $this->filters[$this->prefix.'_units'] == 'imperial' || $this->filters[$this->prefix.'_units'] == 'metric' ) {

			//display hidden field
			$output .= '<input type="hidden" id="'.$this->prefix.'-units" name="units" value="' . esc_attr( $this->filters[$this->prefix.'_units'] ) . '" />';

		} else {
						
			$selected = ( ! empty(  $_GET['units'] ) &&  $_GET['units'] == 'metric' ) ? 'selected="selected"' : '';

			//display dropdown
			$output .= '<div class="'.$this->prefix.'-filter-wrapper units dropdown '.$this->prefix.'-filters-count-'.esc_attr( $this->filters['filters_count'] ).'">';			
			$output .= '<select name="units" class="'.$this->prefix.'-filter" id="'.$this->prefix.'-units">';
			$output .= 	'<option selected="selected" value="imperial">' . esc_attr( $this->labels['miles'] ) . '</option>';
			$output .= 	'<option '.$selected.' value="metric">' . esc_attr( $this->labels['kilometers'] ) . '</option>';
			$output .= 	'</select>';
			$output .= '</div>';
		}

		return apply_filters( $this->prefix.'_form_units_filter', $output, $this );
	}

	/**
	 * Radius filter to display in search form
	 * 
	 * When multiple values pass, a dropdown select box will be displayed. Otherwise, when single value passes, 
	 * it will be a default value in hidden field.
	 * 
	 * @since  1.0
	 * 
	 * @author Eyal Fitoussi
	 */
	protected function filters_sort() {

		$orderby = explode( ",", str_replace(' ', '', $this->filters[$this->prefix.'_orderby'] ) );
		$output  = '';

		//display dropdown
		if ( count( $orderby ) > 1 ) {
						
			//display dropdown
			$output .= '<div class="'.$this->prefix.'-filter-wrapper orderby dropdown '.$this->prefix.'-filters-count-'.esc_attr( $this->filters['filters_count'] ) .'">';			
			$output .= '<select name="'.$this->prefix.'_orderby" class="'.$this->prefix.'-filter" id="'.$this->prefix.'-orderby">';
			$output .= 	'<option class="orderby-first-option" value="">' . esc_attr( $this->labels['orderby_filters']['label'] ) . '</option>';
		
			$valCount = 1;

			$allowed_values = array( 
				'distance', 
				'title', 
				'featured',
				'date',
				'modified',
				'ID',
				'parent',
				'rand',
				'name' 
			);

			$output .= '</option>';

			$default_value = ! empty( $_GET['gjm_orderby'] ) ? esc_attr( $_GET['gjm_orderby'] ) : '';
				
			foreach ( $orderby as $value ) {
				
				if ( in_array( $value, $allowed_values ) ) {				
					
					$selected = ( ! empty( $default_value ) && $default_value == $value ) ? 'selected="selected"' : '';

					$output .= '<option '.$selected.' value="' . esc_attr( $value ) . '" class="'.$this->prefix.'-orderby-value-' . $valCount . '">'.esc_attr( $this->labels['orderby_filters'][$value] ).'</option>';
				}
				$valCount++;
			}
			
			$output .= 	'</select>';
			$output .= '</div>';
		
		} else {
		
			//display hidden default value
			$output .= '<input type="hidden" id="'.$this->prefix.'-orderby" name="'.$this->prefix.'_orderby" value="' . esc_attr( reset( $orderby ) ) . '" />';
		}

		return apply_filters( $this->prefix.'_form_orderby_filter', $output, $orderby, $this );
	}
	
	/**
	 * Generate geolocation filters elements
	 * 
	 * @return [type] [description]
	 */
	public function output_form_filters() {

		// Field counter
		$this->filters['filters_count'] = 0;

		// look for radius dropdown
		if ( count( explode( ",", $this->filters[$this->prefix.'_radius'] ) ) > 1 ) {
			$this->filters['filters_count']++;
		}

		// look for orderby dropdown
		if ( isset( $this->filters[$this->prefix.'_orderby'] ) && count( explode( ",", $this->filters[$this->prefix.'_orderby'] ) ) > 1 ) {
			$this->filters['filters_count']++;
		}

		// look for units dropdown
		if ( ! in_array( $this->filters[$this->prefix.'_units'], array( 'imperial', 'metric' ) ) ) {
			$this->filters['filters_count']++;
		}
		
		$output = array();

		// we need this wrapper only when there is at aleast one filter
		if ( $this->filters['filters_count'] > 0 ) {
			$output['start'] = '<div class="'.$this->prefix.'-filters-wrapper">';
		}

		//add radius filter
		$output['radius'] = self::filters_radius();
		
		//add units filter
		$output['units'] = self::filters_units();
		
		//add sort by filter
		$output['orderby'] = self::filters_sort();

		if ( $this->filters['filters_count'] > 0 ) {
			$output['end'] = '</div>';
		}

		$output = apply_filters( $this->prefix.'_form_filters_output', $output, $this->filters );

		// display fields
		return implode( ' ', $output );
	}

	/**
	 * Enable indeed features 
	 * 
	 * @param  [type] $prefix [description]
	 * @param  [type] $atts   [description]
	 * @return [type]         [description]
	 */
	public function enable_indeed_features( $prefix, $atts ) {

		// enable indeed integration
		if ( class_exists( 'WP_Job_Manager_Indeed_Integration' ) && get_option( 'job_manager_indeed_enable_backfill', 1 ) ) {

			$indeed_map_icon = apply_filters( 'gjm_indeed_map_icon', $atts[$prefix.'_location_marker'], $prefix, $atts );
		
			echo '<div style="display:none" class="indeed-enabled" data-map_icon="'.$indeed_map_icon.'" data-distance="'.$atts[$prefix.'_distance'].'"></div>';
		}
	}
	
	/**
	 * Modify indeed radius
	 * 
	 * @param  [type] $args [description]
	 * @return [type]       [description]
	 */
	public function modify_indeed_radius( $args ) {

		$args['radius'] = $this->filters['radius'];

		return $args;
	}

	/**
	 * generate the geolocation elements to add to the search form
	 * 
	 * @since  1.0
	 * 
	 * @author Eyal Fitoussi
	 */
	public function get_search_form_filters( $atts = array() ) {

		$this->labels = gjm_labels();
		$this->prefix = esc_attr( $this->prefix );

		if ( empty( $atts ) ) {
			$atts = $this->shortcode_atts();
		}

		// look for preset pages and dynamically enable the geo feature if needed
		if ( $this->check_preset_page() ) {

			$atts[$this->prefix.'_use'] = 2;

		} elseif ( empty( $atts[$this->prefix.'_use'] ) ) {
			
			$atts[$this->prefix.'_use'] = 0;
		} 

		$output = '';

		// When using shotcode attributes
		if ( $atts[$this->prefix.'_use'] == 1 ) {

			// get settings from shortcode attrs
			$this->filters = $atts;

			$output .= '<input type="hidden" class="gjm_map url-disabled" name="'.$this->prefix.'_map" value="' . esc_attr( $this->filters[$this->prefix.'_map'] ) . '" />';
		
		// when using the options set in the admin
		} elseif ( $atts[$this->prefix.'_use'] == 2 ) {

			$this->filters = wp_parse_args( $this->settings['search_form'], $atts );

			// generate random element ID
			$this->filters[$this->prefix.'_element_id'] = rand( 10, 1000 );

		} else {

			return;
		}

		//load stylsheet
		wp_enqueue_style( 'gjm-frontend-style' );

		//add hidden locator button that will dynamically append into the address field
		//echo self::locator_button();

		$latitude  = ! empty( $_GET['latitude'] ) ? esc_attr( urldecode( $_GET['latitude'] ) ) : '';
		$longitude = ! empty( $_GET['longitude'] ) ? esc_attr( urldecode( $_GET['longitude'] ) ) : '';
		$state     = ! empty( $_GET['state'] ) ? esc_attr( urldecode( $_GET['state'] ) ) : '';
		$country   = ! empty( $_GET['country'] ) ? esc_attr( urldecode( $_GET['country'] ) ) : '';

		//hidden fields to hold some values
		$output .= '<input type="hidden" class="gjm_use url-disabled" name="'.$this->prefix.'_use" value="'.esc_attr( $atts[$this->prefix.'_use'] ).'" />';
		$output .= '<input type="hidden" class="gjm_element_id url-disabled" id="'.$this->prefix.'_element_id" name="'.$this->prefix.'_element_id" value="'.$this->filters[$this->prefix.'_element_id'].'" />';
		$output .= '<input type="hidden" class="gjm-lat" name="latitude" value="' . $latitude .'" />';
		$output .= '<input type="hidden" class="gjm-lng" name="longitude" value="' . $longitude .'" />';
		$output .= '<input type="hidden" class="gjm-state" name="state" value="' . $state .'" />';
		$output .= '<input type="hidden" class="gjm-country" name="country" value="' . $country .'" />';
		$output .= '<input type="hidden" class="gjm-prev-address url-disabled" name="prev_address" value="" />';
		$output .= '<input type="hidden" class="gjm-location-marker url-disabled" name="'.$this->prefix.'_location_marker" value="'.esc_attr( $this->filters[$this->prefix.'_location_marker'] ).'" />';
		
		$autolocator  = 'disabled';
		$locator_btn  = 'disabled';
		$autocomplete = 'disabled';
		$results_type = 'geocode';
		$country 	  = '';

		// enable auto-locator feature
		if ( ! empty( $this->filters[$this->prefix.'_auto_locator'] ) ) {
			$autolocator = 'enabled';
		}

		// enable address autocomplete
		if ( ! empty( $this->filters[$this->prefix.'_autocomplete'] ) ) {
			
			$autocomplete = 'enabled';

			$results_type = $this->settings['general_settings'][$this->prefix."_address_autocomplete_results_type"] != '' ? $this->settings['general_settings'][$this->prefix."_address_autocomplete_results_type"] : 'geocode'; 
			$country = $this->settings['general_settings'][$this->prefix."_address_autocomplete_country"] != '' ? $this->settings['general_settings'][$this->prefix."_address_autocomplete_country"] : ''; 
		} 

		// enable locator button feature
		if ( ! empty( $this->filters[$this->prefix.'_locator_button'] ) ) {
			$locator_btn = 'enabled';
		}
		
		$output .= '<div style="display:none" class="gjm-enabled" data-id="'.$this->filters[$this->prefix.'_element_id'].'" data-prefix="'.$this->prefix.'" data-autocomplete="'.$autocomplete.'" data-results_type="'. $results_type.'" data-country="'.$country.'" data-autolocator="'.$autolocator.'" data-locator_button="'.$locator_btn.'"></div>';
		
				// output filters in the form
		$output .= $this->output_form_filters();

		// append data to the form
		do_action( 'gjm_modify_search_form_element', $this->prefix, $this->filters );

		GJM_Init::enqueue_google_maps_api();

		if ( ! wp_script_is( 'gjm', 'enqueued' ) ) {
			wp_enqueue_script( 'gjm' );
		}

		return $output;
	}
	
	/**
	 * Extend search form
	 * 
	 * @param  array  $atts [description]
	 * @return [type]       [description]
	 */
	public function extend_search_form( $atts = array() ) {

		echo $this->get_search_form_filters( $atts );
	}

	/**
	 * Get locations from database based on address and radius
	 * 
	 * @param  array  $query_args [description]
	 * 
	 * @return [type]             [description]
	 */
	public static function get_locations_data( $query_args = array() ) {

		// default values
		$query_args = wp_parse_args( $query_args, array(
			'post_type' => 'job_listing',
			'lat'		=> false,
			'lng'		=> false,
			'radius'	=> false,
			'units'		=> 'imperial',
			'state'		=> false,
			'country'   => false,
		) );

		$locations_data = false;

		// if WP Job Manager internal cache is enabled, we can use that to save the location queries
		$cache_enabled = apply_filters( 'get_job_listings_cache_results', true );

        if ( $cache_enabled ) {
	        
	        // generate query hash for cache
	        $hash = md5( json_encode( $query_args ) );
	        $query_args_hash = 'jm_' . $hash . WP_Job_Manager_Cache_Helper::get_transient_version( 'get_job_listings' );
	        
	        // look for saved query in transient
	        $locations_data = get_transient( $query_args_hash );
	    } 
          
        // if no query found in cache, we generate one.
        if ( false === $locations_data ) {
        
        //if ( 1 == 1 ) {	

            //print_r( 'locations query done' );
        
	        // Get earth radius based on units
	        if ( $query_args['units'] == 'imperial' ) {
	        	$earth_radius = 3959;
	        	$units = 'mi';
	        } else {
	        	$earth_radius = 6371;
	        	$units = 'km';
	        }
	
			global $wpdb;

			$clauses['select']	 = "SELECT";
			$clauses['fields'] 	 = "gjmLocations.post_id, gjmLocations.formatted_address, gjmLocations.address, gjmLocations.lat, gjmLocations.`long`, gjmLocations.`long` as lng, '{$units}' AS units";
			$clauses['distance'] = "";
			$clauses['from']	 = "FROM {$wpdb->prefix}places_locator gjmLocations";
			$clauses['where'] 	 = $wpdb->prepare( "WHERE post_type = '%s'", $query_args['post_type'] );
			$clauses['having']   = '';
			$clauses['orderby']  = '';

			// When searchin boundaries of a state
			$boundaries_search = apply_filters( 'gjm_search_within_boundaries', true );

        	if ( $boundaries_search && ! empty( $query_args['state'] ) ) {

        		$clauses['where'] .= $wpdb->prepare( " AND gjmLocations.state = %s", $query_args['state'] );
        	
        	// When searchin boundaries of a country
        	} elseif ( $boundaries_search && ! empty( $query_args['country'] ) ) {
        		
        		$clauses['where'] .= $wpdb->prepare( " AND gjmLocations.country = %s ", $query_args['country'] );

        	// otherwise, if coords exist we do proximity search
        	} elseif ( $query_args['lat'] !== false && $query_args['lng'] !== false ) {
				
				$clauses['distance'] = $wpdb->prepare( ", 
	        		ROUND( %d * acos( cos( radians( %s ) ) * cos( radians( gjmLocations.lat ) ) * cos( radians( gjmLocations.long ) - radians( %s ) ) + sin( radians( %s ) ) * sin( radians( gjmLocations.lat ) ) ),1 ) AS distance", 
	    			array( 
	    				$earth_radius, 
	    				$query_args['lat'], 
	    				$query_args['lng'], 
	    				$query_args['lat']
	    			) 
	    		);

	    		/*
	        	$rad = deg2rad( $query_args['lat'] );
	        	$a   = cos( $rad );
        		$b   = deg2rad( $query_args['lng'] );
        		$c   = sin( $rad );

     
        		$clauses['distance'] = $wpdb->prepare( ", 
	        		ROUND( %d * acos( %s * cos( radians( gjmLocations.lat ) ) * cos( radians( gjmLocations.long ) - ( %s ) ) + ( %s ) * sin( radians( gjmLocations.lat ) ) ),1 ) AS distance NOT NULL", 
	    			array( 
	    				$earth_radius, 
	    				$a, 
	    				$b, 
	    				$c
	    			) 
	    		);
				
				*/
			
	        	// make sure we pass only numeric or decimal as radius
	            if ( ! empty( $query_args['radius'] ) && is_numeric( $query_args['radius'] ) ) {
	        	   $clauses['having'] = $wpdb->prepare( "HAVING distance <= '%s' OR distance IS NULL", $query_args['radius'] );
	        	}

	        	$clauses['orderby'] = "ORDER BY distance";
		    } 

		  	// query the locations
		    $locations = $wpdb->get_results( 
		    	implode( ' ', apply_filters( 'gmw_get_locations_query_clauses', $clauses, $query_args['post_type'] ) ) 
		    );

		    $locations_data = array(
		    	'posts_id' => array(),
		    	'data'	   => array()
		    );

		    // locations found ?
		    if ( ! empty( $locations ) ) {

			   	// modify the locations query
			    foreach ( $locations as $value ) {
						
					// collect posts id into an array to pass into the WP_Query
			    	$locations_data['posts_id'][] = $value->post_id;
					$locations_data['data'][$value->post_id] = $value;
			    }
			}

            // set new query in transient only if cache enabled     
            if ( $cache_enabled ) {  
            	set_transient( $query_args_hash, $locations_data, DAY_IN_SECONDS );
            }
        }

	    return $locations_data;
	}

	/**
	 * Modify search query based on location
	 * 
	 * @since  1.0
	 * 
	 * @author Eyal Fitoussi
	 */
	public function search_query( $query_args ) {
				
		if ( empty( $_REQUEST['form_data'] ) ) {
			return $query_args;
		}
		
		wp_parse_str( $_REQUEST['form_data'], $form_data );
		
		if ( empty( $form_data[$this->prefix.'_use'] ) ) {

			return $query_args;
			
		//check if we are using the shortcode attributes settings
		} elseif ( $form_data[$this->prefix.'_use'] == 1 ) {

			$this->filters = wp_parse_args( $_REQUEST['form_data'], $this->shortcode_atts() );

		//otherwise, are we using the admin settings
		} elseif ( $form_data[$this->prefix.'_use'] == 2 ) {

			$this->filters = wp_parse_args( $form_data, $this->settings['search_form'] );

		//abort if geolocation disable for this shortcode
		} else {

			return $query_args;
		}

		$this->labels = gjm_labels();

		// set default user coords
		$this->filters['user_location']['lat'] = false;
		$this->filters['user_location']['lng'] = false;

		// add geo values to query args so it can be saved in WP Job Manager cache
		$query_args[$this->prefix]['location'] = $this->filters['search_location'];
		$query_args[$this->prefix]['units']    = $this->filters[$this->prefix.'_units'];
		$query_args[$this->prefix]['radius']   = $this->filters['radius'];

		// disable the location query made by WP Job Manager
		// We are doing our own proximity search query
		if ( ! empty( $query_args[$this->prefix]['location'] ) ) {
			unset( $query_args['meta_query'][0] );
		}

		// if we are using gjm orderby we will need to override the original setting created by Wp Jobs Manager plugin.
		// Unless when using orderby "featured" which is when we will leave it as is
		if ( ! empty( $this->filters[$this->prefix.'_orderby'] ) && $this->filters[$this->prefix.'_orderby'] != 'featured' ) {

			//force gjm orderby value from dropdown or default value
			$query_args['orderby'] = $this->filters[$this->prefix.'_orderby'];

			//adjust the order of posts when choosing to order by title
			if ( $this->filters[$this->prefix.'_orderby'] == 'title' ) {
				$query_args['order'] = 'ASC';
			}
						
		// set the original orderby by Wp Job Manager plugin
		} elseif ( empty( $this->filters[$this->prefix.'_orderby'] ) && ! empty( $this->filters['search_location'] ) ) {

			//$query_args['orderby'] = 'meta_key';
			$this->filters[$this->prefix.'_orderby'] = $query_args['orderby'] = apply_filters( 'gjm_default_orderby', $query_args['orderby'], $this );
		} 
 		
		// when searching by address
		if ( ! empty( $this->filters['search_location'] ) && $this->filters['search_location'] != 'Any Location' && $this->filters['search_location'] != 'Location' ) {

			// look for coords in URL
			if ( ! empty( $_GET['latitude'] ) && ! empty( $_GET['longitude'] ) ) {
	
				$this->filters['user_location']['lat'] = $_GET['latitude'];
				$this->filters['user_location']['lng'] = $_GET['longitude'];

			// look for coords in filters
			} elseif ( ! empty( $this->filters['latitude'] ) && ! empty( $this->filters['longitude'] ) ) {
	
				$this->filters['user_location']['lat'] = $this->filters['latitude'];
				$this->filters['user_location']['lng'] = $this->filters['longitude'];
			
			//in case that an address was entered and was not geocoded via client site
			//try again via serverside.
			} elseif ( class_exists( 'WP_Job_Manager_Geocode' ) ) {

				$this->geocoded = WP_Job_Manager_Geocode::get_location_data( $this->filters['search_location'] );
					
				if ( ! is_wp_error( $this->geocoded ) && ! empty( $this->geocoded['lat'] ) && ! empty( $this->geocoded['long'] ) ) {
						
					$this->filters['user_location']['lat'] = $this->geocoded['lat'];
					$this->filters['user_location']['lng'] = $this->geocoded['long'];
				}
			} 
		}
		
		// default locations query args
		$locations_query_args = $args = array(
			'post_type' => $this->post_type_query,
			'lat'		=> $this->filters['user_location']['lat'],
			'lng'		=> $this->filters['user_location']['lng'],
			'radius'	=> $this->filters['radius'],
			'units'		=> $this->filters[$this->prefix.'_units'],
			'state'		=> $this->filters['state'],
			'country'	=> $this->filters['country']
		);

		// do locations query
		$locations_data = self::get_locations_data( $locations_query_args );

		// get locations data
		$this->locations_data = $locations_data['data'];

    	// When searching based on location, we need to include the locatios post ID in the WP_Query.
    	// This is to restricts the query to only show posts with location and exclude the "Anywhere" jobs.
    	// Otherwise, we skip this and the query will include all jobs, with or without location. 
		if ( ! empty( $this->filters['user_location']['lat'] ) && ! empty( $this->filters['user_location']['lng'] ) ) {

			// pass the locations post ID into the WP_Query
			$query_args['post__in'] = ! empty( $locations_data['posts_id'] ) ? $locations_data['posts_id'] : array( '0' );

			// when sorting by distance we pass "post__in" to the orderby arg. 
			// This way the query will sort the results based on the locations post ID, which are ordered
			// by the distance.
			if ( $this->filters[$this->prefix.'_orderby'] == 'distance' ) {

				$query_args['orderby'] = 'post__in';
			}
		}

		//when map enabled we use the_post action to generate some data for the map 
		//and pass it to the JS file.
		if ( $this->filters[$this->prefix.'_map'] == 1 ) {
			
			add_action( 'the_post', array( $this, 'the_post' ) );
			add_action( 'loop_end', array( $this, 'map_element' ) );
		}

		return $query_args;
	}
	
	/**
	 * The post action hook
	 * 
	 * We use this hook to collect the location data of each post and pass it as the map arument.
	 * 
	 */
	public function the_post() {
		
		global $post;

		// if post has location
		if ( ! empty( $this->locations_data[$post->ID] ) ) {

			// we add this data to the post to pass it to the info-window
			if ( ! empty( $this->locations_data[$post->ID]->distance ) ) {
				$post->address  = $this->locations_data[$post->ID]->address;
				$post->formatted_address = $this->locations_data[$post->ID]->formatted_address;
				$post->distance = $this->locations_data[$post->ID]->distance;
				$post->units    = $this->locations_data[$post->ID]->units;
			}

			// info-window content
			$this->locations_data[$post->ID]->info_window_content = $this->info_window_content( $post );

			//Generate map icon to post object
			$map_icon = ! empty( $this->filters[$this->prefix.'_location_marker'] ) ? $this->filters[$this->prefix.'_location_marker'] : 'https://maps.google.com/mapfiles/ms/icons/red-dot.png';
		
			$this->locations_data[$post->ID]->map_icon = apply_filters( $this->prefix.'_map_icon', $map_icon, $post, $this->filters );
			
			$this->map_locations[] = $this->locations_data[$post->ID];
		}
	}

	/**
	 * Pass locations and other values to javasctip to display on the map
	 * @since  1.0
	 * @author Eyal Fitoussi
	 */
	public function map_element( $query ) {
		
		// create the map object
        $map_args = array(
    		'map_id' => $this->filters[$this->prefix.'_element_id'],
            'prefix' => $this->prefix
        );

        $map_options = array();
        
        $user_location = array(
			'lat'		   => $this->filters['user_location']['lat'],
			'lng'		   => $this->filters['user_location']['lng'],
			'address' 	   => $this->filters['search_location'],
			'map_icon'	   => $this->filters[$this->prefix.'_user_marker'],
			'iw_content'   => __( 'You are here', 'GMW' ),
			'iw_open'	   => false
        );

        $map_args = GJM_Maps::get_map_object( $map_args, $map_options, $this->map_locations, $user_location );

        // encode data
        $map_args = wp_json_encode( $map_args )
       	?>
        <script> 

        jQuery( window ).ready( function() {

        	gjmMapObject = <?php echo $map_args; ?>;
 
        	//console.log( gjm_ajax_update_map)
        	/*
        	if ( typeof GJM_Maps['<?php echo $this->filters[$this->prefix.'_element_id']; ?>'] == 'undefined' ) {
        		GJM_Maps['<?php echo $this->filters[$this->prefix.'_element_id']; ?>'] = new GJM_Map( <?php echo json_encode( $map_args ); ?> );
        		GJM_Maps['<?php echo $this->filters[$this->prefix.'_element_id']; ?>'].init( <?php echo json_encode( $map_args ); ?> );
			} else {
				// initiate it
				GJM_Maps['<?php echo $this->filters[$this->prefix.'_element_id']; ?>'].update_map( <?php echo json_encode( $this->map_locations ); ?>, <?php echo json_encode( $map_args['user_position'] ); ?> );
			} */
        });

        </script>
        <?php
       	
		//remove actions
		remove_action( 'the_post', array( $this, 'the_post'    ) );
		remove_action( 'loop_end', array( $this, 'map_element' ) );
	}

	/**
	 * info window function callback
	 * @param  object $post
	 * @return [type]       [description]
	 */
	public function info_window_content( $post ) {
		return GJM_Maps::get_info_window_content( $post );
	}
	
	/**
	 * Get the job distance in the results 
	 * 
	 * @param  [type] $post [description]
	 * @return [type]       [description]
	 */
	public function get_the_distance( $post ) {

		if ( ! isset( $this->filters ) ) {
			return false;
		}

		if ( ! isset( $this->filters ) || $this->filters[$this->prefix.'_use'] == 0 || $this->filters[$this->prefix.'_distance'] != 1  ) {
			return false;
		}

		if ( empty( $this->locations_data[$post->ID]->distance ) ) {
			return false;
		}

		$location_data = $this->locations_data[$post->ID];

		return '<span class="'.$this->prefix.'-distance-wrapper">' . esc_attr( $location_data->distance ) . ' ' . esc_attr(  $location_data->units ) . '</span>';
	}

	/**
	 * Append distance value to results in listable theme
	 * 
	 * @since  1.0
	 * 
	 * @author Eyal Fitoussi
	 */
	public function listable_job_distance( $post ) {

		if ( ! $distance = $this->get_the_distance( $post ) ) {
			return;
		}
		
		echo apply_filters( $this->prefix.'_results_distance', $distance, $post, $this->locations_data[$post->ID], $this->filters );
	}

	/**
	 * Append distance value to results
	 * 
	 * @since  1.0
	 * 
	 * @author Eyal Fitoussi
	 */
	public function the_distance() {

		global $post;

		if ( ! $distance = $this->get_the_distance( $post ) ) {
			return;
		}

		echo apply_filters( $this->prefix.'_results_distance', $distance, $post, $this->locations_data[$post->ID], $this->filters );
	}

	/**
	 * Map holder for map when using map results shortcode
	 * @param  array $atts element ID
	 * @return void
	 */
	public function results_map_shortcode( $atts ) {

		$element_id = ! empty( $atts['element_id'] ) ? '-'.$atts['element_id'].'"' : '';

		return '<div id="'.esc_attr( $this->prefix ).'-results-map-holder'.$element_id.'" class="results-map-holder"></div>';
	}
}
new GJM_Form_Query_Class();