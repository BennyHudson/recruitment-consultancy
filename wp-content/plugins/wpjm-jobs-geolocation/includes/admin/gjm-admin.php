<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	die();
}

/**
 * GJM_Admin class
 */ 
class GJM_Admin {

	/**
	 * __construct function.
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {

		$this->settings = get_option( 'gjm_options' );
		
		//include admin files
		include( 'gjm-admin-settings.php' );
		include( 'gjm-db.php' );

		add_action( 'admin_print_scripts-post-new.php', array( $this, 'address_autocomplete' ), 11 );
		add_action( 'admin_print_scripts-post.php', 	array( $this, 'address_autocomplete' ), 11 );
		add_action( 'job_manager_save_job_listing' , 	array( $this, 'update_job_location'  ), 99, 2 );	
		add_action( 'pmxi_saved_post', 					array( $this, 'pmxi_location_update' ), 20 );
		add_action( 'goft_wpjm_after_insert_job',       array( $this, 'pmxi_location_update' ), 30 );
	}
			
	/**
	 * Google Places address autocomplete in "Edit Job" page admin.
	 * @return void [description]
	 */
	function address_autocomplete() {
		
		global $post_type;

		if ( $post_type != 'job_listing' || empty( $this->settings['general_settings']['gjm_address_autocomplete_form_admin'] ) ) {
			return;
		}
		
		GJM_Init::enqueue_google_maps_api();

		wp_enqueue_script( 'gjm-autocomplete');
	}
	
	/**
	 * Add location data to GEo my WP table in database
	 * 
	 * @param unknown_type $post_id
	 */
	public function add_location_to_db( $post_id ) {
		
		global $wpdb;
		
		$street_number = sanitize_text_field( get_post_meta( $post_id, 'geolocation_street_number', true ) );
		$street_name   = sanitize_text_field( get_post_meta( $post_id, 'geolocation_street', true ) );	
		$street_check  = trim( $street_number . ' ' . $street_name );
		$street		   = ! empty( $street_check ) ? $street_number . ' ' . $street_name : '';

		$wpdb->replace( $wpdb->prefix . 'places_locator',
			array(
				'post_id'			=> $post_id,
				'feature'  			=> 0,
				'post_type' 		=> 'job_listing',
				'post_title'		=> ! empty( $_POST['post_title'] ) ? sanitize_text_field( $_POST['post_title'] ) : '',
				'post_status'		=> 'publish',
				'street_number' 	=> ! empty( $street_number ) ? $street_number : '',
				'street_name' 		=> ! empty( $street_name ) ? $street_name : '',
				'street' 			=> $street,
				'apt' 				=> '',
				'city' 				=> sanitize_text_field( get_post_meta( $post_id, 'geolocation_city', true ) ),
				'state' 			=> sanitize_text_field( get_post_meta( $post_id, 'geolocation_state_short', true ) ),
				'state_long' 		=> sanitize_text_field( get_post_meta( $post_id, 'geolocation_state_long', true ) ),
				'zipcode' 			=> sanitize_text_field( get_post_meta( $post_id, 'geolocation_postcode', true ) ),
				'country' 			=> sanitize_text_field( get_post_meta( $post_id, 'geolocation_country_short', true ) ),
				'country_long' 		=> sanitize_text_field( get_post_meta( $post_id, 'geolocation_country_long', true ) ),
				'address' 			=> ! empty( $_POST['_job_location'] ) ? sanitize_text_field( $_POST['_job_location'] ) : '',
				'formatted_address' => sanitize_text_field( get_post_meta( $post_id, 'geolocation_formatted_address', true ) ),
				'phone' 			=> '',
				'fax' 				=> '',
				'email' 			=> ! empty( $_POST['_application'] ) ? sanitize_text_field( $_POST['_application'] ) : '',
				'website' 			=> ! empty( $_POST['_company_website'] ) ? sanitize_text_field( $_POST['_company_website'] ) : '',
				'lat' 				=> sanitize_text_field( get_post_meta( $post_id, 'geolocation_lat', true ) ),
				'long' 				=> sanitize_text_field( get_post_meta( $post_id, 'geolocation_long', true ) ),
				'map_icon'  		=> '_default.png',
			)
		);
	}

	/**
	 * Update location data when importing using WP ALL IMPORT
	 * @param unknown_type $post_id
	 */
	public function pmxi_location_update( $post_id ) {

		if ( 'job_listing' === get_post_type( $post_id ) ) {

			$geolocated = get_post_meta( $post_id, 'geolocated', true );

			if ( ! empty( $geolocated ) ) {	
				
				self::add_location_to_db( $post_id ); 
									
				return;
			}
		}
	}
	
	/**
	 * Update Job location when saving job in admin
	 * @param  $post_id 
	 * @return void
	 */
	function update_job_location( $post_id, $post ) {
		
		$geolocated = get_post_meta( $post_id, 'geolocated', true );
		
		if ( empty( $_POST['_job_location'] ) || empty( $geolocated ) ) {
			
			global $wpdb;

			$wpdb->query( $wpdb->prepare( "DELETE FROM " . $wpdb->prefix . "places_locator WHERE post_id=%d", $post_id ) );

			return;
		
		} else {

			self::add_location_to_db( $post_id );
			return;
		} 	
	}	
}
new GJM_Admin;