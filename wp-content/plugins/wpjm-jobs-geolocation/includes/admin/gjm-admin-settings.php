<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * GJM_Admin class
 */
class GJM_Admin_Settings {

	/**
	 * Options
	 * 
	 * @var array
	 */
	public $settings = array();

	/**
	 * Prefix 
	 * 
	 * @var string
	 */
	public $prefix = 'gjm';

	public $name_singular = 'job';

	public $name_plural = 'jobs';

	/**
	 * __construct function.
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {

		$this->settings = get_option( 'gjm_options' );	
	
		//default settings - load only on Jobs settings page
		if ( ! empty( $_GET['post_type'] ) && $_GET['post_type'] == 'job_listing' && !empty( $_GET['page' ] ) && $_GET['page'] == 'job-manager-settings' ) {
			add_action( 'admin_init', array( $this, 'default_options' ), 99 );
		}

		//hook custom function that creates the importer button
		add_action( 'wp_job_manager_admin_field_gjm_locations_importer', array( $this, 'locations_importer_form' ) );
		
		//locations importer
		add_action( 'admin_init', array( $this, 'locations_importer' ) );
		add_action( 'admin_notices', array( $this, 'admin_notices' ) );

		//admin settings - append GJM settings to WPJM settings page
		add_filter( 'job_manager_settings', array( $this, 'admin_settings' ) );

		//make sure we updating WPJM settings page to also update our settings - run this only on UPDATE settings
		if ( ! empty( $_POST['action'] ) && $_POST['action'] == 'update' && !empty( $_POST['option_page' ] ) && $_POST['option_page'] == 'job_manager' ) {
			add_action( 'admin_init', array( $this, 'options_validate' ), 10 );		
		}	
	}

	/**
	 * Set the default options of the plugin if not exist
	 */
	public function default_options() {

		//only if no options set
		if ( ! empty( $this->settings ) ) {
			return;
		}

		$options = array(
			'general_settings' => array(
				$this->prefix.'_address_autocomplete_form_admin' 	=> 1,
				$this->prefix.'_address_autocomplete_form_frontend' => 1,
				$this->prefix.'_address_autocomplete_results_type'  => 'geocode',
				$this->prefix.'_address_autocomplete_country'	   	=> ''
			),
			'search_form'  		=> array(
				$this->prefix.'_auto_locator' 	=> 1,
				$this->prefix.'_autocomplete' 	=> 1,
				$this->prefix.'_locator_button' => 1,
				$this->prefix.'_radius' 		=> '10,15,25,50,100',
				$this->prefix.'_units' 			=> 'both',
				$this->prefix.'_orderby' 		=> 'distance,title,featured,date',
				$this->prefix.'_map' 			=> 1,
				$this->prefix.'_map_width' 		=> '100%',
				$this->prefix.'_map_height' 	=> '250px',
				$this->prefix.'_map_type' 		=> 'ROADMAP',
				$this->prefix.'_group_markers' 	=> 'markers_clusterer',
				$this->prefix.'_user_marker' 	=> 'https://maps.google.com/mapfiles/ms/icons/blue-dot.png',
				$this->prefix.'_location_marker'=> 'https://maps.google.com/mapfiles/ms/icons/red-dot.png',
				$this->prefix.'_clusters_path'	=> 'https://raw.githubusercontent.com/googlemaps/js-marker-clusterer/gh-pages/images/m',
				$this->prefix.'_zoom_level' 	=> 'auto',
				$this->prefix.'_max_zoom_level' => '',
				$this->prefix.'_scroll_wheel' 	=> 1,
				$this->prefix.'_distance' 		=> 1,
				$this->prefix.'_pages_enabled'  => array()
			),
			'single_page'  		=> array(
				$this->prefix.'_single_map_enabled' 		=> 'top',
				$this->prefix.'_single_map_width' 			=> '100%',
				$this->prefix.'_single_map_height' 			=> '200px',
				$this->prefix.'_single_map_type' 			=> 'ROADMAP',
				$this->prefix.'_single_map_location_marker'	=> 'https://maps.google.com/mapfiles/ms/icons/red-dot.png',
				$this->prefix.'_single_map_scroll_wheel' 	=> 1
			),
		);

		//save each field in its own database option.
		//we do this because thats how WP Job Manager saves its options and display them again on page load
		foreach ( $options as $groups ) {
			foreach ( $groups as $field => $value ) {
				update_option( $field, $value );
			}
		}
		
		//update default options into single database option
		update_option( $this->prefix.'_options', $options );
	}

	/**
	 * Locations importer button
	 * 
	 * @param  $option 
	 * @return void
	 */
	public function locations_importer_form( $option ) {
	?>		
		<input type="button" id="gjm-locations-import-toggle" class="button-secondary" value="<?php _e( "Import", "GJM" ); ?>" onclick="jQuery('#gjm_import_hidden').prop('disabled', false);jQuery(this).closest('form').submit();" />
		<input type="hidden" id="gjm_import_hidden" name="gjm_import_action" value="<?php echo $this->prefix; ?>" disabled="disabled" />
		<p class="description"><?php echo $option['desc']; ?></p>
		<?php wp_nonce_field( $this->prefix.'_locations_import_nonce', $this->prefix.'_locations_import_nonce' ); ?>
	<?php 				
	}
	
	/**
	 * Locations importer
	 * @return void
	 */
	public function locations_importer() {
		
		if ( empty( $_POST['gjm_import_action'] ) ) {
			return;
		}
	
		$prefix = $_POST['gjm_import_action'];
		
		//get prefix
		if ( $prefix == 'gjm' ) {
			$post_type = 'job_listing';
			$page_name = 'job-manager-settings';
		} else {
			$post_type = 'resume';
			$page_name = 'resume-manager-settings';
		}

		//varify nonce
		if ( empty( $_POST[$prefix.'_locations_import_nonce'] ) || !wp_verify_nonce( $_POST[$prefix.'_locations_import_nonce'], $prefix.'_locations_import_nonce' ) ) {
			wp_safe_redirect( admin_url( 'edit.php?post_type='.$post_type.'&page='.$page_name.'&gjm_notice=locations_import_nonce_error&notice_status=error' ) );
		}
	
		global $wpdb;
		
		header( 'Content-Type: text/csv; charset=utf-8' );
		
		//get all posts with posts types jobs or resume
		$posts = $wpdb->get_results("
			SELECT `ID` as 'post_id', `post_title`, `post_type`, `post_status`
			FROM `{$wpdb->prefix}posts`
			WHERE `post_type` = '{$post_type}'
			", ARRAY_A );
		
		//abort if no posts found
		if ( empty( $posts ) ) {
			wp_safe_redirect( admin_url( 'edit.php?post_type='.$post_type.'&page='.$page_name.'&gjm_notice=no_locations_to_import&notice_status=error' ) );
			exit;
		}
		
		//number of locations imported
		$imported = 0;
		
		//loop found posts
		foreach ( $posts as $post_key => $post_info ) {
			
			//check that location does not already exist in gmw table database
			$check_location = $wpdb->get_col( "SELECT `post_id` FROM `{$wpdb->prefix}places_locator` WHERE `post_id` = {$post_info['post_id']}", 0 );
				
			//skip location if exist in gmw table
			if ( ! empty( $check_location ) ) {
				continue;
			}
							
			//get all jobs or resume custom fields with location from database
			$post_location = $wpdb->get_results("
				SELECT `meta_key`, `meta_value`
				FROM `{$wpdb->prefix}postmeta`
				WHERE `post_id` = {$post_info['post_id']}
				AND `meta_key` IN ( 'geolocation_street', 'geolocation_city', 'geolocation_state_short', 'geolocation_state_long', 'geolocation_postcode',
				'geolocation_country_short', 'geolocation_country_long', 'geolocation_lat', 'geolocation_long', 'geolocation_formatted_address')
			", ARRAY_A );

			//loop custom fields and add them to array of posts
			foreach ( $post_location as $location_key => $location ) {			
				$posts[$post_key][$location['meta_key']] =  $location['meta_value'];
			}

			//verify coordinates
			if ( empty( $posts[$post_key]['geolocation_lat'] ) || empty( $posts[$post_key]['geolocation_long'] ) ) {
				continue;
			}
			
			//create entry in gmw table in database
			$created = $wpdb->query( $wpdb->prepare(
				"INSERT INTO `{$wpdb->prefix}places_locator`
				( `post_id`, `feature`, `post_status`, `post_type`, `post_title`, `lat`,
				`long`, `street`, `apt`, `city`, `state`, `state_long`, `zipcode`, `country`,
				`country_long`, `address`, `formatted_address`, `phone`, `fax`, `email`, `website`, `map_icon` )
				VALUES ( %d, %d, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s )",
				array(
					$posts[$post_key]['post_id'],
					0,
					$posts[$post_key]['post_status'],
					$post_type,
					$posts[$post_key]['post_title'],
					$posts[$post_key]['geolocation_lat'],
					$posts[$post_key]['geolocation_long'],
					$posts[$post_key]['geolocation_street'],
					'',
					$posts[$post_key]['geolocation_city'],
					$posts[$post_key]['geolocation_state_short'],
					$posts[$post_key]['geolocation_state_long'],
					$posts[$post_key]['geolocation_postcode'],
					$posts[$post_key]['geolocation_country_short'],
					$posts[$post_key]['geolocation_country_long'],
					$posts[$post_key]['geolocation_formatted_address'],
					$posts[$post_key]['geolocation_formatted_address'],
					'',
					'',
					'',
					'',
					'default.png'
				) ) );
	
			if ( $created )
				$imported++;		
		}
					
		if ( $imported == 0 ) {
			wp_safe_redirect( admin_url( 'edit.php?post_type='.$post_type.'&page='.$page_name.'&gjm_notice=no_locations_imported&notice_status=error' ) );
		} else {
			wp_safe_redirect( admin_url( 'edit.php?post_type='.$post_type.'&page='.$page_name.'&gjm_notice=locations_imported&number_of_locations='.$imported.'&notice_status=updated' ) );
		}
		exit;
	}
	
	/**
	 * Importer admin notice
	 * @return void
	 */
	function admin_notices() {
		
		//check if notice exist
		if ( empty( $_GET['gjm_notice'] ) )
			return;
	
		$notice 	= $_GET['gjm_notice'];
		$status 	= $_GET['notice_status'];
		$locations  = ( !empty( $_GET['number_of_locations'] ) ) ? $_GET['number_of_locations'] : 0;
		
		$messages 	= array();
		$messages['locations_import_nonce_error'] = __( 'There was an error while trying to import locations.', 'GJM' );
		$messages['no_locations_to_import'] = __( 'No locations to import were found.', 'GJM' );
		$messages['no_locations_imported'] = __( 'No locations were imported.', 'GJM' );
		$messages['locations_imported'] = sprintf( __( '%d Locations successfully imported.', 'GJM' ), $locations );
							
		?>
	    <div class="<?php echo $status;?>" style="display:block;margin-top:10px;">
	    	<p><?php echo $messages[$notice]; ?></p>
	    </div>
		<?php
	}

	/**
     * Get existing WordPress pages
     */
    public function get_pages() {
         
        $pages = array();

        foreach ( get_pages() as $page ) {
            $pages[ $page->ID ] = $page->post_title;
        }

        return $pages;
    }

    /**
     * Multiselect type 
     * @param  [type] $option      [description]
     * @param  [type] $attributes  [description]
     * @param  [type] $value       [description]
     * @param  [type] $placeholder [description]
     * @return [type]              [description]
     */
	public function multiselect_type( $option, $attributes, $value, $placeholder ) {

		?>
		<select multiple="multiple" data-placeholder="Select pages..." id="setting-<?php echo $option['name']; ?>" class="regular-text" name="<?php echo $option['name']; ?>[]" <?php echo implode( ' ', $attributes ); ?>><?php
			
			if ( ! is_array( $value ) || empty( $value ) ) {
				$value = array();
			}

			if ( in_array( 'front_page', $value ) ) {
				$front_selected = 'selected="selected"';
			} else {
				$front_selected = '';
			}

			echo '<option value="front_page" ' . $front_selected . '>' . 'Home / front page' . '</option>';

			foreach( $option['options'] as $key => $name ) {

				$selected = '';

				if ( in_array( $key, $value ) ) {
					$selected = 'selected="selected"';
				}

				echo '<option value="' . esc_attr( $key ) . '" ' . $selected . '>' . esc_html( $name ) . '</option>';
			}

		?></select>

		<?php $chosen_id = '#setting-' . $this->prefix . '_enabled_pages'; ?>

		<script>
		jQuery( document ).ready( function() {
			jQuery( '<?php echo $chosen_id; ?>' ).chosen( {width: "35em"} );
		});
		</script>
		<?php

		if ( $option['desc'] ) {
			echo ' <p class="description">' . $option['desc'] . '</p>';
		}

		// enable chosen for the multiselect
		wp_enqueue_script( 'chosen', JOB_MANAGER_PLUGIN_URL . '/assets/js/jquery-chosen/chosen.jquery.min.js', array( 'jquery' ), '1.1.0', true );	
		wp_enqueue_style( 'chosen', JOB_MANAGER_PLUGIN_URL . '/assets/css/chosen.css' );
	}

	/**
	 * Admin settings page
	 * @param  array $args 
	 * @return void     
	 */
	public function admin_settings( $args ) {

		$prefix = $this->prefix;

		add_action( 'wp_job_manager_admin_field_'.$prefix.'_multiselect', array( $this, 'multiselect_type' ), 50, 4 );
		
		$is_gjm_settings_page = false;

		if ( 
			isset( $_GET['page'] ) && $_GET['page'] == 'job-manager-settings' ||
			isset( $_POST['option_page']) && $_POST['action'] == 'update' && $_POST['option_page'] == 'job_manager' ) {
			
			$is_gjm_settings_page = true;
		}

		//general settings
		$args['gjm_general_settings'] = array(

			__( 'GEO General Settings', 'GJM' ),
			array(
				$prefix.'_import_locations' => array(
					'name'        => $prefix.'_import_locations',
					'type'        => $prefix.'_locations_importer',
					'std'         => '',
					'label'       => __( 'Import Locations', 'GJM' ),
					'desc'        => __( "Import missing locations to the geolocation database. You can use this to import locations created previously the installation of the geolocation extension or other missing locations.", 'GJM' ),    
					'attributes'  => array(),
				)
			)
		);

		//apply only for WP Job Manager settings page
		if ( $is_gjm_settings_page ) {
				
			$args['gjm_general_settings'][1][] = array(
				'name'        => $prefix.'_google_api_key',
				'type'		  => 'text',
				'std'         => '',
				'placeholder' => '',
				'label'       => __( 'Google API Key', 'GJM' ),
				'desc'        => sprintf( __( 'Enter your Google API key. Click <a href="%s" target="_blank">here</a> to learn how to generate and setup your Google Maps API key.', 'GJM' ), 'http://docs.gravitygeolocation.com/article/101-create-google-map-api-key' ),
				'attributes'  => array( 'size' => 40 )
			);

			$args['gjm_general_settings'][1][] = array(
				'name'        => 'gjm_region',
				'type'		  => 'text',
				'std'         => '',
				'placeholder' => '',
				'label'       => __( 'Google API Region', 'GJM' ),
				'desc'        => sprintf( __( "Enter the region code that you would like to be used as the default when Google API geocodes an address. This is useful when most or all of the jobs on your site located in a specific country or region. <a %s target='_blank'>Here</a> you can find the list of the supported region codes.", 'GJM' ), 'href="http://www.iana.org/assignments/language-subtag-registry/language-subtag-registry"' ),
				'attributes'  => array( 'size' => 3 )
			);

			$args['gjm_general_settings'][1][] = array(
				'name'        => 'gjm_language',
				'type'		  => 'text',
				'std'         => '',
				'placeholder' => '',
				'label'       => __( 'Google API Language', 'GJM' ),
				'desc'        => sprintf( __( "Enter the default language of Google API. This feature controls the language of the maps as well as the address autocomplete suggested results. <a %s target='_blank'>Here</a> you can find the list of the supported languages.", 'GJM' ), 'href="https://developers.google.com/maps/faq#languagesupport"' ),
				'attributes'  => array( 'size' => 3 )
			);	
		}

		$args['gjm_general_settings'][1][] = array(
			'name'        => $prefix.'_address_autocomplete_form_admin',
			'type'        => 'checkbox',
			'std'         => '1',
			'label'       => __( 'Address Autocomplete (admin)', 'GJM' ),
			'cb_label'    => __( 'Enable', 'GJM' ),
			'desc'        => sprintf( __( "Enable suggested results by Google API in the location field of the new/edit %s page of the admin's dasboard.", 'GJM' ), $this->name_singular ),
			'attributes'  => array()
		);

		$args['gjm_general_settings'][1][] = array(
			'name'        => $prefix.'_address_autocomplete_form_frontend',
			'type'        => 'checkbox',
			'std'         => '1',
			'label'       => __( 'Address Autocomplete (front-end)', 'GJM' ),
			'cb_label'    => __( 'Enable', 'GJM' ),
			'desc'        => sprintf( __( 'Enable suggested results by Google API in the location field of the front-end %s submission form.', 'GJM' ), $this->name_singular ),						
			'attributes'  => array()
		);

		$args['gjm_general_settings'][1][] = array(
			'name'        => $prefix.'_address_autocomplete_results_type',
			'type'		  => 'select',
			'std'         => 'geocode',
			'label'       => __( 'Autocomplete Result Type', 'GJM' ),
			'desc'        => sprintf( __( 'Select the type of the address autocomplete results. You can choose between geocode, establishment, address, regions, and cities. <a href="%s" target="_blank">Here</a> you can find more information regarding the supported result types.', 'GJM' ), 'https://developers.google.com/maps/documentation/javascript/places-autocomplete#add_autocomplete' ),
			'options'	  => array(
				'geocode'		=> 'geocode',
				'establishment' => 'establishment',
				'address'		=> 'address',
				'(regions)' 	=> '(regions)',
				'(cities)'		=> '(cities)'
			),
			'attributes'  => array()
		);

		$args['gjm_general_settings'][1][] = array(
			'name'        => $prefix.'_address_autocomplete_country',
			'type'		  => 'text',
			'std'         => '',
			'placeholder' => '',
			'label'       => __( 'Autocomplete Country', 'GJM' ),
			'desc'        => __( "Enter a country code if you would like to restrict the address autocomplete results to a specific country.", 'GJM' ),
			'attributes'  => array( 'size' => 3 )
		);
		
		//search form options
		$args['gjm_search_form'] = array(
			__( 'Geo Search Form', 'GJM' ),
			array(
				array(
					'name'        => 'top_message',
					'std'         => '1',
					'label'       => __( 'Usage', 'GJM' ),
					'desc'        => sprintf( __( 'To enable the geolocation features of this settings page, add the shortcode attribute %s_use="2" to any of the [%s] shortcodes on your site. For example, [%s %s_use="2"].', 'GJM' ), $this->prefix, $this->name_plural, $this->name_plural, $this->prefix ),
					'type'        => 'text',
					'attributes'  => array( 'style' => 'display:none', 'disabled' => 'disabled' )
				),
				array(
					'name'        => $prefix.'_enabled_pages',
					'std'         => '',
					'label'       => __( 'Preset Pages', 'GJM' ),
					'desc'        => __( 'Select the pages where you would like to enable the geolocation settings of this page. This is useful when you want to enable the geolocation features on pages that use a custom template file and you don\'t have access to the shortcode. For example on the home page or the "jobs" page which some themes load using custom template files.', 'GJM' ),
					'type'		  => $prefix.'_multiselect',
					'options'	  => $this->get_pages()
				),
				array(
					'name'        => $prefix.'_auto_locator',
					'std'         => '1',
					'label'       => __( 'Page Load Locator', 'GJM' ),
					'cb_label'    => __( 'Enable', 'GJM' ),
					'desc'        => __( 'When enabled, the browser will try to detect the user\'s current position on page load, and dynamically submit the search form using this location.' , 'GJM' ),
					'type'        => 'checkbox',
					'attributes'  => array()
				),
				array(
					'name'        => $prefix.'_autocomplete',
					'std'         => '1',
					'label'       => __( 'Address Autocomplete', 'GJM' ),
					'cb_label'    => __( 'Enable', 'GJM' ),
					'desc'        => __( 'Enable suggested results by Google Places while typing an address in the location field.', 'GJM' ),
					'type'        => 'checkbox',
					'attributes'  => array()
				),
				array(
					'name'        => $prefix.'_locator_button',
					'std'         => '0',
					'label'       => __( 'Locator Button', 'GJM' ),
					'cb_label'    => __( 'Enable', 'GJM' ),
					'desc'        => __( 'Disply a locator button, inside the location field, that will dynamically retrive the visitor\'s current position.', 'GJM' ),
					'type'        => 'checkbox',
					'attributes'  => array()
				),
				array(
					'name'        => $prefix.'_radius',
					'std'         => '5,10,15,25,50,100',
					'placeholder' => '',
					'label'       => __( 'Radius', 'GJM' ),
					'desc'        => __( 'Enter a single value to be used as the default value, or multiple values comma separated, to displaed as a select box in the search form.', 'GJM' ),
					'type'		  => 'text',
					'attributes'  => array( 'size' => '25' )
				),
				array(
					'name'        => $prefix.'_units',
					'std'         => 'both',
					'placeholder' => '',
					'label'       => __( 'Units', 'GJM' ),
					'desc'        => __( 'Select a single unit value to be used as the default, or select "Both" to display both units in a select box in the search form.', 'GJM' ),
					'type'        => 'select',
					'options'	  => array( 
						'both' 		=> 'Both', 
						'imperial' 	=> 'Miles', 
						'metric' 	=> 'Kilometers' ),
					'attributes'  => array()
				),
				array(
					'name'        => $prefix.'_orderby',
					'type'		  => 'text',
					'std'         => 'distance,title,featured,date',
					'placeholder' => '',
					'label'       => __( 'Orderby', 'GJM' ),
					'desc'        => __( 'Enter a single value that will be used as the default, or multiple values, comma separated and in the order which you would like to display them, to display an orderby select box in the jobs search form.', 'GJM' ),
					'attributes'  => array( 'size' => '25' )
				),
				array(
					'name'        => $prefix.'_map',
					'std'         => '1',
					'label'       => __( 'Google Map', 'GJM' ),
					'cb_label'    => __( 'Enable', 'GJM' ),
					'desc'        => __( 'Disply map showing the locations above the list of results.', 'GJM' ),
					'type'        => 'checkbox',
					'attributes'  => array()
				),
				array(
					'name'        => $prefix.'_map_height',
					'type'		  => 'text',
					'std'         => '250px',
					'placeholder' => '',
					'label'       => __( 'Map Height', 'GJM' ),
					'desc'        => __( 'Map height in pixels or percentage (ex. 100% or 250px).', 'GJM' ),
					'attributes'  => array( 'size' => '7' )
				),
				array(
					'name'        => $prefix.'_map_width',
					'type'		  => 'text',
					'std'         => '100%',
					'placeholder' => '',
					'label'       => __( 'Map Width', 'GJM' ),
					'desc'        => __( 'Map width in pixels or percentage (ex. 100% or 250px).', 'GJM' ),
					'attributes'  => array( 'size' => '7' )
				),
				array(
					'name'        => $prefix.'_map_type',
					'std'         => 'ROADMAP',
					'label'       => __( 'Map Type', 'GJM' ),
					'desc'        => __( 'Select the map type.', 'GJM' ),
					'type'		  => 'select',
					'options'	  => array(
							'ROADMAP' 	=> __( 'ROADMAP' , 'GJM' ),
							'SATELLITE' => __( 'SATELLITE' , 'GJM' ),
							'HYBRID'    => __( 'HYBRID' , 'GJM' ),
							'TERRAIN'   => __( 'TERRAIN' , 'GJM' )
					),
				),
				array(
					'name'        => $prefix.'_group_markers',
					'std'         => 'markers_clusterer',
					'label'       => __( 'Map Markers Grouping', 'GJM' ),
					'desc'        => __( 'Select between Marker Clusterer or Marker Spiderfier to group nearby markers on the map.', 'GJM' ),
					'type'        => 'select',
					'options'	  => array(
							'normal' 			=> __( 'No grouping' , 'GJM' ),
							'markers_clusterer' => __( 'Markers Clusters' , 'GJM' ),
							'markers_spiderfier'=> __( 'Markers Spiderfier' , 'GJM' ),
					),
				),
				array(
					'name'        => $prefix.'_user_marker',
					'std'         => 'https://maps.google.com/mapfiles/ms/icons/blue-dot.png',
					'label'       => __( 'User Map Icon', 'GJM' ),
					'desc'        => __( 'URL of a map icon that represents the user\'s position on the map.', 'GJM' ),
					'type'        => 'text',
					'attributes'  => array( 'style' => 'width:400px' ) 
				),
				array(
					'name'        => $prefix.'_location_marker',
					'std'         => 'https://maps.google.com/mapfiles/ms/icons/red-dot.png',
					'label'       => __( 'Location Map Icon', 'GJM' ),
					'desc'        => __( 'URL of a map icon that represents the jobs location on the map.', 'GJM' ),
					'type'        => 'text',
					'attributes'  => array( 'style' => 'width:400px' ) 
				),
				array(
					'name'        => $prefix.'_zoom_level',
					'std'         => '',
					'label'       => __( 'Zoom Level', 'GJM' ),
					'desc'        => __( 'Select "auto" zoom or a zoom level between 1 to 20. A menual zoon level will zoom in to the visitor\'s position, if exist on the map. Otherwise, if the visitor\'s location does not exist, or if the zoom level is set to "auto", the map will zoom to fit all markers in view.', 'GJM' ),
					'type'		  => 'select',
					'options' 	  => array(
						'auto' => 'Auto Zoom',
						'1'    => '1',
						'2'    => '2',
						'3'    => '3',
						'4'    => '4',
						'5'    => '5',
						'6'    => '6',
						'7'    => '7',
						'8'    => '8',
						'9'    => '9',
						'10'   => '10',
						'11'   => '11',
						'12'   => '12',
						'13'   => '13',
						'14'   => '14',
						'15'   => '15',
						'16'   => '16',
						'17'   => '17',
						'18'   => '18',
						'19'   => '19',
						'20'   => '20'
					)
				),
				array(
					'name'        => $prefix.'_max_zoom_level',
					'type'		  => 'text',
					'std'         => '',
					'label'       => __( 'Maximum Zoom Level', 'GJM' ),
					'desc'        => __( "Enter a value between 1 to 20 that will be used as the maximum zoom level of the map. Leave blank for no zoom level limit.", 'GJM' ),
					'attributes'  => array( 'size' => '3') 
				),
				array(
					'name'        => $prefix.'_scroll_wheel',
					'std'         => '1',
					'label'       => __( 'Zoom With Scrollwheel', 'GJM' ),
					'cb_label'    => __( 'Enable', 'GJM' ),
					'desc'        => __( 'Enable map zoom in/out using the mouse scrollwheel.', 'GJM' ),
					'type'        => 'checkbox',
					'attributes'  => array()
				),
				array(
					'name'        => $prefix.'_clusters_path',
					'std'         => 'https://raw.githubusercontent.com/googlemaps/js-marker-clusterer/gh-pages/images/m',
					'label'       => __( 'Clusters Path', 'GJM' ),
					'desc'        => __( 'The path to the folder that contains the marker clusters images. The images must named m1.png, m2.png, m3.png, m4.png and m5.png, respective to the cluster size.', 'GJM' ),
					'type'        => 'text',
					'attributes'  => array( 'style' => 'width:600px' ) 
				),
				array(
					'name'        => $prefix.'_distance',
					'std'         => '1',
					'label'       => __( "Show Distance", 'GJM' ),
					'cb_label'    => __( 'Enable', 'GJM' ),
					'desc'        => __( 'Display the distance to each location in the list of results.', 'GJM' ),
					'type'        => 'checkbox',
					'attributes'  => array()
				),
						
			),
		);
	
		//single page options
		$args['gjm_single_page'] = array(
			__( 'Geo Single Page Map', 'GJM' ),
			array(
				array(
					'name'        => $prefix.'_single_map_enabled',
					'std'         => 'top',
					'label'       => __( 'Map Location', 'GJM' ),
					'desc'        => __( 'Select the location of the map.', 'GJM' ),
					'type'        => 'select',
					'attributes'  => array(),
					'options'	  => array( 
						'disabled' => __( 'Disabled', 'GMW' ),
						'top'	   => __( 'Top', 'GMW' ),
						'bottom'   => __( 'Bottom', 'GMW' )
					)
				),
				array(
					'name'        => $prefix.'_single_map_width',
					'type'		  => 'text',
					'std'         => '100%',
					'placeholder' => '',
					'label'       => __( 'Map Width', 'GJM' ),
					'desc'        => __( 'Map width in pixels or percentage (ex. 100% or 250px).', 'GJM' ),
					'attributes'  => array()
				),
				array(
					'name'        => $prefix.'_single_map_height',
					'type'		  => 'text',
					'std'         => '250px',
					'placeholder' => '',
					'label'       => __( 'Map Height', 'GJM' ),
					'desc'        => __( 'Map height in pixels or percentage (ex. 100% or 250px).', 'GJM' ),
					'attributes'  => array()
				),
				array(
					'name'        => $prefix.'_single_map_type',
					'std'         => 'ROADMAP',
					'label'       => __( 'Map Type', 'GJM' ),
					'desc'        => __( 'Select The Map Type.', 'GJM' ),
					'type'		  => 'select',
					'options'	  => array(
							'ROADMAP' 	=> __( 'ROADMAP' , 'GJM' ),
							'SATELLITE' => __( 'SATELLITE' , 'GJM' ),
							'HYBRID'    => __( 'HYBRID' , 'GJM' ),
							'TERRAIN'   => __( 'TERRAIN' , 'GJM' )
					),
				),
				array(
					'name'        => $prefix.'_single_map_location_marker',
					'type'		  => 'text',
					'std'         => 'https://maps.google.com/mapfiles/ms/icons/red-dot.png',
					'label'       => __( 'Map Icon', 'GJM' ),
					'desc'        => __( 'URL of a map icon file.', 'GJM' ),
					'attributes'  => array( 'size' => '50') 
				),
				array(
					'name'        => $prefix.'_single_map_scroll_wheel',
					'type'        => 'checkbox',
					'std'         => '1',
					'label'       => __( 'Zoom With Scrollwheel', 'GJM' ),
					'cb_label'    => __( 'Enable', 'GJM' ),
					'desc'        => __( 'Use the mouse scrollwheel to zoom in/out.', 'GJM' ),
					'attributes'  => array()
				),
				array(
					'name'        => $prefix.'_single_map_zoom_level',
					'type'		  => 'text',
					'std'         => '13',
					'label'       => __( 'Zoom Level', 'GJM' ),
					'desc'        => __( 'Enter a value between 1 to 20 that will be used as the initial zoom level.', 'GJM' ),
					'attributes'  => array( 'size' => '5') 
				),
				array(
					'name'        => $prefix.'_single_map_max_zoom_level',
					'type'		  => 'text',
					'std'         => '',
					'label'       => __( 'Maximum Zoom Level', 'GJM' ),
					'desc'        => __( 'Enter a value between 1 to 20 or leave blank for no zoom level limit.', 'GJM' ),
					'attributes'  => array( 'size' => '5') 
				),
			),
		);

		return $args;
	}

	/**
	 * Validate the updated values and return them to be saved in gjm_options single option.
	 * i like keeping all the options saved in a single array which can later be pulled with
	 * a single call to database instead of multiple calles per option
	 */
	function options_validate() {

		$prefix = $this->prefix;

		$valid_input = array();

		foreach ( $this->admin_settings( array() ) as $section_name => $section ) {
			
			$sn = str_replace( 'gjm_','', $section_name );

			foreach ( $section[1] as $option ) {

				if ( empty( $option['type'] ) ) {
					continue;				
				}
				
				switch ( $option['type'] ) {

					case "checkbox" :

						$valid_input[$sn][$option['name']] = $_POST[$option['name']] = ( !empty( $_POST[$option['name']] ) ) ? 1 : 0; 

					break;

					case "select" :

						if ( !empty( $_POST[$option['name']] ) && in_array( $_POST[$option['name']], array_keys( $option['options'] ) ) ) {
							$valid_input[$sn][$option['name']] = $_POST[$option['name']];
						} else {
							$valid_input[$sn][$option['name']] = $_POST[$option['name']] = ( !empty( $option['std'] ) ) ? $option['std'] : '';
						}

					break;

					case $prefix."_multiselect" :

						if ( ! empty( $_POST[$option['name']] ) && is_array( $_POST[$option['name']] ) ) {
							$valid_input[$sn][$option['name']] = $_POST[$option['name']];
						} else {
							$valid_input[$sn][$option['name']] = $_POST[$option['name']] = array();
						}

					break;

					case "text" :
						if ( !empty( $_POST[$option['name']] ) ) {
							$valid_input[$sn][$option['name']] = $_POST[$option['name']] = esc_attr( sanitize_text_field( $_POST[$option['name']] ) );
						} else {
							$valid_input[$sn][$option['name']] = $_POST[$option['name']] = ( !empty( $option['std'] ) ) ? $option['std'] : '';
						}
					break;
				}
			}
		}

		update_option( $prefix.'_options', $valid_input );
	}
}
new GJM_Admin_Settings;