<?php
/**
 * function wpjmbb_job_reference_field()
 *
 * adds a job reference field to the wp job manager plugin and
 * also to the add-on to add this to the feed.
 *
 * @param	array	$fields are the current job fields
 * @return	array	$fields is the modified fields
 */
function wpjmbb_job_reference_field( $fields ) {
	
	/* add the job reference field */
	$fields[ '_job_reference' ] = array(
		'label'			=> 'Job Reference',
		'description'	=> 'Unique to each job, the job reference is used for the edit/delete commands.',
		'type'			=> 'text'
	);
	
	return $fields;
	
}

add_filter( 'wpjmbb_fields', 'wpjmbb_job_reference_field' );
add_filter( 'job_manager_job_listing_data_fields', 'wpjmbb_job_reference_field' );

/**
 * function wpjmbb_job_company_logo_field()
 *
 * adds a job reference field to the wp job manager plugin and
 * also to the add-on to add this to the feed.
 *
 * @param	array	$fields are the current job fields
 * @return	array	$fields is the modified fields
 */
function wpjmbb_job_company_logo_field( $fields ) {
	
	/* add the job reference field */
	$fields[ '_company_logo' ] = array(
		'label'			=> 'Company Logo',
		'description'	=> 'URL to the company logo.',
		'type'			=> 'file'
	);
	
	return $fields;
	
}

add_filter( 'wpjmbb_fields', 'wpjmbb_job_company_logo_field' );