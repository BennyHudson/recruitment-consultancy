<?php
/**
 * function wpjmbb_sample_xml()
 *
 * builds sample xml output depending on the fields and
 * taxonomies the site has installed
 *
 * returns an xml formatted string
 */
function wpjmbb_sample_xml() {
	
	$w = new XMLWriter;
	$w->openMemory();
	$w->setIndent(true);
	$w->startElement('job');
	
	/* add node for username and password & command */
	$w->writeElement( 'command', 'add' );
	$w->writeElement( 'username', wpjmbb_get_username() );
	$w->writeElement( 'password', wpjmbb_get_password() );
	
	/* add node for the job title and description */
	$w->writeElement( 'job_title', 'Sample Job Title' );
	$w->writeElement( 'job_description', 'Can include html and should be wrapped in CDATA tags.' );

	/* add node for the company logo */
	$w->writeElement( 'company_logo', 'https://markwilkinson.me/content/uploads/2015/11/wp-job-manager-broadbean-graphic-715x499.png' );
	
	$job_fields = wpjmbb_get_fields();
	
	/* prepare the job fields array */
	$job_fields = wpjmbb_get_job_fields_for_xml_sample( $job_fields );
	
	/* check we have job fields to process */
	if( ! empty( $job_fields ) ) {
		
		/* loop through each field */
		foreach( $job_fields as $meta_key => $field ) {
			
			/* remove the underscrore from the first part of the key */
			$node = ltrim( $meta_key, '_' );
			
			/* change any dashes for underscore */
			$node = str_replace( '-', '_', $node );
			
			/* if this is the filled field - skip over it as this data is not sent */
			if( $node == 'filled' ) {
				continue;
			}
			
			/* if we have an xml sample value - use it */
			if( $field[ 'xml_sample_value' ] ) {
				$node_value = $field[ 'xml_sample_value' ];
			}
			
			/* no xml sample value for this field - use default */
			else {
				$node_value = 'Sample data value';
			}
			
			 $w->writeElement( $node, $node_value );
		
		}
		
	}
	
	/* get the taxonomy objects for jobs */
	$job_taxonomies = get_object_taxonomies(
		'job_listing',
		'names'
	);
	
	/* if we have terms */
	if( ! empty( $job_taxonomies ) ) {
	
		/* loop through each taxonomy */
		foreach( $job_taxonomies as $tax ) {
			
			/* write the xml node element for this tax */
			$w->writeElement( $tax, 'Freelance, Contract, Full Time' );
			
		}
	
	}
	
	$w->endElement();
	
	$output = $w->outputMemory();
	
	echo '<pre>', htmlentities( $output ), '</pre>';
		

}