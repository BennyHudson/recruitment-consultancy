<?php
/**
 * function wpjmbb_activate_license()
 *
 * activates a users license once enter into the settings screen
 * the user must click the activate button to make this work
 */
function wpjmbb_activate_license() {
	
	// listen for our activate button to be clicked
	if( isset( $_POST[ 'wpjmbb_license_activate' ] ) ) {
		
		// run a quick security check 
	 	if( ! check_admin_referer( 'wpjmbb_license_nonce', 'wpjmbb_license_nonce' ) )  {
		 	return; // get out if we didn't click the Activate button
	 	}	
		
		// retrieve the license from the database
		$license = trim( wpjmbb_get_license_key() );
		
		// data to send in our API request
		$api_params = array( 
			'edd_action'=> 'activate_license', 
			'license' 	=> $license, 
			'item_name' => urlencode( EDD_WPJMBB_ITEM_NAME ), // the name of our product in EDD
			'url'       => home_url()
		);
		
		// Call the custom API.
		$response = wp_remote_post(
			EDD_WPJMBB_STORE_URL,
			array(
				'timeout'   => 15,
				'sslverify' => false,
				'body'      => $api_params
			)
		);
		
		// make sure the response came back okay
		if ( is_wp_error( $response ) ) {
			return false;
		}
		
		// decode the license data
		$license_data = json_decode( wp_remote_retrieve_body( $response ) );
		
		// $license_data->license will be either "active" or "inactive"
		update_option( 'wpjmbb_license_key_status', $license_data->license );
	
	}
	
}

add_action( 'admin_init', 'wpjmbb_activate_license' );

/**
 * function wpjmbb_deactivate_license()
 *
 * activates a users license once enter into the settings screen
 * the user must click the activate button to make this work
 */
function wpjmbb_deactivate_license() {
	
	// listen for our activate button to be clicked
	if( isset( $_POST[ 'wpjmbb_license_deactivate' ] ) ) {
		
		// run a quick security check 
	 	if( ! check_admin_referer( 'wpjmbb_license_nonce', 'wpjmbb_license_nonce' ) )  {
		 	return; // get out if we didn't click the Activate button
	 	}	
		
		// retrieve the license from the database
		$license = trim( wpjmbb_get_license_key() );
		
		// data to send in our API request
		$api_params = array( 
			'edd_action'=> 'deactivate_license', 
			'license' 	=> $license, 
			'item_name' => urlencode( EDD_WPJMBB_ITEM_NAME ), // the name of our product in EDD
			'url'       => home_url()
		);
		
		// Call the custom API.
		$response = wp_remote_post(
			EDD_WPJMBB_STORE_URL,
			array(
				'timeout'   => 15,
				'sslverify' => false,
				'body'      => $api_params
			)
		);
		
		// make sure the response came back okay
		if ( is_wp_error( $response ) ) {
			return false;
		}
		
		// decode the license data
		$license_data = json_decode( wp_remote_retrieve_body( $response ) );
		
		// $license_data->license will be either "deactivated" or "failed"
		if( $license_data->license == 'deactivated' ) {
			
			// $license_data->license will be either "active" or "inactive"
			update_option( 'wpjmbb_license_key_status', $license_data->license );
			
		}
	
	}
	
}

add_action( 'admin_init', 'wpjmbb_deactivate_license' );