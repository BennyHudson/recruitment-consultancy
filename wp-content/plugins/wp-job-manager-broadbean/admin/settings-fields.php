<?php
/**
 * function wpjmbb_setting_sample_xml_output()
 *
 * outputs the sample xml for the plugin on the settings page
 * this is what can be used to setup the feed
 */
function wpjmbb_setting_sample_xml_output( $option, $attributes, $value, $placeholder ) {
	
	?>
	<div style="background-color: #FFFFFF; padding: 0 16px; border: 1px solid #ddd; font-family: Consolas,Monaco,monospace;">
		<?php echo wpjmbb_sample_xml(); ?>
	</div>
	<?php
	
	if ( $option['desc'] ) {
		
		?>
			
		<p class="description"><?php echo $option['desc']; ?></p>
		<p class="description">The following information is provided for the default fields that require a specific data type:</p>
		
		<div style="margin-top: 16px;" class="description">
			
			<ul>
				<li><code>job_description</code>: The description can contain HTML but must be sent wrapped in CDATA tags.</li>
				<li><code>application</code>: This is where the application tracking email address or application tracking URL should be posted to.</li>
				<li><code>company_website</code>: This should be a valid URL.</li>
				<li><code>company_logo</code>: This should be a valid URL to an image. This image will be pulled into your WordPress media library.</li>
				<li><code>company_website</code>: This should be a valid URL to a video file or a link to a video on social sharing services e.g. Youtube.</li>
				<li><code>featured</code>: This should be a boolean value where 1 (true) is featured and 0 (false) is not.</li>
				<li><code>job_expires</code>: This should be a valid date in the format YYYY-MM-DD.</li>
				<li><code>job_author</code>: This should be the ID of the WordPress user who is posting the job. Send a value of -1 to post as a Guest author.</li>
			</ul>
			
		</div>
		
		<?php

	}
	
}

add_action( 'wp_job_manager_admin_field_sample_xml', 'wpjmbb_setting_sample_xml_output', 10, 4 );

/**
 * function wpjmbb_setting_just_text_output()
 *
 * controls the output for settings fields of type just_text
 */
function wpjmbb_setting_just_text_output( $option, $attributes, $value, $placeholder ) {
	
	if ( $option[ 'std' ] ) {
		echo '<div style="background-color: #F7F7F7; padding: 3px 5px; border: 1px solid #ddd; display: inline-block; width:25em;">' . esc_attr( $option[ 'std' ] ) . '</div>';
	}

	if ( $option['desc'] ) {
		echo ' <p class="description">' . $option['desc'] . '</p>';
	}
	
}

add_action( 'wp_job_manager_admin_field_just_text', 'wpjmbb_setting_just_text_output', 10, 4 );

/**
 * function function wpjmbb_setting_no_input_output()
 *
 * controls the output for settings fields of type no_input
 */
function wpjmbb_setting_no_input_output( $option, $attributes, $value, $placeholder ) {

	if ( $option['desc'] ) {
		echo ' <p class="description">' . $option['desc'] . '</p>';
	}
	
}

add_action( 'wp_job_manager_admin_field_no_input', 'wpjmbb_setting_no_input_output', 10, 4 );

/**
 * function wpjmbb_license_field()
 *
 * field type for adding the license to the settings tab
 * creates the license input as a password input and also adds
 * a submit button to activate the license.
 * the activate button only shows if a license is saved
 */
function wpjmbb_license_field( $option, $attributes, $value, $placeholder ) {
	
	?>
	
	<input id="setting-<?php echo $option['name']; ?>" class="regular-text" type="password" name="<?php echo $option['name']; ?>" value="<?php esc_attr_e( $value ); ?>" <?php echo implode( ' ', $attributes ); ?> <?php echo $placeholder; ?> />
	
	<?php
	
	/* add a nonce field to check for on activation of the license */
	wp_nonce_field( 'wpjmbb_license_nonce', 'wpjmbb_license_nonce' );
		
	/* get the license key and status */
	$license		= wpjmbb_get_license_key();
	$license_status = wpjmbb_get_license_key_status();
	
	/* if we have a license key and the status in no valid */
	if( $license != false && $license_status != 'valid' ) {
		
		/* output the activate license button */
		?>
		<input type="submit" class="button-secondary" name="wpjmbb_license_activate" value="<?php _e( 'Activate License' ); ?>"/>
		<?php
		
	}
	
	/* we have an active valid license */
	elseif( $license != false && $license_status != 'invalid' ) {
		
		/* output the deactivate license button */
		?>
		<input type="submit" class="button-secondary" name="wpjmbb_license_deactivate" value="<?php _e( 'Deactivate License' ); ?>"/>
		<?php
		
	}
		

	if ( $option['desc'] ) {
		echo ' <p class="description">' . $option['desc'] . '</p>';
	}
	
}

add_action( 'wp_job_manager_admin_field_license', 'wpjmbb_license_field', 10, 4 );