<?php
/* exist if directly accessed */
if( ! defined( 'ABSPATH' ) ) exit;

/**
 * function wpjmbb_add_settings_tab()
 *
 * adds settings for the integrations username and password
 * and the sample xml and instructional fields
 */
function wpjmbb_add_settings_tab( $settings ) {
	
	/* add our new settings in its own tab */
	$settings[ strtolower( apply_filters( 'wpjmbb_integration_service_name', 'Broadbean' ) ) ] = array(
		apply_filters( 'wpjmbb_integration_service_name', 'Broadbean' ),
		array(
			array(
				'name'			=> 'wpjmbb_license_key',
				'std'			=> '',
				'placeholder'	=> 'License key',
				'label'			=> __( 'License Key', 'wp-job-manager' ),
				'desc'			=> __( 'Enter your plugin license key for auto updates and support features. Once saved you will need to activate it using the activate button which appears to the right.', 'wp-job-manager' ),
				'type'			=> 'license',
				'attributes'	=> array()
			),
			array(
				'name'			=> 'job_manager_bb_username',
				'std'			=> '',
				'placeholder'	=> 'Username',
				'label'			=> __( 'Username', 'wp-job-manager' ),
				'desc'			=> __( 'Enter a username for your feed. This username is used to authenticate when a job is sent.', 'wp-job-manager' ),
				'attributes'	=> array()
			),
			array(
				'name'			=> 'job_manager_bb_password',
				'std'			=> '',
				'placeholder'	=> 'Username',
				'label'			=> __( 'Password', 'wp-job-manager' ),
				'desc'			=> __( 'Enter a password for your feed. Letters only (upper and lower is best). This username is used to authenticate when a job is sent.', 'wp-job-manager' ),
				'attributes'	=> array()
			),
			array(
				'name'			=> 'job_manager_bb_logging',
				'std'			=> '0',
				'label'			=> __( 'Debug Logging', 'wp-job-manager' ),
				'cb_label'   	=> __( 'Tick to enable debug logging.', 'wp-job-manager' ),
				'desc'			=> __( 'When checked the incoming raw feed data is saved with each job.', 'wp-job-manager' ),
				'type'			=> 'checkbox',
				'attributes'	=> array()
			),
			array(
				'name'			=> 'job_manager_bb_settings_title',
				'std'			=> '',
				'label'			=> __( '', 'wp-job-manager' ),
				'desc'			=> __( 'When setting up your feed to your site, the feed provider (e.g. Broadbean) will require the information outlined below.' ),
				'type'			=> 'no_input',
				'attributes'	=> array()
			),
			array(
				'std'			=> home_url( '/' ) . '?wpjmbb=' . strtolower( apply_filters( 'wpjmbb_integration_service_name', 'broadbean' ) ),
				'label'			=> __( 'Posting URL', 'wp-job-manager' ),
				'desc'			=> __( 'The XML should be posted to this URL.', 'wp-job-manager' ),
				'type'			=> 'just_text',
				'attributes'	=> array()
			),
			array(
				'label'			=> 'Required XML',
				'desc'			=> __( 'Your site needs to receive an XML feed posted to the URL above and in the format above. The sample values indicate the way in which the data needs to sent.' ),
				'type'			=> 'sample_xml',
				'attributes'	=> array()
			)
		)
	);
	
	return $settings;
	
}

add_filter( 'job_manager_settings', 'wpjmbb_add_settings_tab' );