-------------------------------------------------------------------------------------------------------------------
Version 0.2.2 (02/03/2016)
	- Company logo now uses the featured image of a job listing post.
	- (15/03/2016) - changed store location constant to new Highrise Store URL for updates to continue working

-------------------------------------------------------------------------------------------------------------------
Version 0.2.1 (07/01/2016)
	- Correct the wpjmbb_get_job_by_reference() which has a reference hard coded into the function

-------------------------------------------------------------------------------------------------------------------
Version 0.2 (22/12/2015)
	- Different default company logo in sample XML - that doesn't 404!
	- Function to get a job based on its job reference, used for the deleting of a job
	- Add a job reference field to jobs and sample XML
	- Allow command (add/delete) to be sent with XML from poster
	- Command allows deleting of jobs to be done from the poster
	
-------------------------------------------------------------------------------------------------------------------
Version 0.1.4 (16/11/2015)
	- Correct sending email from the applicant and to the tracking email address when using the application add-on.
	- HOTFIX - if download_url() function does not get a valid URL, make function fail nicely.
	
-------------------------------------------------------------------------------------------------------------------
Version 0.1.3 (11/11/2015)
	- Filterable strings for service name. Allows changing the "broadbean" string to something else.

-------------------------------------------------------------------------------------------------------------------
Version 0.1.2 (05/11/2015)
	- Get the license key correctly in the updater class

-------------------------------------------------------------------------------------------------------------------
Version 0.1.1 (05/11/2015)
	- Update to allow a user to deactivate their license from the settings screen
	- Removed the readme.md file which is not needed

-------------------------------------------------------------------------------------------------------------------
Version 0.1 (04/11/2015)
	- Initial plugin release.