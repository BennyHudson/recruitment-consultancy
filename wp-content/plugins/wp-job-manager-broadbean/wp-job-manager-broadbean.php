<?php
/*
Plugin Name: WP Job Manager Broadbean Addon
Plugin URI: http://wpbroadbean.com/wp-job-manager-broadbean-add-on/
Description: Integrates Broadbean job posting with the WP Job Manager plugin. Requires some setup with Broadbean. A plugin from the developers of <a href="wpbroadbean.com">wpbroadbean.com</a>.
Version: 0.2.3
Author: Mark Wilkinson
Author URI: http://markwilkinson.me
License: GPLv2 or later

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
*/

/* exist if directly accessed */
if( ! defined( 'ABSPATH' ) ) exit;

/* define variable for path to this plugin file. */
define( 'WPJMBB_LOCATION', dirname( __FILE__ ) );

// this is the URL our updater / license checker pings. This should be the URL of the site with EDD installed
define( 'EDD_WPJMBB_STORE_URL', 'https://store.highrise.digital' );

// the name of your product. This is the title of your product in EDD and should match the download title in EDD exactly
define( 'EDD_WPJMBB_ITEM_NAME', 'WP Job Manager Broadbean Add-on' );

/* check if the EDD plugin updater class is already available */
if( !class_exists( 'EDD_SL_Plugin_Updater' ) ) {
	
	// load our custom updater if it doesn't already exist 
	include( dirname( __FILE__ ) . '/inc/EDD_SL_Plugin_Updater.php' );
	
}

/**
 * function wpjmbb_plugin_updater()
 *
 * allows auto updates
 */
function wpjmbb_plugin_updater() {
	
	// retrieve our license key from the DB
	$license_key = trim( wpjmbb_get_license_key() );
	 
	// setup the updater
	$edd_updater = new EDD_SL_Plugin_Updater(
		EDD_WPJMBB_STORE_URL,
		__FILE__,
		array(
			'version'	=> '0.2.3',		// current version number
			'license' 	=> $license_key,	// license key (used get_option above to retrieve from DB)
			'item_name' => EDD_WPJMBB_ITEM_NAME,	// name of this plugin
			'author' 	=> 'Mark Wilkinson',	// author of this plugin
			'url'       => home_url()
		)
	);
	
}

add_action( 'admin_init', 'wpjmbb_plugin_updater' );

/* load required files & functions */
require_once( dirname( __FILE__ ) . '/admin/settings.php' );
require_once( dirname( __FILE__ ) . '/admin/settings-fields.php' );
require_once( dirname( __FILE__ ) . '/admin/samplexml.php' );
require_once( dirname( __FILE__ ) . '/admin/admin.php' );
require_once( dirname( __FILE__ ) . '/admin/fields.php' );
require_once( dirname( __FILE__ ) . '/inc/wpjmbb-functions.php' );

/**
 * Function wpjmbb_add_new_query_var()
 * adds a new query var to wordpress which is included in the main query
 */
function wpjmbb_add_new_query_var( $wpjmbb_public_query_vars ) {
    
    /* set a name for our new query var */
    $wpjmbb_public_query_vars[] = 'wpjmbb';
    
    /* return the new name to the query_vars filter */
    return $wpjmbb_public_query_vars;
    
}

add_filter( 'query_vars', 'wpjmbb_add_new_query_var', 99 );

/**
 * Function wpjmbb_inbox_load()
 * Loads the broadbean inbox template php file when the above
 * query var is called. When a user navigates their browser to
 * yoursite.com/?wpjmbb=brodbean it loads page using the
 * inbox.php template form this plugin folder.
 */
function wpjmbb_inbox_load() {
    
    /* get the query var we named above from the url */
    $wpjmbb_bb = get_query_var( 'wpjmbb' );
    
    /* make the query var value filterable */
    $wpjmbb_query_var_value = strtolower( apply_filters( 'wpjmbb_integration_service_name', 'broadbean' ) );
    
    /* check that its value is equal to adcourier */
    if( $wpjmbb_bb == $wpjmbb_query_var_value ) {
    
    	/* check for a dashboard content file in the theme folder */
		if( file_exists( STYLESHEETPATH . '/job_manager/' . sanitize_title( strtolower( apply_filters( 'wpjmbb_integration_service_name', 'broadbean' ) ) ) . '/inbox.php' ) ) {

			/* load the dashboard content file from the theme folder */
			require_once STYLESHEETPATH . '/job_manager/' . sanitize_title( strtolower( apply_filters( 'wpjmbb_integration_service_name', 'broadbean' ) ) ) . '/inbox.php';

		} else {

			/* load the adcourier inbox file */
			require_once( dirname( __FILE__ ) . '/inc/inbox.php' );

		}
        
        /* stop wordpress loading any further */
        exit;
    
    }
    
}

/* redirect the user to our adcourier inbox file when correct query var and value are inputted */
add_action( 'template_redirect', 'wpjmbb_inbox_load' );