<?php

/* exist if directly accessed */
if( ! defined( 'ABSPATH' ) ) exit;

/**
 * function wpjmbb_prepare_terms()
 * 
 * prepare terms sent via the broadbean feed to adding to a job post once created
 * returns an array of mixed term ids and terms names
 *
 * hierarchical taxonomies have term ids added whereas non hierarchical taxonomies have term name added 
 * @param (string)	$sent_terms are the terms sent via the broadbean feed for this taxonomy
 * @param (string)	$taxonomy is the taxomony name associated with the terms in $sent_terms
 * @return (array)	$wpjmbb_tax_terms is an array of terms grouped by term
 */
function wpjmbb_prepare_terms( $sent_terms, $taxonomy ) {
	
	if ( empty( $sent_terms ) ) {
		return;
	}
	
	/* turn category terms into arrays */
	$wpjmbb_tax = wp_strip_all_tags( $sent_terms );
	$wpjmbb_taxonomy_terms = explode( ',', $wpjmbb_tax );

	/* setup array to store the category term ids in */
	$wpjmbb_tax_terms = array();

	/* loop through each term in array getting its id */
	foreach( $wpjmbb_taxonomy_terms as $wpjmbb_taxonomy_term ) {
		
		/* 	check whether the term exists, and return its ID if it does, 
			if it doesn't exist then create it 
			either way add it to our array 
		*/
		/* check whether the term exists */
		if ( $term_id = term_exists( $wpjmbb_taxonomy_term, $taxonomy ) ) {
			
			/* is this taxonomy hierarchical */
			if( is_taxonomy_hierarchical( $taxonomy ) ) {
				
				/* add to term id to our terms array */
				$wpjmbb_tax_terms[] = $term_id[ 'term_id' ];
			
			/* non hierarchical taxonomy */
			} else {
				
				/* add to term id to our terms array */
				$wpjmbb_tax_terms[] = $wpjmbb_taxonomy_term;
				
			}
		
		/* term does not exist */
		} else {
			
			/* add the term */
			$new_term = wp_insert_term(
				$wpjmbb_taxonomy_term, // term to insert
				$taxonomy, // taxonomy to add the term to
				array(
					'slug' => sanitize_title( $wpjmbb_taxonomy_term )
				)
			);
			
			/* is this taxonomy hierarchical */
			if( is_taxonomy_hierarchical( $taxonomy ) ) {
				
				/* add to term id to our terms array */
				$wpjmbb_tax_terms[] = $new_term[ 'term_id' ];
			
			/* non hierarchical taxonomy */
			} else {
				
				/* get the term name from its id */
				$new_term_obj = get_term_by(
					'id',
					$new_term['term_id'],
					$taxonomy
				);
				
				/* add to term name to our terms array */
				$wpbb_tax_terms[] = $new_term_obj->name;
				
			}
		
		}
		
		
	} // end loop through each term
	
	return $wpjmbb_tax_terms;
	
}

/**
 * function wpjmbb_get_username()
 *
 * retreives the username assigned for the broadbean feed
 * this username is set in the Broadbean tab of the setttings page
 */
function wpjmbb_get_username() {
	return get_option( 'job_manager_bb_username' );
}

/**
 * function wpjmbb_get_password()
 *
 * retreives the passsword assigned for the broadbean feed
 * this password is set in the Broadbean tab of the setttings page
 */
function wpjmbb_get_password() {
	return get_option( 'job_manager_bb_password' );
}

/**
 * function wpjmbb_get_debug_logging()
 *
 * retreives the debug logged option from settings
 * this password is set in the Broadbean tab of the setttings page
 */
function wpjmbb_get_debugging() {
	return get_option( 'job_manager_bb_logging' );
}

/**
 * function wpjmbb_get_license_key()
 *
 * retreives the license key
 */
function wpjmbb_get_license_key() {
	return get_option( 'wpjmbb_license_key' );
}

/**
 * function wpjmbb_get_license_key_status()
 *
 * retreives the license key status - active inactive
 */
function wpjmbb_get_license_key_status() {
	return get_option( 'wpjmbb_license_key_status' );
}

/**
 * function wpjmbb_get_fields()
 *
 * retrieves all of the job fields to save
 * this includes the defauly fields added by the plugin
 * and also any custom ones added by developers
 */
function wpjmbb_get_fields() {
	
	/* if the class WP_Job_Manager_Writepanels does not already exist - maybe on the front-end */
	if( ! class_exists( 'WP_Job_Manager_Writepanels' ) ) {
		
		/* include the job manager plugin write panels class */
		require JOB_MANAGER_PLUGIN_DIR . '/includes/admin/class-wp-job-manager-writepanels.php';
		
	}
	
	/* create a n new instance of the write panels class */
	$fields = new WP_Job_Manager_Writepanels();
	
	/* get the fields */
	$fields = $fields->job_listing_fields();
	
	/* return the fields */
	return apply_filters( 'wpjmbb_fields', $fields );
	
}

/**
 * function wpjmbb_prepare_job_fields()
 *
 * do some housekeeping on some fields to make then work nicely
 */
function wpjmbb_prepare_job_fields( $fields ) {
	
	/* set the company video field to a text type so a URL can be passed */
	$fields[ '_company_video' ][ 'type' ] = 'text';
	
	/* add fields which by default don't show for a non logged in user */
	$fields[ '_featured' ] = array();
	$fields[ '_job_expires' ] = array();
	
	return $fields;
	
}

/**
 * function wpjmbb_get_job_fields_for_xml_sample()
 *
 * gets an array of all the job fields ready for using
 * to output the sample xml
 */
function wpjmbb_get_job_fields_for_xml_sample() {
	
	/* get the job fields */
	$job_fields = wpjmbb_get_fields();
	
	/* prepare the job fields */
	$job_fields = wpjmbb_prepare_job_fields( $job_fields );
	
	/* loop through each field */
	foreach( $job_fields as $field_key => $field_array ) {
		
		/**
		 * switch via field key depending on the key
		 * setting an appropriate sample value for the xml node
		 */
		switch( $field_key ) {
			
			case '_company_website' :
				
				$xml_sample_value = 'http://domain.com/';
				
				/* break out of switch statement */
				break;
			
			case '_company_logo' :
				
				$xml_sample_value = 'http://markwilkinson.me/content/uploads/2015/11/wp-job-manager-broadbean-graphic-715x499.png';
				
				/* break out of switch statement */
				break;
			
			case '_company_video' :
				
				$xml_sample_value = 'http://html5demos.com/assets/dizzy.mp4';
				
				/* break out of switch statement */
				break;
			
			case '_company_twitter' :
				
				$xml_sample_value = '@wpmark';
				
				/* break out of switch statement */
				break;
			
			case '_job_expires' :
				
				$xml_sample_value = '2015-12-01';
				
				/* break out of switch statement */
				break;
				
			case '_featured' :
				
				$xml_sample_value = 1;
				
				/* break out of switch statement */
				break;
				
			case '_job_location' :
				
				$xml_sample_value = 'Placename e.g. London';
				
				/* break out of switch statement */
				break;
			
			case '_application' :
				
				$xml_sample_value = 'job.tracking@email-address.com or http://job-application-url.com/';
				
				/* break out of switch statement */
				break;
			
			case '_job_author' :
				
				$xml_sample_value = 1;
				
				/* break out of switch statement */
				break;
			
			default:
				$xml_sample_value = 'Sample data value';
			
		}
		
		$job_fields[ $field_key ][ 'xml_sample_value' ] = $xml_sample_value;
		
	}
	
	return $job_fields;
	
}

/**
 * function wpbb_get_job_by_reference()
 * gets the job post id for a job given its reference
 * @param (string) $job_ref is the job refrerence to check by
 * @return (int/false) $post_id returns the post of the job if a job exists with that reference or false
 */
function wpjmbb_get_job_by_reference( $job_ref ) {
		
	/* get posts according to args above */
	$jobs = new WP_Query(
		array(
			'post_type'		=> 'job_listing',
			'post_status'	=> 'all',
			'meta_key'		=> '_job_reference',
			'meta_value'	=> $job_ref,
			'relation'		=> 'AND',
			'fields'		=> 'ids'
		)
	);
	
	/* get an array of post ids for jobs returned */
	$jobs = $jobs->posts;
	
	/* reset query */
	wp_reset_query();
	
	/* check we have some jobs */
	if( empty( $jobs ) ) {
		return false;
	}
	
	/* return the post id of the job matching the reference */
	return array_shift( $jobs );
	
}

/**
 * function wpjmbb_email_application_to_tracking_address()
 *
 * Send an email to the Broadbean tracking address on each application
 */
function wpjmbb_email_application_to_tracking_address( $headers, $job_id, $application_id ) {
	
	/* get the tracking address */
	$tracking_email = get_post_meta( $job_id, '_application', true );
	
	/* set new headers and return them */
	$headers[] = 'Cc:' . $tracking_email;
	return $headers;
	
}

add_filter( 'create_job_application_notification_headers', 'wpjmbb_email_application_to_tracking_address', 10, 3 );