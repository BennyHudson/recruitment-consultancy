<?php
/**
 * start by getting the stored user name and password used to the feed
 */
$username = wpjmbb_get_username();
$password = wpjmbb_get_password();

/* set logging to false - filterable */
$logging = wpjmbb_get_debugging();

/**
 * get the contents of the feed provided by adcourier
 * if you want to test the feed, by pulling an XML feed from a specific url
 * you can use the wpjmbb_xml_feed_url filter
 */
$xml_content = file_get_contents( apply_filters( 'wpjmbb_xml_feed_url', 'php://input' ) );

/* if we have no xml content */
if( $xml_content == false || $xml_content == '' ) {
	
	/* through an error and die */
	wp_die( __( '<p>Error: Invalid XML or no XML supplied.</p>' ) );
	
}

/**
 * turn the raw posted data into a more usable object
 * each xml node is now an property of the object
 */
$xml_params = simplexml_load_string( $xml_content );

/**
 * before we go any further - lets authenticate
 * check the username / password sent matches that stored
 */
if( wp_strip_all_tags( (string) $xml_params->username ) != $username || wp_strip_all_tags( (string) $xml_params->password != $password ) ) {
	
	/* username and/or password are not correct, show an error message and stop file loading any further */
	wp_die( __( '<p>Error: Sorry username and/or password are not valid.</p>' ) );

} // if end username/password authenticate

/* get the command for this job - either add/delete/edit */
$command = wp_strip_all_tags( (string) $xml_params->command );

/* we are now authenticated there check which command we should be doing */
if( $command == 'add' || $command == '' ) {
	
	/**
	 * prepare the taxonomy terms to add
	 * defaults are for job_listing_category and job_listing_type taxonomies
	 * start by building a filterable array of taxonomy to handle
	 */
	
	/* disprove later when checking if there are any to add */
	$have_tax_terms = false;
	
	/* get the taxonomy objects for jobs - just their names */
	$job_taxonomies = get_object_taxonomies(
		'job_listing',
		'names'
	);
	
	/* if we have taxonomies */
	if( ! empty( $job_taxonomies ) ) {
		
		/* set up holding array to fill with terms*/
		$tax_terms = array();
		
		/** 
		 * loop through each of the taxonomies we have preparing it for adding to the post
		 * this result in array like so:
		 * array(
		 *		'job_listing_category'	=> array(
		 *				'General',
		 *				'IT'
		 *		),
		 *		'job_listing_type'		=> array(
		 *				'Temporary'
		 *		)
		 * )
		 */
		foreach( $job_taxonomies as $taxonomy ) {
			
			/* check the xml has content */
			if( $xml_params->$taxonomy != '' ) {
				
				/* add the prepared terms to our terms array */
				$tax_terms[ $taxonomy ] = wpjmbb_prepare_terms( $xml_params->$taxonomy, $taxonomy );
				
				$have_tax_terms = true;
				
			}
			
		} // end loop through taxonomies
		
	} // end if have taxonomies
	
	/**
	 * check which author to set
	 * if -1 was sent then we set no post author
	 * otherwise set author to the sent id
	 */
	
	/* start by getting the author */
	$sent_author = wp_strip_all_tags( $xml_params->job_author );
	
	if( $sent_author == -1 ) {
		$author = 0;
	} else {
		$author = $sent_author;
	}
	
	/**
	 * lets now insert the actual post into wordpress
	 * uses the wp_insert_post function to do this
	 * if this works it will return the post id of the job added
	 */
	$job_post_id = wp_insert_post(
		apply_filters(
			'wpjmbb_new_job_post_args',
			array(
				'post_type'	=> 'job_listing',
				'post_title'	=> wp_strip_all_tags( (string) $xml_params->job_title ),
				'post_content'	=> wp_kses_post( $xml_params->job_description ),
				'post_status' 	=> 'publish',
				'post_author'	=> (int) $author
			),
			$xml_params
		)
	);
	
	/**
	 * lets check that the job was added
	 * checking for a job id present in the variable
	 */
	if( $job_post_id != 0 ) {
		
		/**
		 * if wpbb logging is set to true the plugin will save the raw incoming XML feed
		 * as post meta for this job with the key being _wpjmbb_raw_bb_feed
		 * it also adds the date before the XML output.
		 */
		if( $logging == true ) {
			
			$debug_content = array();
			
			/* combine the XML with the current date stamp */
			$debug_content[ 'sent_date' ] = date( 'd:m:Y H:i:s' );
			$debug_content[ 'sent_xml' ] = $xml_content;
			
			/* lets save the raw posted data in post meta for this job */
			add_post_meta(
				$job_post_id, // this is id of the job we have just added
				'_wpjmbb_raw_feed', // this is the meta key to store the post meta in
				$debug_content, // this is value to store - sent from broadbean/logicmelon
				true
			);
			
		} // end if we should be logging errors
			
		/**
		 * handle assign taxonomy terms to the newly added job post
		 * check we have a job type to add
		 */
		if( $have_tax_terms == true ) {
			
			$terms_added = array();
			
			/**
			 * job was added successfully
			 * start by looping through the tax terms ids to add to this job
			 */
			foreach( $tax_terms as $tax => $terms ) {
				
				$terms_added[ $tax ] = wp_set_post_terms(
					(int) $job_post_id,
					$terms,
					$tax
				);
	
				/**
				 * @hook wpjmbb_job_term_added
				 * fires after the term has been added
				 * @param (int) $job_post_id is the post id for the added job
				 * @param (array) $terms terms to be added
				 * @param (string) $tax taxonomy of the term
				 */
				do_action( 'wpjmbb_job_term_added', $job_post_id, $terms, $tax );
				
			} // end loop through terms
			
		}
		
		/* is the sidebload image function around */
		if ( ! function_exists( 'media_handle_sideload' ) ) {
			
			require_once( ABSPATH . 'wp-admin/includes/media.php') ;
			require_once( ABSPATH . 'wp-admin/includes/file.php' );
			require_once (ABSPATH . 'wp-admin/includes/image.php' );
		
		} // end if have sideload function
		
		/**
		 * get all of the fields added by the core plugin
		 * and third party plugins
		 */
		$job_fields = wpjmbb_get_fields();
		
		/* prepare the job fields array */
		$job_fields = wpjmbb_prepare_job_fields( $job_fields );
		
		/* check we have job fields to process */
		if( ! empty( $job_fields ) ) {
			
			/* loop through each field */
			foreach( $job_fields as $meta_key => $field ) {
				
				/* remove the underscrore from the first part of the key */
				$bb_field = ltrim( $meta_key, '_' );
				
				/* change any dashes for underscore */
				$bb_field = str_replace( '-', '_', $bb_field );
				
				/* lets check that the field sent is not empty */
				if( $xml_params->$bb_field != '' ) {
					
					/* get the meta value */
					$value = (string) wp_strip_all_tags( $xml_params->$bb_field );
					
					/* check we have a type set */
					if( isset( $field[ 'type' ] ) ) {
						
						/* if this is an upload field */
						if( $field[ 'type' ] == 'file' ) {
							
							/* get the url from the posted xml field */
							$url = $xml_params->$bb_field;
							
							/* perform a download on the url */
						    $tmp = download_url( $url );
						    
						    /* check for any download errors - for example if the url is a 404 */
						    if( ! is_wp_error( $tmp ) ) {
							    
							    $file_array = array(
							        'name' => basename( $url ),
							        'tmp_name' => $tmp
							    );
							
							    // Check for download errors
							    if ( is_wp_error( $tmp ) ) {
							        @unlink( $file_array[ 'tmp_name' ] );
							        return $tmp;
							    }
							
							    $id = media_handle_sideload( $file_array, $job_post_id );
							    // Check for handle sideload errors.
							    if ( is_wp_error( $id ) ) {
							        @unlink( $file_array['tmp_name'] );
							        return $id;
							    }
								
								/* get the url from the newly upload file */
								$value = wp_get_attachment_url( $id );
								
								/* if this is the company logo */
								if( $bb_field == 'company_logo' ) {

									/* set the attachment as the featured image */
									update_post_meta( $job_post_id, '_thumbnail_id', $id );

								}
							   
							/* downloading the url has failed */
						    } else {
							    
							    /* set the value to an empty string - save this as post meta instead of value sent */
							    $value = '';
							    
						    }
						
						} // end if field type is file
																	
					} // end if has a field type
	
					/* add the meta data */
					update_post_meta( $job_post_id, $meta_key, $value );
					
					/**
					 * @hook wpjmbb_job_field_added
					 * fires after the field has been added
					 * @param (int) $job_post_id is the post id for the added job
					 * @param (array) $field term to be added
					 */
					do_action( 'wpjmbb_job_field_added', $job_post_id, $field );
					
				} // end if a field was sent
				
			} // end loop through job fields
			
		} // end if have job fields
		
		/**
		 * everything appears to have worked
		 * therefore lets output a success message
		 */
		echo apply_filters( 'wpjmbb_job_added_success_message', 'Success: This Job has been added and has a post ID of ' . $job_post_id . '. The permalink to this job is: ' . get_permalink( $job_post_id ), $job_post_id );
		
	/**
	 * wp_insert_post returned zero
	 * this means the post was not added
	 */
	} else {
		
		/* output a error to the indicate the problem */
		echo apply_filters( 'wpjmbb_job_added_failure_message', 'Error: There was an error, the job was not published.', $job_post_id );
				
	} // end if have job id
	
}

/* if the command is to delete this job */
elseif( $command == 'delete' ) {
	
	/* get the sent job reference */
	$job_reference = wp_strip_all_tags( (string) $xml_params->job_reference );
	
	/* get the job id of the job that has the sent reference */
	$job_id = wpjmbb_get_job_by_reference( $job_reference );
	
	/* if we have a job matching the sent reference */
	if( $job_id != false ) {
		
		/* setup string of deleted posts */
		$deleted_posts = '<p>Post or posts deleted. The following post or posts were deleted: ';
		
		/* delete the post */
		$deleted = wp_delete_post( $job_id );
		
		/* add to delete posts string */
		$deleted_posts .= $job_id . ' | Job Reference: ' . $job_reference;
	
		/* output confirmation message */
		echo $deleted_posts . '</p>';
		
	} else {
		
		/* command not regonised, show an error message and stop file loading any further */
		wp_die( __( '<p>Error: A delete command was sent, however the job to delete cannot be found. The job reference sent, does not match a job on this site.</p>' ) );
		
	}
	
}

/* stop any further loading */
die();