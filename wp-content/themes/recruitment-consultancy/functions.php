<?php
	 
	add_action( 'after_setup_theme', 'wedo_theme_setup' );
	function wedo_theme_setup() {
		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'post-thumbnails' );
		add_action( 'widgets_init', 'wedo_register_sidebars' );
		add_filter('wp_mail_from_name', 'new_mail_from_name');
		add_action( 'init', 'register_my_menus' );
		add_action( 'login_enqueue_scripts', 'my_login_logo' );
		add_action('login_head', 'add_favicon');
		add_action('admin_head', 'add_favicon');
		add_action('init', 'wedo_posts');
		add_action('init', 'wedo_taxonomies');
		add_action('init', 'flush_rewrite_rules');
		add_action( 'wp_enqueue_scripts', 'wedo_scripts' );
		add_action('wp_before_admin_bar_render', 'annointed_admin_bar_remove', 0);
		add_filter( 'wpseo_metabox_prio', function() { return 'low';});
		add_image_size( 'gallery-thumb', 300, 300, true );
		add_image_size( 'post-feature', 900, 450, true );
		add_image_size( 'page-flood', 2000, 640, true );

		// Broadbean Fields
		add_filter( 'job_manager_job_listing_data_fields', 'admin_add_job_fields' );
		add_action( 'single_job_listing_meta_end', 'display_job_salary_data' );
		add_action( 'job_listing_meta_end', 'display_job_salary_data' );
		add_filter( 'submit_resume_form_fields', 'remove_submit_resume_form_fields' );
		add_filter( 'resume_manager_resume_fields', 'wpjms_admin_resume_form_fields' );
		add_filter( 'submit_resume_form_fields', 'wpjms_frontend_resume_form_fields' );
		add_filter( 'apply_with_resume_email_message', 'wpjms_color_field_email_message', 10, 2 );
		add_filter( 'submit_resume_form_fields', 'resume_file_required' );

	}

	get_template_part('functions/include', 'favicons');
	get_template_part('functions/include', 'postnav');
	get_template_part('functions/include', 'menus');
	get_template_part('functions/include', 'scripts');
	get_template_part('functions/include', 'sidebar');
	get_template_part('functions/include', 'cpts');
	get_template_part('functions/include', 'users');
	get_template_part('functions/include', 'email');
	get_template_part('functions/include', 'footer');
	get_template_part('functions/include', 'gallery'); 

	//Job Content
	get_template_part('functions/include', 'salary');
	get_template_part('functions/include', 'jobfields');
	get_template_part('functions/include', 'resumefields');

	remove_action('wp_head', 'rsd_link');
	remove_action('wp_head', 'wp_generator');
	remove_action('wp_head', 'feed_links', 2);
	remove_action('wp_head', 'index_rel_link');
	remove_action('wp_head', 'wlwmanifest_link');
	remove_action('wp_head', 'feed_links_extra', 3);
	remove_action('wp_head', 'start_post_rel_link', 10, 0);
	remove_action('wp_head', 'parent_post_rel_link', 10, 0);
	remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);

	remove_filter( 'single_job_listing_start', 'job_listing_company_display', 30 );

	add_filter('acf/settings/google_api_key', function () {
	    return 'AIzaSyBR5bX6m_CEPwitun65XjrFWYZVRtzqADA';
	});

?>
