<?php 
    $args = array(
        'post_type'             => 'testimonials',
        'posts_per_page'        => 5,
        'orderby'               => 'rand'
    );  
    $the_query = new WP_Query( $args );
?>
<?php if(is_front_page()) { ?>
    <div class="slider-container">
        <?php if($the_query->have_posts() ) { ?>
            <ul class="testimonial-slider">
            	<?php while($the_query->have_posts()) { ?>
                    <?php $the_query->the_post(); ?>
                        <li>
                            <?php the_content(); ?>
                            <h2><?php the_title(); ?></h2>
                        </li>	
                	<?php wp_reset_postdata(); ?>
            	<?php } ?>
            </ul>
        <?php } ?>
    </div>
<?php } ?>
