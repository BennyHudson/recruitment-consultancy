<?php
/**
 * Filters in `[jobs]` shortcode.
 *
 * This template can be overridden by copying it to yourtheme/job_manager/job-filters.php.
 *
 * @see         https://wpjobmanager.com/document/template-overrides/
 * @author      Automattic
 * @package     WP Job Manager
 * @category    Template
 * @version     1.21.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

wp_enqueue_script( 'wp-job-manager-ajax-filters' );

do_action( 'job_manager_job_filters_before', $atts );
?>

<section class="page-feature">
	<div class="page-feature-blur" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/images/homepage-feature.jpg');"></div>
	<div class="page-feature-grad">
		<section class="container ultra extra-bottom">
			<form class="job_filters">
				<?php do_action( 'job_manager_job_filters_start', $atts ); ?>

				<h2 class="search-form-title white">Find your next job in West Sussex or Surrey</h2>

				<div class="form-split three-split">
					<?php do_action( 'job_manager_job_filters_search_jobs_start', $atts ); ?>

					<aside>
						<input type="text" name="search_keywords" id="search_keywords" placeholder="<?php esc_attr_e( 'Keywords', 'wp-job-manager' ); ?>" value="<?php echo esc_attr( $keywords ); ?>" />
					</aside>

					<aside>
						<div class="search_categories">
							<select name="filter_by_salary" class="job-manager-filter">
								<option value=""><?php _e( 'Any Salary', 'wp-job-manager' ); ?></option>
								<option value="upto20"><?php _e( 'Up to £20,000', 'wp-job-manager' ); ?></option>
								<option value="20000-25000"><?php _e( '£20,000 to £25,000', 'wp-job-manager' ); ?></option>
								<option value="25000-30000"><?php _e( '£25,000 to £30,000', 'wp-job-manager' ); ?></option>
								<option value="30000-35000"><?php _e( '£30,000 to £35,000', 'wp-job-manager' ); ?></option>
								<option value="35000-40000"><?php _e( '£35,000 to £40,000', 'wp-job-manager' ); ?></option>
								<option value="40000-45000"><?php _e( '£40,000 to £45,000', 'wp-job-manager' ); ?></option>
								<option value="45000-50000"><?php _e( '£45,000 to £50,000', 'wp-job-manager' ); ?></option>
								<option value="50000-55000"><?php _e( '£50,000 to £55,000', 'wp-job-manager' ); ?></option>
								<option value="55000-60000"><?php _e( '£55,000 to £60,000', 'wp-job-manager' ); ?></option>
								<option value="over60"><?php _e( '£60,000+', 'wp-job-manager' ); ?></option>
							</select>
						</div>
					</aside>

					<aside class="search-location-container">
						<input type="text" name="search_location" id="search_location" placeholder="<?php esc_attr_e( 'Location', 'wp-job-manager' ); ?>" value="<?php echo esc_attr( $location ); ?>" />
					</aside>

					<?php if ( $categories ) : ?>
						<?php foreach ( $categories as $category ) : ?>
							<input type="hidden" name="search_categories[]" value="<?php echo sanitize_title( $category ); ?>" />
						<?php endforeach; ?>
					<?php elseif ( $show_categories && ! is_tax( 'job_listing_category' ) && get_terms( 'job_listing_category' ) ) : ?>
						<aside>
							<?php if ( $show_category_multiselect ) : ?>
								<?php job_manager_dropdown_categories( array( 'taxonomy' => 'job_listing_category', 'hierarchical' => 1, 'name' => 'search_categories', 'orderby' => 'name', 'selected' => $selected_category, 'hide_empty' => false ) ); ?>
							<?php else : ?>
								<?php job_manager_dropdown_categories( array( 'taxonomy' => 'job_listing_category', 'hierarchical' => 1, 'show_option_all' => __( 'Any category', 'wp-job-manager' ), 'name' => 'search_categories', 'orderby' => 'name', 'selected' => $selected_category, 'multiple' => false ) ); ?>
							<?php endif; ?>
						</aside>
					<?php endif; ?>

					<?php do_action( 'job_manager_job_filters_search_jobs_end', $atts ); ?>
				</div>

				<?php do_action( 'job_manager_job_filters_end', $atts ); ?>
			</form>
		</section>
	</div>
</section>

<?php do_action( 'job_manager_job_filters_after', $atts ); ?>

<noscript><?php _e( 'Your browser does not support JavaScript, or it is disabled. JavaScript must be enabled in order to view listings.', 'wp-job-manager' ); ?></noscript>
