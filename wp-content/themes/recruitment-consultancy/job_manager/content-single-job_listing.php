<?php
/**
 * Single job listing.
 *
 * This template can be overridden by copying it to yourtheme/job_manager/content-single-job_listing.php.
 *
 * @see         https://wpjobmanager.com/document/template-overrides/
 * @author      Automattic
 * @package     WP Job Manager
 * @category    Template
 * @since       1.0.0
 * @version     1.28.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post;

	$job_contact_name = get_post_meta($post->ID, '_contact_name');
	$job_contact_email = get_post_meta($post->ID, '_contact_email');
	$job_contact_telephone = get_post_meta($post->ID, '_contact_telephone');
	$job_contact_url = get_post_meta($post->ID, '_contact_url');

	$job_duration = get_post_meta($post->ID, '_job_duration');
	$job_skills = get_post_meta($post->ID, '_job_skills');
	$job_requirements = get_post_meta($post->ID, '_job_requirements');
	$job_min_experience = get_post_meta($post->ID, '_min_experience');
	$job_education_level = get_post_meta($post->ID, '_education_level');

?>
<section class="page-feature">
	<div class="page-feature-blur" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/images/homepage-feature.jpg');"></div>
		<div class="page-feature-grad">
			<section class="container ultra extra-bottom">
				<h1 class="page-title alt"><?php the_title(); ?></h1>
				<?php
					/**
					 * single_job_listing_start hook
					 *
					 * @hooked job_listing_meta_display - 20
					 * @hooked job_listing_company_display - 30
					 */
					do_action( 'single_job_listing_start' );
				?>
			</section>
		</div>
	</div>
</section>
<section class="background-grad">
	<section class="container ultra no-top">
		<article class="cover">
			<div class="single_job_listing">
				<?php if ( get_option( 'job_manager_hide_expired_content', 1 ) && 'expired' === $post->post_status ) : ?>
					<div class="job-manager-info"><?php _e( 'This listing has expired.', 'wp-job-manager' ); ?></div>
				<?php else : ?>
					<aside class="page-main left">
						<div class="job_description">
							<?php wpjm_the_job_description(); ?>
						</div>
						<?php if ( candidates_can_apply() ) : ?>
							<?php get_job_manager_template( 'job-application.php' ); ?>
						<?php endif; ?>
					</aside>
					<aside class="page-sidebar">
						<?php if($job_skills || $job_requirements || $job_min_experience || $job_education_level || $job_contact_name || $job_contact_email || $job_contact_url || $job_contact_telephone) { ?>
							<div class="widget">
								<?php if($job_skills || $job_requirements || $job_min_experience || $job_education_level) { ?>
									<?php if($job_skills && $job_requirements) { ?>
										<h3>Job Skills &amp; Requirements</h3>
									<?php } elseif($job_skills) { ?>
										<h3>Required Skills</h3>
									<?php } else { ?>
										<h3>Job Requirements</h3>
									<?php } ?>
									<dl>
										<?php if($job_skills) { ?>
											<dt>Skills:</dt>
											<dd><?php echo $job_skills[0]; ?></dd>
										<?php } ?>
										<?php if($job_requirements) { ?>
											<dt>Minimum requirements:</dt>
											<dd><?php echo $job_requirements[0]; ?></dd>
										<?php } ?>
										<?php if($job_min_experience) { ?>
											<dt>Minimum experience:</dt>
											<dd><?php echo $job_min_experience[0]; ?></dd>
										<?php } ?>
										<?php if($job_education_level) { ?>
											<dt>Minimum education:</dt>
											<dd><?php echo $job_education_level[0]; ?></dd>
										<?php } ?>
									</dl>
								<?php } ?>
								<?php if($job_contact_name || $job_contact_email || $job_contact_url || $job_contact_telephone) { ?>
									<h3>Contact Details</h3>
									<dl>
										<?php if($job_contact_name) { ?>
											<dt>Name:</dt>
											<dd><?php echo $job_contact_name[0]; ?></dd>
										<?php } ?>
										<?php if($job_contact_email) { ?>
											<dt>Email:</dt>
											<dd><?php echo $job_contact_email[0]; ?></dd>
										<?php } ?>
										<?php if($job_contact_url) { ?>
											<dt>Website:</dt>
											<dd><?php echo $job_contact_url[0]; ?></dd>
										<?php } ?>
										<?php if($job_contact_telephone) { ?>
											<dt>Telephone:</dt>
											<dd><?php echo $job_contact_telephone[0]; ?></dd>
										<?php } ?>
									</dl>
								<?php } ?>
							</div>
						<?php } ?>
					</aside>

					<?php
						/**
						 * single_job_listing_end hook
						 */
						do_action( 'single_job_listing_end' );
					?>
				<?php endif; ?>
			</div>
		</article>
	</section>
</section>
