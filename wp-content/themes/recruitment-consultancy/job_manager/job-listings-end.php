<?php
/**
 * Content shown after job listings in `[jobs]` shortcode.
 *
 * This template can be overridden by copying it to yourtheme/job_manager/job-listings-end.php.
 *
 * @see         https://wpjobmanager.com/document/template-overrides/
 * @author      Automattic
 * @package     WP Job Manager
 * @category    Template
 * @version     1.15.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>
			</ul>
		</article>
	</section>
</section>
<section class="cv-upload-section">
	<div class="page-feature-blur"></div>
	<div class="page-feature-grad">
		<section class="container ultra">
			<h2 class="section-title alt white">Don't see any vacancies for you?</h2>
			<h3 class="white">Register with us below to hear about the latest office jobs in your area first! Don't miss out!</h3>
			<aside class="cv-upload-form">
				<?php echo do_shortcode('[submit_resume_form]'); ?>
			</aside>
			<aside class="cv-upload-content">
				<?php the_field('why_register', 40); ?>
			</aside>
		</section>
	</div>
</section>
