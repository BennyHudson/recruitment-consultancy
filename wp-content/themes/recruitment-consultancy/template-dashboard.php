<?php
	//Template Name: Dashboard
	get_header();
?>

	<?php if (have_posts()) { ?>
		<?php while ( have_posts() ) { ?>
			<?php the_post(); ?>

			<section class="page-feature">
				<div class="page-feature-blur" style="background-image: url('https://source.unsplash.com/collection/524681/2000x600');"></div>
					<div class="page-feature-grad">
						<section class="container ultra extra-bottom">
							<h1 class="page-title alt"><?php the_title(); ?></h1>					
						</section>
					</div>
				</div>
			</section>
			<section class="background-grad">
				<section class="container ultra no-top">
					<article class="cover">
						<?php the_content(); ?>
					</article>
				</section>
			</section>

		<?php } ?>
	<?php } ?>

<?php get_footer(); ?>
