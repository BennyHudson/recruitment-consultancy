<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
	<link href="https://fonts.googleapis.com/css?family=Bree+Serif|Lato:300,300i,700,700i" rel="stylesheet">
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width"/>  
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<?php get_template_part('includes/include', 'favicon'); ?>
	<title><?php bloginfo('name'); ?></title>
	<?php wp_head(); ?>
	<script type="text/javascript">
	    window.cookieconsent_options = {"message":"This website uses cookies to ensure you get the best experience on our website","dismiss":"Got it!","learnMore":"More info","link":null,"theme":"dark-bottom"};
	</script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/1.0.9/cookieconsent.min.js"></script>
</head>
<body <?php body_class('wedo'); ?>>

	<section class="page-content">
		<section class="footer-fix">
			
			<header class="headroom">
				<section class="container">
					<?php get_template_part('snippets/content', 'logo'); ?>
					<nav>
						<?php wp_nav_menu( array( 'theme_location' => 'header-menu', 'container' => '' ) ); ?>
					</nav>
				</section>
			</header>
		
