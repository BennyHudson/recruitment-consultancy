var $ = jQuery.noConflict();
$(document).ready(function() {

	jQuery('.container').fitVids();

    if($('.job-manager-error').length) {
        $('html, body').animate({
            scrollTop: $('.cv-upload-section').offset().top
        }, 1000);
    }

	var footerHeight = $('footer').outerHeight();
    var headerHeight = $('header').outerHeight();
    $('.footer-fix').css({
        'padding-bottom' : footerHeight,
        'padding-top' : headerHeight
    });

    $('header').headroom({
        offset : headerHeight
    });

    $('body.page-id-11 option[value="Guildford"], body.page-id-30 option[value="Guildford"], body.page-id-31 option[value="Guildford"], body.page-id-32 option[value="Guildford"], body.page-id-127 option[value="Guildford"], body.page-id-291 option[value="Guildford"], body.page-id-11 option[value="guildford"], body.page-id-30 option[value="guildford"], body.page-id-31 option[value="guildford"], body.page-id-32 option[value="guildford"], body.page-id-127 option[value="guildford"], body.page-id-291 option[value="guildford"]').attr('selected','selected');

    $('ul:not(.sub-menu) > li.menu-item-has-children').hoverIntent(function() {
        $(this).find('> ul').slideToggle();
    });

    $('.testimonial-slider').bxSlider({
        minSlides: 3,
        maxSlides: 3,
        slideWidth: 400,
        slideMargin: 80,
        moveSlides: 1,
        pager: false,
        prevText: '<i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i>',
        nextText: '<i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>'
    });

    $('.simple-testimonial-slider').bxSlider({
        pager: false,
        prevText: '<i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i>',
        nextText: '<i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>',
        adaptiveHeight: true
    });

    $('.page-scroller').click(function(e) {
        e.preventDefault();
        var target = $(this).data('target');
        $('html, body').animate({
            scrollTop: $('#' + target).offset().top
        }, 1000);
    });

    

    $('.tab-content').each( function(i){
        var postnum = i; // remove any possible overloaded "+" operator ambiguity
        $(this).attr('id', 'tab-content-' + postnum);
    });

    $('.tabs').each( function(i){
        var postnum = i; // remove any possible overloaded "+" operator ambiguity
        $(this).attr('id', 'tabs-' + postnum);
    });

    var sliderSets = $('.slider-wrap');
    function initSliders(targetSlider, targetPager) {
        jQuery(targetSlider).bxSlider({
            pagerCustom: targetPager,
            controls: false,
            mode: 'fade',
            adaptiveHeight: true
        });
    }
    $(sliderSets).each(function() {
        var targetSlider = "#" + $(this).children().children('.tab-content').attr('id');
        var targetPager = "#" + $(this).children().children('.tabs').attr('id');
        initSliders(targetSlider, targetPager);
    });

    $('.faq-trigger').click(function(e) {
        e.preventDefault();
        if($(this).hasClass('active')) {
            $('.faq-trigger.active').removeClass('active');
            $('.faq-content.open').slideUp().removeClass('open');
        } else {
            $('.faq-trigger.active').removeClass('active');
            $('.faq-content.open').slideUp().removeClass('open');
            $(this).addClass('active');
            $(this).next('.faq-content').slideDown().addClass('open');
        }
    });

    $('.search-location-container').prependTo('.gjm-filters-wrapper');
    $('#gjm-units').appendTo('.gjm-filters-wrapper');
    $('.search-choice').appendTo('.chosen-choices');

});

enquire
    .register("screen and (min-width:31em)", function() { 
        $(document).ready(function() {
            
        });
    }, true)
    .register("screen and (max-width:31em)", function() { 
        $(document).ready(function() {
            
        });
    });

var ieVersion = null;
if (document.body.style['msFlexOrder'] != undefined) {
    ieVersion = "ie10";
    $('html').addClass('ie-10');
}
if (document.body.style['msTextCombineHorizontal'] != undefined) {
    ieVersion = "ie11";
    $('html').removeClass('ie-10').addClass('ie-11');
}
