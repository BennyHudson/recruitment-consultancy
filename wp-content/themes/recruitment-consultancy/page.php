<?php 
	get_header(); 
	$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'page-flood' );
?>

	<?php if (have_posts()) { ?>
		<?php while ( have_posts() ) { ?>
			<?php the_post(); ?>	

			<section class="page-feature">
				<?php if($image) { ?>
					<div class="page-feature-blur" style="background-image: url('<?php echo $image[0]; ?>');"></div>
				<?php } else { ?>
					<div class="page-feature-blur" style="background-image: url('https://source.unsplash.com/collection/524681/2000x600');"></div>
				<?php } ?>
				<div class="page-feature-grad">
					<section class="container ultra">
						<h1 class="page-title white"><?php the_title(); ?></h1>
						<?php the_content(); ?>
						<?php if(is_page(9)) { ?>
							<h4>Learn more:</h4>
							<a href="<?php echo get_the_permalink(18); ?>" class="button outline white">Employer Services</a>
							<a href="<?php echo get_the_permalink(12); ?>" class="button outline white">Candidate Services</a>
							<a href="<?php echo get_the_permalink(16); ?>" class="button outline white">About Us</a>
						<?php } else { ?>
							<?php if(get_field('page_sections')) { ?>
								<?php $count = 0; ?>
								<?php while(the_repeater_field('page_sections')) { ?>
									<?php $count++; ?>
									<a href="#" class="button outline white page-scroller" data-target="section-<?php echo $count; ?>"><?php the_sub_field('call_to_action'); ?></a>
								<?php } ?>
							<?php } ?>
						<?php } ?>
					</section>
				</div>
			</section>

			<section class="page-body">
				<?php if(get_field('page_sections')) { ?>
					<?php $count = 0; ?>
					<?php while(the_repeater_field('page_sections')) { ?>
						<?php $count++; ?>
						<section class="page-section" id="section-<?php echo $count; ?>">
							<?php if(have_rows('page_subsections')) { ?>
								<?php while(have_rows('page_subsections')) { ?>
									<?php the_row(); ?>
									<?php if(get_row_layout() !== 'secondary_feature_block') { ?>
										<section class="page-subsection">
											<section class="container ultra">
									<?php } ?>
											<?php if(get_row_layout() !== 'job_cta_block') { ?>
												<i class="fa <?php the_sub_field('section_icon'); ?> background-icon large" aria-hidden="true"></i>
											<?php } ?>
											<section class="foreground-content">

												<?php if(get_row_layout() == 'simple_content_block') { ?>
													<?php if(get_sub_field('section_title')) { ?><h2 class="section-title"><?php the_sub_field('section_title'); ?></h2><?php } ?>
													<?php the_sub_field('section_content'); ?>
												<?php } ?>

												<?php if(get_row_layout() == 'split_content_block') { ?>

													<?php if(get_sub_field('split_direction') == 'ltor') { ?>
														<aside class="page-main left">
															<?php if(get_sub_field('section_title')) { ?><h2 class="section-title"><?php the_sub_field('section_title'); ?></h2><?php } ?>
															<?php the_sub_field('section_primary_content'); ?>
														</aside>
														<aside class="page-sidebar">
															<div class="widget">
																<?php the_sub_field('section_secondary_content'); ?>
															</div>
														</aside>
													<?php } else { ?>
														<aside class="page-sidebar left">
															<div class="widget">
																<?php the_sub_field('section_secondary_content'); ?>
															</div>
														</aside>
														<aside class="page-main">
															<?php if(get_sub_field('section_title')) { ?><h2 class="section-title"><?php the_sub_field('section_title'); ?></h2><?php } ?>
															<?php the_sub_field('section_primary_content'); ?>
														</aside>
													<?php } ?>

												<?php } ?>

												<?php if(get_row_layout() == 'testimonial_block') { ?>
													<?php if(get_sub_field('section_title')) { ?><h2 class="section-title"><?php the_sub_field('section_title'); ?></h2><?php } ?>

													<?php 
														$source = get_sub_field('source');
														if(is_page(10) || is_page(21) || is_page(22) || is_page(23) || is_page(125) || is_page(292)) {
															$branch = 'worthing';
														} elseif(is_page(11) || is_page(30) || is_page(31) || is_page(32) || is_page(127) || is_page(291)) {
															$branch = 'guildford';
														}
													    if($branch) {
														    $args = array(
														        'post_type'             => 'testimonials',
														        'posts_per_page'        => 5,
														        'orderby'               => 'rand',
														        'tax_query' => array(
															        array(
															            'taxonomy' => 'source',
															            'field' => 'slug',
															        	'terms' => $source
															    	),
															    	array(
															            'taxonomy' => 'location',
															            'field' => 'slug',
															        	'terms' => $branch
															    	)
															    )
														    );  
														} else {
															$args = array(
														        'post_type'             => 'testimonials',
														        'posts_per_page'        => 5,
														        'orderby'               => 'rand',
														        'tax_query' => array(
															        array(
															            'taxonomy' => 'source',
															            'field' => 'slug',
															        	'terms' => $source
															    	)
															    )
														    );
														}
													    $the_query = new WP_Query( $args );
													?>
												    <div class="slider-container">
												        <?php if($the_query->have_posts() ) { ?>
												            <ul class="testimonial-slider">
												            	<?php while($the_query->have_posts()) { ?>
												                    <?php $the_query->the_post(); ?>
												                        <li>
												                            <?php the_excerpt(); ?>
												                            <h2><?php the_title(); ?></h2>
												                        </li>	
												                	<?php wp_reset_postdata(); ?>
												            	<?php } ?>
												            </ul>
												        <?php } ?>
												    </div>


												<?php } ?>

												<?php if(get_row_layout() == 'job_search_block') { ?>

													<aside class="page-main left">
														<?php if(get_sub_field('section_title')) { ?><h2 class="section-title"><?php the_sub_field('section_title'); ?></h2><?php } ?>
														<?php the_sub_field('section_content'); ?>
													</aside>
													<aside class="page-sidebar">
														<div class="widget">
															<h2><i class="fa fa-search"></i> <?php the_sub_field('job_search_title'); ?></h2>
															<?php get_template_part('includes/partial', 'search'); ?>
														</div>
													</aside>
													
												<?php } ?>

												<?php if(get_row_layout() == 'tabs_block') { ?>
													<?php if(get_sub_field('tabs')) { ?>
														<?php $count = -1; ?>
														<div class="slider-wrap">
															<div>
																<ul class="horizontal-tab-triggers tabs">
																	<?php while(the_repeater_field('tabs')) { ?>
																		<?php $count++; ?>
																		<li><a href="#" data-slide-index="<?php echo $count; ?>"><?php the_sub_field('tab_title'); ?></a></li>
																	<?php } ?>
																</ul>
															</div>
															<div>
																<ul class="horizontal-tab-content tab-content">
																	<?php while(the_repeater_field('tabs')) { ?>
																		<li>
																			<div class="tab-content">
																				<?php the_sub_field('tab_content'); ?>
																			</div>
																		</li>
																	<?php } ?>
																</ul>
															</div>
														</div>

													<?php } ?>
												<?php } ?>

												<?php if(get_row_layout() == 'faq_block') { ?>

													<?php if(get_sub_field('faq')) { ?>

														<?php $count = 0; ?>

														<div class="faq-unit">
															<?php while(the_repeater_field('faq')) { ?>
																<?php $count++; ?>
																<a href="#" class="faq-trigger <?php if($count == 1) { echo 'active'; } ?>"><?php the_sub_field('question'); ?></a>
																<div class="faq-content <?php if($count == 1) { echo 'open'; } ?>">
																	<?php the_sub_field('answer'); ?>
																</div>
															<?php } ?>
														</div>

													<?php } ?>

												<?php } ?>

												<?php if(get_row_layout() == 'meet_the_team_block') { ?>

													<?php if(get_sub_field('section_title')) { ?><h2 class="section-title"><?php the_sub_field('section_title'); ?></h2><?php } ?>
													<?php if(get_sub_field('meet_the_team')) { ?>
								
														<ul class="team-list">
															<?php while(the_repeater_field('meet_the_team')) { ?>
																<?php
																	$image_id = get_sub_field('photo');

																	$thumbnail = wp_get_attachment_image_src($image_id, 'gallery-thumb');
																	$thumbnail_url = $thumbnail[0];

																?>
																<li class="team-member">
																	<aside class="team-meta">
																		<img src="<?php echo $thumbnail_url; ?>" alt="<?php the_sub_field('name'); ?>">
																	</aside>
																	<aside class="team-content">
																		<h3 class="green"><?php the_sub_field('name'); ?> <span><?php the_sub_field('job_title'); ?></span></h3>
																		<?php the_sub_field('bio'); ?>
																	</aside>
																</li>
															<?php } ?>
														</ul>
													<?php } ?>

												<?php } ?>

												<?php if(get_row_layout() == 'half_split_block') { ?>

													<section class="split-modules">

														<?php if(get_sub_field('module_content')) { ?>
															<?php while(the_repeater_field('module_content')) { ?>

																<aside class="module half-width">
																	<i class="fa <?php the_sub_field('module_icon'); ?> background-icon small" aria-hidden="true"></i>
																	<div class="foreground-content">
																		<h2 class="module-title"><?php the_sub_field('module_title'); ?></h2>
																		<?php if(get_sub_field('module_content') == 'testimonials') { ?>
																			<?php 
																				$source = get_sub_field('source');
																				if(is_page(10) || is_page(21) || is_page(22) || is_page(23) || is_page(125) || is_page(292)) {
																					$branch = 'worthing';
																				} elseif(is_page(11) || is_page(30) || is_page(31) || is_page(32) || is_page(127) || is_page(291)) {
																					$branch = 'guildford';
																				}
																				if($branch) {
																				    $args = array(
																				        'post_type'             => 'testimonials',
																				        'posts_per_page'        => 5,
																				        'orderby'               => 'rand',
																				        'tax_query' => array(
																					        array(
																					            'taxonomy' => 'source',
																					            'field' => 'slug',
																					        	'terms' => $source
																					    	),
																					    	array(
																					            'taxonomy' => 'location',
																					            'field' => 'slug',
																					        	'terms' => $branch
																					    	)
																					    )
																				    );  
																				} else {
																					$args = array(
																				        'post_type'             => 'testimonials',
																				        'posts_per_page'        => 5,
																				        'orderby'               => 'rand',
																				        'tax_query' => array(
																					        array(
																					            'taxonomy' => 'source',
																					            'field' => 'slug',
																					        	'terms' => $source
																					    	)
																					    )
																				    );
																				}
																			    $the_query = new WP_Query( $args );
																			?>
																		    <div class="slider-container">
																		        <?php if($the_query->have_posts() ) { ?>
																		            <ul class="simple-testimonial-slider">
																		            	<?php while($the_query->have_posts()) { ?>
																		                    <?php $the_query->the_post(); ?>
																		                        <li>
																		                            <?php the_content(); ?>
																		                            <h2><?php the_title(); ?></h2>
																		                        </li>	
																		                	<?php wp_reset_postdata(); ?>
																		            	<?php } ?>
																		            </ul>
																		        <?php } ?>
																		    </div>
																		<?php } elseif(get_sub_field('module_content') == 'map') { ?>
																			<?php 
																				$location = get_sub_field('map');
																				if( !empty($location) ) {
																			?>
																				<div class="acf-map large">
																					<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
																				</div>
																			<?php } ?>
																		<?php } elseif(get_sub_field('module_content') == 'free_content') { ?>
																			<?php the_sub_field('free_content'); ?>
																		<?php } ?>
																	</div>
																</aside>

															<?php } ?>

														<?php } ?>

													</section>

												<?php } ?>

												<?php if(get_row_layout() == 'job_cta_block') { ?>

													<h2 class="section-title">Help me to find my next job...</h2>
													<p>We are office work specialists and have been placing candidates in businesses throughout Surrey and West Sussex for over 20 years!</p>
													<p>Let us help you to find your next job...</p>

													<section class="split-modules">
														<aside class="module one-third">
															<i class="fa fa-search background-icon small" aria-hidden="true"></i>
															<div class="foreground-content">
																<h2 class="module-title">Permanent Office Jobs</h2>
																<a href="<?php echo get_the_permalink(22); ?>" class="button block worthing small angle">Permanent Jobs in West Sussex</a>
																<a href="<?php echo get_the_permalink(31); ?>" class="button block guildford small angle">Permanent Jobs in Surrey</a>
															</div>
														</aside>
														<aside class="module one-third">
															<i class="fa fa-search background-icon small" aria-hidden="true"></i>
															<div class="foreground-content">
																<h2 class="module-title">Temporary Office Jobs</h2>
																<a href="<?php echo get_the_permalink(21); ?>" class="button block worthing small angle">Temporary Jobs in West Sussex</a>
																<a href="<?php echo get_the_permalink(30); ?>" class="button block guildford small angle">Temporary Jobs in Surrey</a>
															</div>
														</aside>
														<aside class="module one-third">
															<i class="fa fa-search background-icon small" aria-hidden="true"></i>
															<div class="foreground-content">
																<h2 class="module-title">Contract Office Jobs</h2>
																<a href="<?php echo get_the_permalink(23); ?>" class="button block worthing small angle">Contract Jobs in West Sussex</a>
																<a href="<?php echo get_the_permalink(32); ?>" class="button block guildford small angle">Contract Jobs in Surrey</a>
															</div>
														</aside>
													</section>

												<?php } ?>

												<?php if(get_row_layout() == 'secondary_feature_block') { ?>
													<section class="page-subsection page-feature">
														<?php
															$image_id = get_sub_field('image');

															$image = wp_get_attachment_image_src($image_id, 'page-flood');
															$image_url = $image[0];

														?>
														<div class="page-feature-blur" style="background-image: url('<?php echo $image_url; ?>');"></div>
														<img src="<?php echo $image_url; ?>">
													</section>
												<?php } ?>
											<?php if(get_row_layout() !== 'secondary_feature_block') { ?>
													</section>
												</section>
											<?php } ?>

									</section>     
								<?php } ?>
							<?php } ?>

							<?php if(is_page(9)) { ?>
								<section class="page-subsection">
									<section class="container ultra">

										<section class="split-modules">
											<aside class="module half-width">
												<h2 class="module-title green">I need to fill an office-based vacancy</h2>
												<p>We can help.</p>
												<p>For over 20 years we have built strong relationships with local businesses ranging in size from 5-500 staff.</p>
												<p>We have a large and active base of office-based candidates we know personally who are available for immediate start.</p>
												<p>Trust us to provide your next office team member.</p>
												<p>Contact your local office today with what you need and we will do our best to help.</p>
												<a href="<?php echo get_the_permalink(10); ?>" class="button worthing">Worthing Office</a>
												<a href="<?php echo get_the_permalink(11); ?>" class="button guildford">Guildford Office</a>
											</aside>
											<aside class="module half-width">
												<h2 class="module-title green">I want to find my next office-based job</h2>
												<p>Fantastic - We have temporary, permanent, part-time and contract office based opportunities available right now.</p>
												<a href="<?php echo get_the_permalink(5); ?>" class="button">Search our jobs</a>
												<a href="<?php echo get_the_permalink(40); ?>" class="button">Submit your CV</a>
											</aside>
										</section>
									</section>
								</section>

							<?php } ?>

						</section>
					<?php } ?>
				<?php } ?>			
			</section>

			<?php if(get_field('cv_upload') == 'yes') { ?>
				<section class="cv-upload-section">
					<div class="page-feature-blur"></div>
					<div class="page-feature-grad">
						<section class="container ultra">
							<?php if(!is_page(40)) { ?>
								<h2 class="section-title alt white"><?php the_field('cv_upload_title'); ?></h2>
								<h3 class="white"><?php the_field('cv_upload_subtitle'); ?></h3>
							<?php } ?>
							<aside class="cv-upload-form">
								<?php echo do_shortcode('[submit_resume_form]'); ?>
							</aside>
							<aside class="cv-upload-content">
								<?php the_field('why_register', 40); ?>
							</aside>
						</section>
					</div>
				</section>
			<?php } ?>
		<?php } ?>
	<?php } ?>

<?php get_footer(); ?>
