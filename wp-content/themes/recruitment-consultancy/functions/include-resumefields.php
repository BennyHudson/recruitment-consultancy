<?php

	function remove_submit_resume_form_fields($fields) {

		unset($fields['resume_fields']['candidate_title']);
		unset($fields['resume_fields']['candidate_photo']);
		unset($fields['resume_fields']['candidate_video']);
		unset($fields['resume_fields']['candidate_location']);
		//unset($fields['resume_fields']['resume_content']);
		unset($fields['resume_fields']['links']);
		unset($fields['resume_fields']['candidate_education']);
		unset($fields['resume_fields']['candidate_experience']);

		return $fields;
	}

	function wpjms_admin_resume_form_fields( $fields ) {
	
		// $fields['_candidate_color'] = array(
		//     'label' 		=> __( 'Favourite Color', 'job_manager' ),
		//     'type' 			=> 'text',
		//     'placeholder' 	=> __( 'Blue', 'job_manager' ),
		//     'description'	=> '',
		//     'priority' => 1
		// );

		return $fields;
		
	}

	// This is your function which takes the fields, modifies them, and returns them
	function resume_file_required( $fields ) {

	    // Here we target one of the job fields (candidate name) and change it's label
	    $fields['resume_fields']['resume_file']['required'] = true;
	    $fields['resume_fields']['resume_content']['required'] = false;

	    $fields['resume_fields']['resume_file']['label'] = "Upload your CV";
	    $fields['resume_fields']['resume_content']['label'] = "Cover Letter";

	    // And return the modified fields
	    return $fields;
	}

	function wpjms_frontend_resume_form_fields( $fields ) {
	
		$fields['resume_fields']['candidate_telephone'] = array(
		    'label' => __( 'Telephone Number', 'job_manager' ),
		    'type' => 'text',
		    'required' => false,
		    'placeholder' => '',
		    'priority' => 2
		);
		$fields['resume_fields']['candidate_availability'] = array(
		    'label' => __( 'Availability', 'job_manager' ),
		    'type' => 'select',
		    'options' => array(
		    	''			=> '',
		    	'immediate' => 'Immediate',
		    	'1month'	=> 'One month',
		    	'3months'	=> 'Three months',
		    	'more'		=> 'More than three months'
		    ),
		    'required' => false,
		    'placeholder' => '',
		    'priority' => 2
		);
		$fields['resume_fields']['perm_or_temp'] = array(
		    'label' => __( 'Permanent or Temporary Role?', 'job_manager' ),
		    'type' => 'select',
		    'options' => array(
		    	''			=> '',
		    	// 'full-time'	=> 'Full time',
		    	// 'part-time'	=> 'Part time',
		    	'temp'		=> 'Temp work',
		    	'contract'	=> 'Contract'
		    ),
		    'required' => false,
		    'placeholder' => '',
		    'priority' => 3
		);
		$fields['resume_fields']['full_or_part'] = array(
		    'label' => __( 'Full-time or Part-time?', 'job_manager' ),
		    'type' => 'select',
		    'options' => array(
		    	''			=> '',
		    	'full-time'	=> 'Full time',
		    	'part-time'	=> 'Part time'
		    ),
		    'required' => false,
		    'placeholder' => '',
		    'priority' => 4
		);
		$fields['resume_fields']['local_office'] = array(
		    'label' => __( 'Send your details to which office?', 'job_manager' ),
		    'type' => 'select',
		    'options' => array(
		    	'worthing'	=> 'Worthing',
		    	'guildford'	=> 'Guildford'
		    ),
		    'required' => false,
		    'placeholder' => '',
		    'priority' => 100
		);

		return $fields;
		
	}

	function wpjms_color_field_email_message( $message, $resume_id ) {
	  // $message[] = "\n" . "Favourite Color: " . get_post_meta( $resume_id, '_candidate_color', true );  
	  return $message;
	}

?>
