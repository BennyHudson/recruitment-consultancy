<?php
	function wedo_posts() {
		
		register_post_type(
			'testimonials', 
			array(
				'labels'		=> array (
									'name'					=> _x('Testimonials', 'post type general name'),
									'singular_name'			=> _x('Testimonial', 'post type singular name'),
									'add_new'				=> _x('Add new testimonial', 'book'),
									'add_new_item'			=> ('Add New Testimonial'),
									'edit_item'				=> ('Edit Testimonial'),
									'new_item'				=> ('New Testimonial'),
									'all_items'				=> ('All Testimonials'),
									'view_item'				=> ('View Testimonials'),
									'search_items'			=> ('Search Testimonials'),
									'not_found'				=> ('No Testimonials found'),
									'not_found_in_trash'	=> ('No Testimonials found in trash'),
									'parent_item_colon'		=> '',
									'menu_name'				=> 'Testimonials'
								),
				'description'	=> 'Holds all the Testimonials',
				'public'		=> true,
				'menu_position'	=> 19,
				'menu_icon'		=> 'dashicons-format-status',
				'supports'		=> array('title', 'editor', 'excerpt', 'revisions', 'thumbnail'),
				'has_archive'	=> true
			)
		);
	
	}
	function wedo_taxonomies() {
		
		register_taxonomy( 
			'source', 
			'testimonials', 
			array(
	            'labels' 		=> array(
						 			'name'              => _x( 'Testimonial Source', 'taxonomy general name' ),
						            'singular_name'     => _x( 'Testimonial Source', 'taxonomy singular name' ),
						            'search_items'      => __( 'Search Testimonial Sources' ),
						            'all_items'         => __( 'All Testimonial Sources' ),
						            'parent_item'       => __( 'Parent Testimonial Source' ),
						            'parent_item_colon' => __( 'Parent Testimonial Source:' ),
						            'edit_item'         => __( 'Edit Testimonial Source' ), 
						            'update_item'       => __( 'Update Testimonial Source' ),
						            'add_new_item'      => __( 'Add New Testimonial Source' ),
						            'new_item_name'     => __( 'New Testimonial Source' ),
						            'menu_name'         => __( 'Testimonial Sources' ),
						        ),
	            'hierarchical' => true,
	        )
		);
		register_taxonomy( 
			'location', 
			'testimonials', 
			array(
	            'labels' 		=> array(
						 			'name'              => _x( 'Testimonial Location', 'taxonomy general name' ),
						            'singular_name'     => _x( 'Testimonial Location', 'taxonomy singular name' ),
						            'search_items'      => __( 'Search Testimonial Locations' ),
						            'all_items'         => __( 'All Testimonial Locations' ),
						            'parent_item'       => __( 'Parent Testimonial Location' ),
						            'parent_item_colon' => __( 'Parent Testimonial Location:' ),
						            'edit_item'         => __( 'Edit Testimonial Location' ), 
						            'update_item'       => __( 'Update Testimonial Location' ),
						            'add_new_item'      => __( 'Add New Testimonial Location' ),
						            'new_item_name'     => __( 'New Testimonial Location' ),
						            'menu_name'         => __( 'Testimonial Locations' ),
						        ),
	            'hierarchical' => true,
	        )
		);
	
    }
?>
