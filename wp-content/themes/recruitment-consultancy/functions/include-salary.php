<?php

	function display_job_salary_data() {
		global $post;

		// Populate salary fields

		$salary_currency = 	get_post_meta($post->ID, '_salary_currency');
		$salary_from = 		get_post_meta($post->ID, '_salary_from');
		$salary_to =		get_post_meta($post->ID, '_salary_to');
		$salary_frequency =	get_post_meta($post->ID, '_salary_per');
		$benefits =			get_post_meta($post->ID, '_salary_benefits');

		$salary =			get_post_meta($post->ID, '_salary');

		if(get_post_meta($post->ID, '_salary_from') && get_post_meta($post->ID, '_salary_to')) {
			echo '<li class="salary"><i class="fa fa-money" aria-hidden="true"></i>';

				if($salary_currency[0] == 'gbp') {
					echo '£';
				} else {
					echo $salary_currency[0];
				}

				echo $salary_from[0];

				if($salary_to) {
					echo ' - ';
					if($salary_currency[0] == 'gbp') {
						echo '£';
					} else {
						echo $salary_currency[0];
					}
					echo $salary_to[0];
				}
				if($salary_frequency) {
					echo '/' . $salary_frequency[0];
				}
				if($benefits) {
					echo '<span class="salary-benefits"> + ' . $benefits[0] . '</span>';
				}
			echo '</li>';
			
		} elseif($salary) {
			echo $salary[0];
		}
	}
	
	/**
	 * This code gets your posted field and modifies the job search query
	 */
	add_filter( 'job_manager_get_listings', 'filter_by_salary_field_query_args', 10, 2 );
	function filter_by_salary_field_query_args( $query_args, $args ) {
		if ( isset( $_POST['form_data'] ) ) {
			parse_str( $_POST['form_data'], $form_data );
			// If this is set, we are filtering by salary
			if ( ! empty( $form_data['filter_by_salary'] ) ) {
				$selected_range = sanitize_text_field( $form_data['filter_by_salary'] );
				switch ( $selected_range ) {
					case 'upto20' :
						$query_args['meta_query'][] = array(
							'relation'	=> 'OR',
							array(
								'key'     => '_salary',
								'value'   => '20000',
								'compare' => '<',
								'type'    => 'NUMERIC'
							),
							array(
								'key'     => '_salary_from',
								'value'   => '20000',
								'compare' => '<',
								'type'    => 'NUMERIC'
							),
							array(
								'key'     => '_salary_to',
								'value'   => '20000',
								'compare' => '<',
								'type'    => 'NUMERIC'
							)
						);
					break;
					case 'over60' :
						$query_args['meta_query'][] = array(
							'relation'	=> 'OR',
							array(
								'key'     => '_salary',
								'value'   => '60000',
								'compare' => '>=',
								'type'    => 'NUMERIC'
							),
							array(
								'key'     => '_salary_from',
								'value'   => '60000',
								'compare' => '>=',
								'type'    => 'NUMERIC'
							),
							array(
								'key'     => '_salary_to',
								'value'   => '60000',
								'compare' => '>=',
								'type'    => 'NUMERIC'
							)
						);
					break;
					default :
						$query_args['meta_query'][] = array(
							'relation'	=> 'OR',
							array(
								'key'     => '_salary',
								'value'   => array_map( 'absint', explode( '-', $selected_range ) ),
								'compare' => 'BETWEEN',
								'type'    => 'NUMERIC'
							),
							array(
								'key'     => '_salary_from',
								'value'   => array_map( 'absint', explode( '-', $selected_range ) ),
								'compare' => 'BETWEEN',
								'type'    => 'NUMERIC'
							),
							array(
								'key'     => '_salary_to',
								'value'   => array_map( 'absint', explode( '-', $selected_range ) ),
								'compare' => 'BETWEEN',
								'type'    => 'NUMERIC'
							)
						);
					break;
				}
				// This will show the 'reset' link
				add_filter( 'job_manager_get_listings_custom_filter', '__return_true' );
			}
		}
		return $query_args;
	}

?>
