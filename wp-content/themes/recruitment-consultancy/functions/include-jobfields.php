<?php

	function admin_add_job_fields( $fields ) {

		// Contact Data

		$fields['_contact_name'] = array(
			'label'       => __( 'Contact Name', 'job_manager' ),
			'type'        => 'text',
			'description' => ''
		);
		$fields['_contact_email'] = array(
			'label'       => __( 'Contact Email', 'job_manager' ),
			'type'        => 'text',
			'description' => ''
		);
		$fields['_contact_telephone'] = array(
			'label'       => __( 'Contact Telephone', 'job_manager' ),
			'type'        => 'text',
			'description' => ''
		);
		$fields['_contact_url'] = array(
			'label'       => __( 'Contact URL', 'job_manager' ),
			'type'        => 'text',
			'description' => ''
		);

		// Job Details

		$fields['_job_duration'] = array(
			'label'       => __( 'Job Duration', 'job_manager' ),
			'type'        => 'text',
			'description' => ''
		);
		$fields['_job_skills'] = array(
			'label'       => __( 'Job Skills', 'job_manager' ),
			'type'        => 'text',
			'description' => ''
		);
		$fields['_job_requirements'] = array(
			'label'       => __( 'Job Requirements', 'job_manager' ),
			'type'        => 'text',
			'description' => ''
		);
		$fields['_min_experience'] = array(
			'label'       => __( 'Minimum Experience', 'job_manager' ),
			'type'        => 'text',
			'description' => ''
		);
		$fields['_education_level'] = array(
			'label'       => __( 'Education Level', 'job_manager' ),
			'type'        => 'text',
			'description' => ''
		);
		$fields['_job_postcode'] = array(
			'label'       => __( 'Job Postcode', 'job_manager' ),
			'type'        => 'text',
			'description' => ''
		);

		// Salary Details

		$fields['_salary_currency'] = array(
			'label'       => __( 'Salary Currency', 'job_manager' ),
			'type'        => 'text',
			'description' => ''
		);
		$fields['_salary_from'] = array(
			'label'       => __( 'Salary From', 'job_manager' ),
			'type'        => 'text',
			'description' => ''
		);
		$fields['_salary_to'] = array(
			'label'       => __( 'Salary To', 'job_manager' ),
			'type'        => 'text',
			'description' => ''
		);
		$fields['_salary_per'] = array(
			'label'       => __( 'Salary Per', 'job_manager' ),
			'type'        => 'text',
			'description' => ''
		);
		$fields['_salary_benefits'] = array(
			'label'       => __( 'Salary Benefits', 'job_manager' ),
			'type'        => 'text',
			'description' => ''
		);

		$fields['_salary'] = array(
			'label'       => __( 'Salary ($)', 'job_manager' ),
			'type'        => 'text',
			'description' => ''
		);
		return $fields;
	}

?>
