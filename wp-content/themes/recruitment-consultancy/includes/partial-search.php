<form method="GET" action="<?php bloginfo('url'); ?>/jobs" class="form-widget">
	<input type="text" id="search_keywords" name="search_keywords" placeholder="Keywords" />
	<input type="text" id="search_location" name="search_location" placeholder="Location" />
	<select id="search_category" name="search_category">
	<option disabled selected value="Category">Specialist Sectors</option>
		<?php foreach ( get_job_listing_categories() as $cat ) : ?>
			<option value="<?php echo esc_attr( $cat->term_id ); ?>"><?php echo esc_html( $cat->name ); ?></option>
		<?php endforeach; ?>
	</select>
	<input type="submit" value="Search" />
	<?php echo do_shortcode( '[gjm_form_geolocation_features gjm_autocomplete="1" gjm_locator_button="1"]' ); ?>
</form>
