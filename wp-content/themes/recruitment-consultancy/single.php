<?php get_header(); ?>

	<?php if (have_posts()) { ?>
		<?php while ( have_posts() ) { ?>
			<?php the_post(); ?>	

			<section class="container ultra">
				<article>
					<?php the_title(); ?>
					<?php the_content(); ?>
				</article>
			</section>

		<?php } ?>
	<?php } ?>

<?php get_footer(); ?>
