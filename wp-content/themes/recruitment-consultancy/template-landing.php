<?php
	//Template Name: Landing Page
	get_header();
	$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'page-flood' );
?>

	<?php if (have_posts()) { ?>
		<?php while ( have_posts() ) { ?>
			<?php the_post(); ?>	

			<section class="page-feature">
				<div class="page-feature-blur" style="background-image: url('<?php echo $image[0]; ?>');"></div>
				<div class="page-feature-grad">
					<section class="container ultra">
						<?php the_content(); ?>
					</section>
				</div>
			</section>

			<section class="landing-content">
				<section class="container ultra">
					<section class="split-modules">
						<aside class="module half-width">
							<h2 class="module-title green">I need to fill an office-based vacancy</h2>
							<p>We can help.</p>
							<p>For over 20 years we have built strong relationships with local businesses ranging in size from 5-500 staff.</p>
							<p>We have a large and active base of office-based candidates we know personally who are available for immediate start.</p>
							<p>Trust us to provide your next office team member.</p>
							<p>Contact your local office today with what you need and we will do our best to help.</p>
							<p>&nbsp;</p>
							<a href="<?php echo get_the_permalink(10); ?>" class="button worthing">Worthing Office</a>
							<a href="<?php echo get_the_permalink(11); ?>" class="button guildford">Guildford Office</a>
						</aside>
						<aside class="module half-width">
							<h2 class="module-title green">I want to find my next office-based job</h2>
							<p>Fantastic - We have temporary, permanent, part-time and contract office based opportunities available right now.</p>
							<p>&nbsp;</p>
							<a href="<?php echo get_the_permalink(5); ?>" class="button">Search our jobs</a>
							<a href="<?php echo get_the_permalink(40); ?>" class="button">Submit your CV</a>
						</aside>
					</section>
				</section>
			</section>

			<section class="landing-stats page-feature">
				<div class="page-feature-blur"></div>
				<div class="page-feature-grad">
					<section class="container ultra">
						<h2 class="section-title white">Last month at The Recruitment Consultancy</h2>
						<div class="stats-circles">
							<aside>
								<div class="stat-circle">
									<h2 class="white">135</h2>
								</div>
								<h3 class="white">New vacancies</h3>
							</aside>
							<aside>
								<div class="stat-circle">
									<h2 class="white">81</h2>
								</div>
								<h3 class="white">New candidates registered</h3>
							</aside>
							<aside>
								<div class="stat-circle">
									<h2 class="white">81</h2>
								</div>
								<h3 class="white">Successful placements</h3>
							</aside>
						</div>
					</section>
				</div>
			</section>

			<section class="landing-quotes">
				<section class="container ultra">
					<section class="split-modules">
						<aside class="module half-width">
							<i class="fa fa-quote-left background-icon small" aria-hidden="true"></i>
							<div class="foreground-content">
								<h2 class="module-title">What our clients say about us...</h2>
								<?php 
								    $args = array(
								        'post_type'             => 'testimonials',
								        'posts_per_page'        => 5,
								        'orderby'               => 'rand',
								        'tax_query' => array(
									        array(
									            'taxonomy' => 'source',
									            'field' => 'slug',
									        	'terms' => 'client'
									    	)
									    )
								    );  
								    $the_query = new WP_Query( $args );
								?>
							    <div class="slider-container">
							        <?php if($the_query->have_posts() ) { ?>
							            <ul class="simple-testimonial-slider">
							            	<?php while($the_query->have_posts()) { ?>
							                    <?php $the_query->the_post(); ?>
							                        <li>
							                            <?php the_content(); ?>
							                            <h2><?php the_title(); ?></h2>
							                        </li>	
							                	<?php wp_reset_postdata(); ?>
							            	<?php } ?>
							            </ul>
							        <?php } ?>
							    </div>									
							</div>
						</aside>
						<aside class="module half-width">
							<i class="fa fa-quote-left background-icon small" aria-hidden="true"></i>
							<div class="foreground-content">
								<h2 class="module-title">What our candidates say about us...</h2>
								<?php 
								    $args = array(
								        'post_type'             => 'testimonials',
								        'posts_per_page'        => 5,
								        'orderby'               => 'rand',
								        'tax_query' => array(
									        array(
									            'taxonomy' => 'source',
									            'field' => 'slug',
									        	'terms' => 'candidate'
									    	)
									    )
								    );  
								    $the_query = new WP_Query( $args );
								?>
							    <div class="slider-container">
							        <?php if($the_query->have_posts() ) { ?>
							            <ul class="simple-testimonial-slider">
							            	<?php while($the_query->have_posts()) { ?>
							                    <?php $the_query->the_post(); ?>
							                        <li>
							                            <?php the_content(); ?>
							                            <h2><?php the_title(); ?></h2>
							                        </li>	
							                	<?php wp_reset_postdata(); ?>
							            	<?php } ?>
							            </ul>
							        <?php } ?>
							    </div>									
							</div>
						</aside>
					</section>
				</section>
			</section>

		<?php } ?>
	<?php } ?>
	
<?php get_footer(); ?>
