<?php global $post;

if ( ! get_option( 'resume_manager_force_application' ) ) {
	echo '<hr />';
}

if ( is_user_logged_in() && sizeof( $resumes ) ) : ?>
	<form class="apply_with_resume" method="post">
		<p><?php _e( 'Apply using your online resume; just enter a short message to send your application.', 'wp-job-manager-resumes' ); ?></p>
		<p>
			<label for="resume_id"><?php _e( 'Online resume', 'wp-job-manager-resumes' ); ?>:</label>
			<select name="resume_id" id="resume_id" required>
				<?php
					foreach ( $resumes as $resume ) {
						echo '<option value="' . absint( $resume->ID ) . '">' . esc_html( $resume->post_title ) . '</option>';
					}
				?>
			</select>
		</p>
		<p>
			<label><?php _e( 'Message', 'wp-job-manager-resumes' ); ?>:</label>
			<textarea name="application_message" cols="20" rows="4" required><?php echo $resume->post_content; ?></textarea>
		</p>
		<p>
			<input type="submit" name="wp_job_manager_resumes_apply_with_resume" value="<?php esc_attr_e( 'Send Application', 'wp-job-manager-resumes' ); ?>" />
			<input type="hidden" name="job_id" value="<?php echo absint( $post->ID ); ?>" />
		</p>
	</form>
<?php else : ?>
	<form class="apply_with_resume" method="post" action="<?php echo get_permalink( get_option( 'resume_manager_submit_resume_form_page_id' ) ); ?>">
		<p><?php _e( 'You can apply to this job and others using your online resume. Click the link below to submit your online resume and email your application to this employer.', 'wp-job-manager-resumes' ); ?></p>

		<p>
			<input type="submit" name="wp_job_manager_resumes_apply_with_resume_create" value="<?php esc_attr_e( 'Submit Resume &amp; Apply', 'wp-job-manager-resumes' ); ?>" />
			<input type="hidden" name="job_id" value="<?php echo absint( $post->ID ); ?>" />
		</p>
	</form>
<?php endif; ?>
