			<footer>
				<section class="footer-form">
					<section class="footer-container">
						<h2 class="green">Please send me the quarterly newsletter:</h2>
						<?php echo do_shortcode('[contact-form-7 id="4" title="Newsletter Sign Up"]'); ?>
					</section>	
				</section>
				<section class="footer-main">
					<section class="footer-container flex-top">
						<aside class="footer-col footer-address worthing-office">
							<i class="fa fa-map-marker background-icon small"></i>
							<div class="foreground-content">
								<h2>Worthing Office</h2>
								<p>1 Argyll House <br>15 Liverpool Gardens <br>Worthing <br>West Sussex <br>BN11 1RY</p>
								<p><i class="fa fa-phone"></i> 01903 820082 <br><i class="fa fa-envelope"></i> email us</p>
							</div>
						</aside>
						<aside class="footer-col footer-address guildford-office">
							<i class="fa fa-map-marker background-icon small"></i>
							<div class="foreground-content">
								<h2>Guildford Office</h2>
								<p>101 High Street <br>Guildford <br>Surrey <br>GU1 3DP<br><br></p>
								<p><i class="fa fa-phone"></i> 01483 456465 <br><i class="fa fa-envelope"></i> email us</p>
							</div>
						</aside>
						<aside class="footer-col">
							<h2>Join us</h2>
							<ul class="footer-socials">
								<!-- <li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
								<li><a href="#"><i class="fa fa-twitter-square"></i></a></li> -->
								<li><a href="https://www.linkedin.com/company/293973/" target="_blank"><i class="fa fa-linkedin-square"></i></a></li>
							</ul>
						</aside>
						<aside class="footer-col">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/red-member.png" alt="RED Member since 1997">
							<p>Members since 1997!</p>
						</aside>
					</section>
				</section>
				<section class="footer-meta">
					<section class="footer-container">
						<nav>
							<?php wp_nav_menu( array( 'theme_location' => 'footer-menu', 'container' => '' ) ); ?>
						</nav>
						<p>Copyright The Recruitment Consultancy <?php echo the_date('Y'); ?></p>
						<p><a href="http://yokedigital.com/" target="_blank">A Yoke Digital Site</a></p>
					</section>
				</section>
			</footer>

		</section>
	</section>

	<?php if(is_page(291) || is_page(292)) { ?>
		<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyBR5bX6m_CEPwitun65XjrFWYZVRtzqADA"></script>	
	<?php } ?>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/lib/modernizr-custom.js" type="text/javascript"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/lib/matchMedia.js" type="text/javascript"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/lib/matchMedia.addListener.js" type="text/javascript"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/lib/enquire.min.js" type="text/javascript"></script>
	<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/js/font-awesome/css/font-awesome.min.css">
	
	<!--Google Analytics-->

	<?php wp_footer(); ?>
</body>
</html>
